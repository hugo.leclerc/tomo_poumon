Review

Covid 19 -> est-ce vraiment applicable aux images du poumon ?




In this manuscript, the authors propose to add a new regularization term to improve the reconstruction of binary tomographies with very few projections. The purpose of this new term is to force the centroid of the reconstruction to a prescribed position.

From what I can tell, it doesn't seem like a revolutionary proposal, but this manuscript is clear and pleasant to read.

Nevertheless, there are some areas where the content should be improved:
* the title, the summary and the introduction mention that the algorithm is designed to deal with limited-view (i.e. with possibly several projections) but the authors state just after that it is only relevant for only one projection. The title and the summary (at least before the mention of the experimental results) and an important part of the introduction are misleading and should be changed. As an exemple of this point, the protocol for COVID-19 case ascertainment is mentionned as an application of tomographic reconstruction... but does the algorithm proposed in the manuscript is not applicable to this case. The case must either not be mentionned, or it must be specified that it is not in the scope of the proposition.
* It would be worth -- if not mandatory -- having somewhere in the manuscript a discussion section about how to obtain or infer the centroid in the real world, to help the readers figure some cases where the proposition is relevant.
* The main point of the manuscript is that the local minimum with an enforced centroid seems to be better than the one obtained without regularization and with a later translation. Nevertheless, there's no explanation on the reason. Will it work on truly different kinds of shape ? If What would be the criterion

Here are some minor remarks:
* P2 l20: there is a wrong citation reference (a "?" in the text).
* In the introduction, 