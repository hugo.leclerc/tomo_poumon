import sys, os, numpy as np

# symbols
# from .ProjectionLoader import ProjectionLoader
# from .PartRotation import PartRotation
# from .RecoRings import RecoRings
from .VisuClient import VisuClient

from .reco import reconstruction, ring_traces, ring_corr, binarisation_max_disc, max_median_on_discs
from .reco import load_edf_projections, regular_rotation, paganin, back_filter, reco_msh, reco_cg
from .dist import dist, add_ball
from .federer import federer
from .Filter import Filter
from .array import cst_val
from .reach import reach
from .Opt import Opt


def total_variation( vol ):
    if isinstance( vol, Filter ):
        return total_variation( vol.value() )
    vol[ np.isnan( vol ) ] = 0
    sx, sy, sz = 0, 0, 0
    if vol.shape[ 0 ] > 1:
        sz = np.sum( np.abs( vol[ 1:, :, : ] - vol[ :-1, :, : ] ) )
    if vol.shape[ 1 ] > 1:
        sy = np.sum( np.abs( vol[ :, 1:, : ] - vol[ :, :-1, : ] ) )
    if vol.shape[ 2 ] > 1:
        sx = np.sum( np.abs( vol[ :, :, 1: ] - vol[ :, :, :-1 ] ) )
    return ( sz + sy + sx ) / np.std( vol )

def save_xdmf( vol, name ):
    from .compiled_toolboxes import compiled_toolboxes
    ct = compiled_toolboxes( "reco_cpu" )
    ct.save_xdmf( vol, name )
    