from . import reconstruction_toolbox_cpu as rt_cpu
from . import reconstruction_toolbox_gpu as rt_gpu

from .load_or_execute import load_or_execute, output_filename
from .load_edf        import load_edf_projections
from .paganin_filter  import paganin_filter
from .back_filter     import back_filter
from .                import config

from matplotlib import pyplot
import numpy as np
import os

#
def make_paga( experiment, paga_coeff, scale_xy, scale_a ):
    # find directory where experiment exists
    for d in config.acquisition_directories:
        if os.path.exists( os.path.join( d, experiment ) ):
            acquisition_directory = d
            break
    else:
        raise "acquisition directory not found for experiment {}".format( experiment )

    edfs = load_edf_projections( os.path.join( d, experiment ), x_scale = scale_xy, a_scale = scale_a, lim_a = 10000 )
    return paganin_filter( edfs, paga_coeff / scale_xy ** 2 )

#
def make_proj( experiment, paga_coeff, scale_xy, scale_a ):
    paga = load_or_execute( experiment, [ "scale", [ scale_xy, scale_a ], "paga", paga_coeff ], "paga", make_paga, experiment, paga_coeff, scale_xy, scale_a )
    return back_filter( - np.log( paga ) )

#
def process_func( queue_i2c, queue_c2i, experiment ):
    """ have to send get parameter values """

    # parameters
    best_total_variation = 1e40
    best_value_index = 0
    params = {}

    def get_parm( name, default_value = 0, default_amplitude = 1 ):
        if not ( name in params ):
            params[ name ] = { "val" : default_value, "amp": default_amplitude }
        return params[ name ][ "val" ]

    def get_parm_to_opt():
        if "optimize" in params:
            v = [ k for k in params ]
            return v[ params[ "optimize" ][ "val" ] ]
        return ""

    def get_parm_opt( name, num_value_to_try, nb_values_to_try, default_value = 0, default_amplitude = 1 ):
        v = get_parm( name, default_value, default_amplitude )
        if name == get_parm_to_opt():
            if num_value_to_try == nb_values_to_try:
                num_value_to_try = best_value_index
            return v + 5 * params[ name ][ "amp" ] * ( num_value_to_try / ( nb_values_to_try - 1.0 ) - 0.5 )
        return v

    # reconstructions
    need_to_send_params = True
    old_param_set = ""
    fig = pyplot.figure()
    proj_gpu = None
    mlab_obj = None
    while True:
        # read and save param values in queue
        cmd = queue_i2c.get()
        try:
            while True:
                cmd = queue_i2c.get_nowait()
        except Exception:
            pass

        # if no change => continue
        if need_to_send_params == False and not ( "optimize" in params ):
            has_change = False
            for name in cmd:
                has_change |= ( name in params ) == False or params[ name ][ "val" ] != cmd[ name ][ "val" ]
            if has_change == False:
                continue

        # register changes
        params = {}
        for name in cmd:
            params[ name ] = cmd[ name ]

        #
        paga_coeff = get_parm( "paga_coeff", 0, 50 )
        scale_xy = int( get_parm( "scale xy", 4, 1 ) )
        scale_a = int( get_parm( "scale a", 1, 1 ) )

        # load proj if not already done
        param_lst = [ "scale", [ scale_xy, scale_a ], "paga", paga_coeff ]
        param_set = "/".join( str( x ) for x in param_lst )
        if old_param_set != param_set:
            proj = load_or_execute( experiment, param_lst, "proj", make_proj, experiment, paga_coeff, scale_xy, scale_a )
            old_param_set = param_set

            if not ( proj_gpu is None ):
                rt_gpu.del_gpu_vol( proj_gpu )
            proj_gpu = rt_gpu.new_gpu_vol( proj )

        # coordinates to work on
        len_x = int( np.ceil( get_parm( "len x", proj.shape[ 2 ] * scale_xy, 8 ) / scale_xy ) )
        len_y = int( np.ceil( get_parm( "len y",                          1, 8 ) / scale_xy ) )
        len_z = int( np.ceil( get_parm( "len z", proj.shape[ 2 ] * scale_xy, 8 ) / scale_xy ) )

        bnt_x = get_parm( "center x", 0.5 * proj.shape[ 2 ] * scale_xy, 8 ) / scale_xy
        bnt_y = get_parm( "center y", 0.5 * proj.shape[ 1 ] * scale_xy, 8 ) / scale_xy
        bnt_z = get_parm( "center z", 0.5 * proj.shape[ 2 ] * scale_xy, 8 ) / scale_xy

        inc_x = get_parm( "inc x", 1 )
        inc_y = get_parm( "inc y", 1 )
        inc_z = get_parm( "inc z", 1 )

        nb_values_to_try = 1
        if "optimize" in params:
            best_total_variation = 1e40
            nb_values_to_try = 20
            tvs_x = []
            tvs_y = []

        for num_value_to_try in range( nb_values_to_try ):
            # get parameters
            x_off = get_parm_opt( "x_rot", num_value_to_try, nb_values_to_try )
            x_rot = 0.5 * proj.shape[ 2 ] + x_off / scale_xy

            x_slip = get_parm_opt( "x_slip", num_value_to_try, nb_values_to_try ) / scale_xy
            y_slip = get_parm_opt( "y_slip", num_value_to_try, nb_values_to_try ) / scale_xy
            z_slip = get_parm_opt( "z_slip", num_value_to_try, nb_values_to_try ) / scale_xy

            cnt_x = bnt_x - 0.5 * x_slip
            cnt_y = bnt_y - 0.5 * y_slip
            cnt_z = bnt_z - 0.5 * z_slip

            # definition of the rotations
            rota = rt_cpu.make_rotations(
                [ 0.0 * proj.shape[ 2 ], 0.0 * proj.shape[ 1 ], 0.0 * proj.shape[ 2 ] ],
                [ 1.0 * proj.shape[ 2 ], 1.0 * proj.shape[ 1 ], 1.0 * proj.shape[ 2 ] ],
                [ x_rot                , 0.0 * proj.shape[ 1 ], 0.5 * proj.shape[ 2 ] ],
                [ x_rot                , 1.0 * proj.shape[ 1 ], 0.5 * proj.shape[ 2 ] ],
                proj.shape[ 0 ]        , x_slip, y_slip, z_slip
            )

            # total variation
            if nb_values_to_try > 1:
                beg_x = cnt_x - 0.5 * inc_x * len_x
                beg_y = cnt_y - 0.5 * inc_y * len_y
                beg_z = cnt_z - 0.5 * inc_z * len_z

                reco = rt_gpu.reconstruct( proj_gpu, rota, 
                    beg_x, inc_x, len_x,
                    beg_y, inc_y, len_y,
                    beg_z, inc_z, len_z,
                    get_parm( "inter order", 1 )
                )

                tx = np.sum( np.abs( reco[ :, :, 1: ] - reco[ :, :, :-1 ] ) )
                ty = np.sum( np.abs( reco[ :, 1:, : ] - reco[ :, :-1, : ] ) )
                tz = np.sum( np.abs( reco[ 1:, :, : ] - reco[ :-1, :, : ] ) )
                tv = tx + ty + tz

                tvs_x.append( get_parm_opt( get_parm_to_opt(), num_value_to_try, nb_values_to_try ) )
                tvs_y.append( tv )
                if best_total_variation > tv:
                    best_total_variation = tv
                    best_value_index = num_value_to_try

            # display reconstructions
            if num_value_to_try + 1 == nb_values_to_try:
                # 3D viz
                if get_parm( "disp 3D", 0 ) or get_parm( "reconstruct", 0 ):
                    beg_x = cnt_x - 0.5 * inc_x * len_x
                    beg_y = cnt_y - 0.5 * inc_y * len_y
                    beg_z = cnt_z - 0.5 * inc_z * len_z

                    reco = rt_gpu.reconstruct( proj_gpu, rota, 
                        beg_x, inc_x, len_x,
                        beg_y, inc_y, len_y,
                        beg_z, inc_z, len_z,
                        get_parm( "inter order", 1 )
                    )

                    # pyplot.hist( reco.flat, 100 )
                    # pyplot.show()
                    # if get_parm( "reconstruct", 0 ):
                    #     for n, v in [ [ "reco.npy", reco ], [ "xoff.npy", np.array( x_off ) ] ]:
                    #         fn = output_filename( experiment, [ "scale", [ scale_xy, scale_a ], "paga", paga_coeff, "cnt_len", [ cnt_x, cnt_y, cnt_z, len_x, len_y, len_z ] ], n )
                    #         np.save( fn, v )

                    if get_parm( "disp 3D", 0 ):
                        from mayavi import mlab

                        mlab.clf()
                        mlab_obj = mlab.contour3d( reco, contours = [ get_parm( "contour val" ) ] )
                        mlab.view( azimuth = get_parm( "azimuth" ), elevation = get_parm( "elevation" ) )
                        mlab.savefig( '3D.png' )

                # 2D viz
                va = []
                len_v = [ len_z, len_y, len_x ]
                for axes in [ [ 0, 1 ], [ 0, 2 ], [ 1, 2 ] ]:
                    if len_v[ axes[ 0 ] ] != 1 and len_v[ axes[ 1 ] ] != 1:
                        va.append( axes )

                pyplot.ion()
                fig.clear()
                sp = fig.subplots( 1, len( va ) + ( nb_values_to_try > 1 ) )
                if len( va ) + ( nb_values_to_try > 1 ) == 1:
                    sp = [ sp ]
                for num_subplot, axes in enumerate( va ):
                    t_len_x = 1 + ( len_x - 1 ) * ( 2 in axes )
                    t_len_y = 1 + ( len_y - 1 ) * ( 1 in axes )
                    t_len_z = 1 + ( len_z - 1 ) * ( 0 in axes )

                    t_beg_x = cnt_x - 0.5 * inc_x * t_len_x
                    t_beg_y = cnt_y - 0.5 * inc_y * t_len_y
                    t_beg_z = cnt_z - 0.5 * inc_z * t_len_z

                    beg_v = [ t_beg_z, t_beg_y, t_beg_x ]
                    inc_v = [ inc_z, inc_y, inc_x ]
                    
                    reco = rt_gpu.reconstruct( proj_gpu, rota, 
                        t_beg_x, inc_x, t_len_x,
                        t_beg_y, inc_y, t_len_y,
                        t_beg_z, inc_z, t_len_z,
                        get_parm( "inter order", 1 )
                    )

                    # display
                    sp[ num_subplot ].set_title( "".join( [ "zyx"[ i ] for i in axes ] ) )
                    sp[ num_subplot ].imshow( reco.reshape( ( len_v[ axes[ 0 ] ], len_v[ axes[ 1 ] ] ) ), cmap='gray', interpolation='nearest', extent=[
                        beg_v[ axes[ 0 ] ], beg_v[ axes[ 0 ] ] + len_v[ axes[ 0 ] ] * inc_v[ axes[ 0 ] ], 
                        beg_v[ axes[ 1 ] ], beg_v[ axes[ 1 ] ] + len_v[ axes[ 1 ] ] * inc_v[ axes[ 1 ] ]
                    ], origin='lower' )
                    sp[ num_subplot ].contour( reco.reshape( ( len_v[ axes[ 0 ] ], len_v[ axes[ 1 ] ] ) ), colors='red', alpha=0.5, extent=[
                        beg_v[ axes[ 0 ] ], beg_v[ axes[ 0 ] ] + len_v[ axes[ 0 ] ] * inc_v[ axes[ 0 ] ], 
                        beg_v[ axes[ 1 ] ], beg_v[ axes[ 1 ] ] + len_v[ axes[ 1 ] ] * inc_v[ axes[ 1 ] ]
                    ], levels=[ get_parm( "contour val" ) ] )
                    # ax.contour(hdu.datanp.logspace(-4.7, -3., 10), colors='white', alpha=0.5)

                if nb_values_to_try > 1:
                    sp[ -1 ].set_title( "opt of {}".format( get_parm_to_opt() ) )
                    sp[ -1 ].plot( tvs_x, tvs_y )

                pyplot.draw()
                pyplot.pause( 0.001 )

            # send used parms
            if need_to_send_params:
                queue_c2i.put( params )
                need_to_send_params = False
