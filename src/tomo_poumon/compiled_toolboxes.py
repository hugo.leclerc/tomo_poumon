from .cuda import check_cuda
import os, sys, importlib


class CompiledToolBox:
    def __init__( self, mod_name ):
        dir_name = os.path.dirname( os.path.abspath( __file__ ) )
        cpp_name = os.path.join( dir_name, "cpp", mod_name ) + ".cpp"
        lib_name = os.path.join( dir_name, "cpp", "build", mod_name )
        if os.system( f"nsmake lib `{ sys.executable } -m pybind11 --includes` -o { lib_name }`python3-config --extension-suffix` { cpp_name }" ):
            sys.exit( 1 )
        self.lib = importlib.import_module( "tomo_poumon.cpp.build." + mod_name )
        

class CompiledToolBoxes:
    def __init__( self ):
        self.tbs = {}

    def __call__( self, cpu_name, gpu_name = None ):
        hgpu = gpu_name and check_cuda()
        name = gpu_name if hgpu else cpu_name

        # prerequisites, compilation and load if not already made
        if name not in self.tbs:
            # prerequisites
            if hgpu and name != "GpuVol":
                self( "GpuVol" )
                
            # compilation + load
            self.tbs[ name ] = CompiledToolBox( name )

        # get the loaded library
        return self.tbs[ name ].lib


compiled_toolboxes = CompiledToolBoxes()
