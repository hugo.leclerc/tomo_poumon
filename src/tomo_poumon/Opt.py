from .OptParameter import OptParameter
from .config import output_directory
from .Filter import Filter
import os, sys, json
import numpy as np


class Opt:
    def __init__( self, interactive = False, nb_rounds = 1 ):
        self.number_of_rounds = nb_rounds
        self.current_round = -1 
        
        self.curr_opt_param = "" # parameter that is being optimized
        self.error_name = "" # parameter used to get the overall error (objective value)
        
        self._visu_client_has_the_parameters = False # 
        self._visu_client = None # instance of VisuClient
        self.interactive = interactive
        self.parameters = {} # parameter_name => OptParameter (which give current value, increment, ...)
        self.results = {}  # result_name =>( parameter_values => value ). value can be a scalar (will be displayed in a plot) or an array (will be animated)
    
    @property
    def visu_client( self ):
        if not self._visu_client:
            from .VisuClient import VisuClient
            self._visu_client = VisuClient()
        return self._visu_client

    def __iter__( self ):
        return self

    def __next__( self ):
        # for the first call, there's nothing to do
        if self.current_round < 0:
            self.current_round = 0
            return self

        # if we're in an interactive session, we send the values and we wait for new instructions
        if self.interactive:
            if not self._visu_client_has_the_parameters:
                self.visu_client.send_params( self.parameters )
                self._visu_client_has_the_parameters = True

            # plot values obtained so far
            self.plot( block = False )

            # wait for instructions (e.g. new values)
            while True:
                j = self.visu_client.get_instruction()

                if j == "quit" or j == "":
                    raise StopIteration

                if j == "reset_plots":
                    j = self.visu_client.send_reset_plots()
                    self.results.clear()
                    continue

                if j.startswith( "params " ):
                    # save the new values
                    for k, v in json.loads( j[ 7: ] ).items():
                        self.parameters[ k ].sampling = v[ "sampling" ]
                        self.parameters[ k ].center = v[ "center" ]
                        self.parameters[ k ].width = v[ "width" ]
                        self.parameters[ k ].index = v[ "index" ]
                    break

                print( "Unhandled msg type " + j )
                sys.exit( 3 )

            return self

        while True:
            if not self.parameters:
                raise StopIteration

            pk = list( self.parameters.keys() )
            if self.curr_opt_param not in self.parameters:
                self.curr_opt_param = pk[ 0 ]

            # next index in curr_opt_param
            if self.parameters[ self.curr_opt_param ].next_index():
                return self

            # if not possible, get the best value for self.curr_opt_param and optimize another parameter
            if self.error_name:
                best_opt_val = min( self.results[ self.error_name ].values(), key = lambda opt_val: opt_val.value )
                for k, v in best_opt_val.params.items():
                    self.parameters[ k ].set_value( v )

            index = pk.index( self.curr_opt_param ) + 1
            if index == len( pk ):
                self.current_round += 1
                if self.current_round == self.number_of_rounds:
                    raise StopIteration
            self.curr_opt_param = pk[ index ]

    def value( self, name, center, width = 11, sampling = 11 ):
        """ current value for parameter `name`. center, width and sampling are used to init the parameter the first time """
        if name not in self.parameters:
            self.parameters[ name ] = OptParameter( center, width, sampling )
        return self.parameters[ name ].value

    def value_list( self, name, centers, width = 11, sampling = 11, suffixes = "xyzabcdefghijklmnopqr" ):
        """ make a list of values given a list of centers """
        return [ self.value( name + suffixes[ len( centers ) - 1 - i ], center, width, sampling ) for i, center in enumerate( centers ) ]

    def value_array( self, name, centers, width = 11, sampling = 11, suffixes = "xyzabcdefghijklmnopqr" ):
        """ make a np.array of values given a list of centers """
        return np.array( self.value_list( name, centers, width, sampling, suffixes ) )

    def set_error( self, name, value ):
        """ add an error value for the current set of parameters """
        self.set_result( name, value )
        self.error_name = name

    def set_result( self, name, value ):
        """ store a result for the current set of parameters. It can be for example a scalar ... """
        if isinstance( value, Filter ):
            value = value.value()
        print( "Value of {}: {}".format( name, value ) )
        if name not in self.results:
            self.results[ name ] = {}
        self.results[ name ][ frozenset( self.parameter_values.items() ) ] = value

    @property
    def parameter_values( self ):
        res = {}
        for name, param in self.parameters.items():
            res[ name ] = param.value
        return res

    def plot( self, block = True ):
        # for each result that may have to be displayed
        for name_result, result in self.results.items():
            v = {}
            for fsp, result_value in result.items():
                for name_param, value_param in fsp:
                    if name_param not in v:
                        v[ name_param ] = {}

                    other_values = ""
                    for other_name_param, other_value_param in fsp:
                        if other_name_param != name_param:
                            if other_values:
                                other_values += ' '
                            if other_value_param == int( other_value_param ):
                                other_value_param = int( other_value_param )
                            other_values += other_name_param + "=" + str( other_value_param )

                    if other_values not in v[ name_param ]:
                        v[ name_param ][ other_values ] = {}

                    v[ name_param ][ other_values ][ value_param ] = result_value

            # 
            params_to_display = {}
            for name_param, dict_dict_values in v.items():
                for other_values, dict_values in dict_dict_values.items():
                    if len( dict_values ) > 1:
                        params_to_display[ name_param ] = 1

            # make a plot for each varying parameter
            colors = [
                ( 255, 255, 255 ),
                ( 255,   0,   0 ),
                (   0, 255,   0 ),
                (   0,   0, 255 ),
                (   0, 255, 255 ),
                ( 255,   0, 255 ),
                ( 255, 255,   0 ),
                ( 128, 128, 128 ),
            ]
            symbols = "ostd+x"

            for name_param, dict_dict_values in v.items():
                if name_param not in params_to_display:
                    continue
                num_plot = 0
                for other_values, dict_values in dict_dict_values.items():
                    s = sorted( dict_values.items() )
                    self.visu_client.send_plot(
                        ( name_result, name_param, other_values ), 
                        [ v[ 0 ] for v in s ], 
                        [ v[ 1 ] for v in s ], 
                        symbol = symbols[ num_plot % len( symbols ) ],
                        pen = colors[ num_plot % len( colors ) ],
                        json_labels = True
                    )
                    num_plot += 1
