from .Filter import filter
import numpy as np

@filter() # , rem_dist_from_boundaries = True
def dist( input, threshold = 0, nb_points = 0, max_dist = 100 ):
    """ nb_points = min nb points in the ball to consider an intersection """
    nb_points = max( 0, int( nb_points ) )
    if nb_points:
        from .compiled_toolboxes import compiled_toolboxes
        return compiled_toolboxes( "fuzzy_dist" ).fuzzy_dist( input, threshold, nb_points, max_dist )
    import skfmm
    res = skfmm.distance( input )
    # if rem_dist_from_boundaries:
    #     # tous les points qui viennent des bords ouverts doivent passer à l'extérieur
    #     bnd = np.ones_like( res )
    #     bnd[  0,  :,  : ] = 0
    #     bnd[ -1,  :,  : ] = 0
    #     bnd[  :,  0,  : ] = 0
    #     bnd[  :, -1,  : ] = 0
    #     bnd[  :,  :,  0 ] = 0
    #     bnd[  :,  :, -1 ] = 0
    #     phi = np.ma.MaskedArray( bnd, res < 0 )
    #     tmp = skfmm.distance( phi ).data
    #     res[ tmp < res ] = -1

    return res

@filter( save = 0, cache = 0 )
def add_ball( input, position, radius ):
    """ modify a dist function to have a ball... """
    if input.ndim == 3:
        x = np.arange( input.shape[ 2 ] ) - position[ 2 ]
        y = np.arange( input.shape[ 1 ] ) - position[ 1 ]
        z = np.arange( input.shape[ 0 ] ) - position[ 0 ]
        Z, Y, X = np.meshgrid( z, y, x, indexing = "ij" )
        b = radius - ( X**2 + Y**2 + Z**2 )**0.5
    return np.maximum( input, b )

