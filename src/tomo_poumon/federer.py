from .Filter import filter
import numpy as np

# class Federer:
#     def __init__( self ):
#         pass 

@filter( save = 0 )
def federer( input, min_rs = 10, max_rs = 100, inc_rs = 1, min_rc = 100, max_rc = 100, inc_rc = 1, linalg = False ):
    """  """
    from .compiled_toolboxes import compiled_toolboxes
    if linalg:
        return compiled_toolboxes( "federer_linalg" ).federer( input, min_rs, max_rs, inc_rs, min_rc, max_rc, inc_rc )
    return compiled_toolboxes( "federer" ).federer( input, min_rs, max_rs, inc_rs, min_rc, max_rc, inc_rc )
