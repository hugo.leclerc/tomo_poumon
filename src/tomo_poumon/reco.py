from .compiled_toolboxes import compiled_toolboxes
from .config import acquisition_directories
from .Filter import Filter, filter
from .load_edf import load_edf
from skimage.transform import rescale
import scipy.linalg
import numpy.linalg
import numpy as np
import sys, os

@filter( force = 1, save = 0 )
def make_sphere_projections( shape, pos, radius ):
    res = np.empty( shape )
    for num_angle in range( shape[ 0 ] ):
        angle = num_angle * np.pi / shape[ 0 ]
        cx = 0.5 * shape[ 2 ] + ( pos[ 2 ] - 0.5 * shape[ 2 ] ) * np.cos( angle )
        Y, X = np.meshgrid( np.arange( shape[ 1 ] ) + 0.5 - pos[ 1 ], np.arange( shape[ 2 ] ) + 0.5 - cx, indexing = 'ij' )
        res[ num_angle, :, : ] = np.exp( - np.maximum( 0, radius**2 - ( X**2 + Y**2 ) ) ** 0.5 )
    return res 

@filter( force = 1, save = 0 )
def make_sphere_reco( shape, pos, radius ):
    Z, Y, X = np.meshgrid( 
        np.arange( shape[ 0 ] ) + 0.5 - pos[ 0 ],
        np.arange( shape[ 1 ] ) + 0.5 - pos[ 1 ],
        np.arange( shape[ 2 ] ) + 0.5 - pos[ 2 ],
        indexing = 'ij'
    )
    return ( X**2 + Y**2 + Z**2 < radius**2 ) / radius

@filter( save = 1 )
def load_edf_projections( base, beg_y = 0, len_y = -1, x_scale = 1, a_scale = 1, selection = None, flat = "ref0000_" ):
    """  """

    # find the files
    name = os.path.split( base )[ -1 ]
    tried = []
    for n in acquisition_directories:
        trial = os.path.join( n, base, name ) + "0000.edf"
        if os.path.exists( trial ):
            flat = os.path.join( n, base, flat )
            base = os.path.join( n, base, name )
            break
        tried.append( trial )
    else:
        raise BaseException( f"Unable to find { base }0000.edf (tried { tried })" )

    projections = []
    flat_fields = []
    for n in range( 100000 ):
        i = n
        if not ( selection is None ):
            if n == len( selection ):
                break
            i = selection[ n ]
            print( i )
  
        # proj
        try:
            projections.append( load_edf( base + "{:04d}.edf".format( i ), beg_y, len_y, x_scale ) )
        except IOError:
            break

        # flat
        try:
            flat_fields.append( load_edf( flat + "{:04d}.edf".format( n ), beg_y, len_y, x_scale ) )
        except:
            pass

    # 
    if len( projections ) == 0:
        print( "Unable to read file '" + base + "0000.edf'" )
        sys.exit( 1 )

    if len( flat_fields ) == 0:
        print( "Unable to read file '" + flat + "0000.edf'" )
        sys.exit( 1 )

    # assembly
    n = int( len( projections ) / a_scale )
    R = np.empty( [ n, projections[ 0 ].shape[ 0 ], projections[ 0 ].shape[ 1 ] ], dtype = "float32" )
    for i in range( n ):
        R[ i, :, : ] = np.zeros_like( projections[ a_scale * i ] )
        for s in range( a_scale ):
            R[ i, :, : ] += projections[ a_scale * i + s ]
        R[ i, :, : ] /= a_scale * flat_fields[ 0 ]
    return R


@filter()
def load_edf_projections_gray_levels( base, flat = "ref0000_" ):
    """  """

    # find the files
    name = os.path.split( base )[ -1 ]
    tried = []
    for n in acquisition_directories:
        trial = os.path.join( n, base, name ) + "0000.edf"
        if os.path.exists( trial ):
            flat = os.path.join( n, base, flat )
            base = os.path.join( n, base, name )
            break
        tried.append( trial )
    else:
        raise BaseException( f"Unable to find { base }0000.edf (tried { tried })" )

    projection = None
    flat_field = None
    gray_levels = []
    for n in range( 100000 ):
        print( n )
        # flat
        try:
            flat_field = load_edf( flat + "{:04d}.edf".format( n ) )
        except:
            pass

        # proj
        try:
            projection = load_edf( base + "{:04d}.edf".format( n ) )
        except IOError:
            break

        if flat_field is not None:
            projection /= flat_field

        gray_levels.append( np.linalg.norm( projection ) )

    return np.array( gray_levels )

def normalize( v ):
    return ( v - np.mean( v ) ) / np.std( v )

@filter( force = 1 )
def selection_from_gray_levels( grey_levels, min_f = 100, max_f = 5000, step = 1, approx_period = 130, pos_in_cycle = 0 ):
    f = np.fft.rfft( grey_levels )

    #from matplotlib import pylab
    #pylab.plot( f[ 1: ] )
    #pylab.plot( grey_levels )

    f[ :min_f ] = 0
    f[ max_f: ] = 0
    frey_levels = np.fft.irfft( f )

    cv = normalize( frey_levels )
    ev = np.sign( cv[ 1: ] ) > np.sign( cv[ :-1 ] )

    #pylab.plot( ev[ :5000 ] )
    #pylab.show()

    reference = np.argmax( ev[ : int( approx_period * 1.5 ) ] )
    references = [ reference ]
    while reference + 100 <= len( grey_levels ):
        print( "ref:", reference )
        beg = int( reference + approx_period * 0.1 )
        end = int( reference + approx_period * 1.5 )
        reference = beg + np.argmax( ev[ beg: end ] )
        references.append( reference )

    res = np.array( references )[ ::step ]
    res = np.round( res[ :-1 ] * ( 1 - pos_in_cycle ) + res[ 1: ] * ( 0 + pos_in_cycle ) )
    return res.astype( np.long )


@filter( gpu_ret = True )
def regular_rotation( projection_shape, corner_0 = None, corner_1 = None, off_rx = 0, x_slip = 0, y_slip = 0, z_slip = 0 ):
    """ describes how the full part rotate by giving coordinates of the corners of the cube along the time """
    ct = compiled_toolboxes( "reco_cpu", "reco_gpu" )
    na, ny, nx = projection_shape

    if corner_0 is None:
        corner_0 = ( 0, 0, 0 )

    if corner_1 is None:
        corner_1 = ( nx, ny, nx )

    ax_rot_0 = ( 0.5 * nx, 0.0 * ny, 0.5 * nx + off_rx )
    ax_rot_1 = ( 0.5 * nx, 1.0 * ny, 0.5 * nx + off_rx )
    return ct.part_rotation( corner_0, corner_1, ax_rot_0, ax_rot_1, na, x_slip, y_slip, z_slip )


@filter()
def paganin( proj, coeff ):
    """  """
    if coeff == 0:
        return proj

    nevx = int( np.round( 2 ** np.ceil( np.log2( proj.shape[ 2 ] ) + 1 ) ) )
    nevy = int( np.round( 2 ** np.ceil( np.log2( proj.shape[ 1 ] ) + 1 ) ) )

    grid = np.indices( ( nevy, nevx ) )
    kx   = 2 * np.pi * np.minimum( grid[ 1 ], nevx - grid[ 1 ] ) / nevx
    ky   = 2 * np.pi * np.minimum( grid[ 0 ], nevy - grid[ 0 ] ) / nevy
    cdiv = 1 + coeff * ( kx * kx + ky * ky )

    for i in range( proj.shape[ 0 ] ):
        tmp = np.ones( ( nevy, nevx ) ) * np.mean( proj[ i, :, : ] )
        tmp[ :proj.shape[ 1 ], :proj.shape[ 2 ] ] = proj[ i, :, : ]

        fwdt = np.fft.fft2( tmp[ :, : ] )
        invt = np.fft.ifft2( fwdt / cdiv )
        proj[ i, :, : ] = invt.real[ :proj.shape[ 1 ], :proj.shape[ 2 ] ]

    return proj


@filter()
def back_filter( input ):
    """  """
    input = - np.log( input )

    neve = int( np.round( 2 ** np.ceil( np.log2( input.shape[ 2 ] ) + 1 ) ) )
    cmul = np.indices( ( input.shape[ 1 ], int( neve / 2 ) + 1 ) )[ 1 ]
    for i in range( input.shape[ 0 ] ):
        tmp = np.ones( ( input.shape[ 1 ], neve ) ) * np.mean( input[ i, :, : ] )
        tmp[ :, : input.shape[ 2 ] ] = input[ i, :, : ]

        input[ i, :, : ] = np.fft.irfft( cmul * np.fft.rfft( tmp ) ).real[ :, 0 : input.shape[ 2 ] ]
    return input


@filter( gpu_args = [ "projections", "part_rotation" ], gpu_ret = True )
def reconstruction( projections, part_rotation = None, step_size = 1 ):
    """ integration """
    ct = compiled_toolboxes( "reco_cpu", "reco_gpu" )
    a, y, x = projections.shape

    if part_rotation is None:
        part_rotation = ct.part_rotation( ( 0, 0, 0 ), ( x, y, x ), ( x / 2, 0, x / 2 ), ( x / 2, y, x / 2 ), a, 0, 0, 0 )
    return ct.reconstruction( projections, part_rotation, step_size, 1 )


@filter( gpu_args = [ "part_rotation" ], gpu_ret = True )
def ring_traces( part_rotation, step_size = 1 ):
    """ get effect of projections with a single pixel at 1. We keep only significant results """
    ct = compiled_toolboxes( "reco_cpu", "reco_gpu" )

    cpr = part_rotation
    if Filter._in_gpu( cpr ):
        cpr = Filter._to_cpu( None, cpr, "", False )
    beg_x = max( int( np.min( cpr[ :, :, 2 ] ) ), 0 )
    end_x = int( np.max( cpr[ :, :, 2 ] ) ) + 1

    return ct.ring_traces( part_rotation, beg_x,  end_x, step_size )


@filter( gpu_args = [ "ring_traces" ] )
def ring_corr_factorized_matrix( ring_traces, regul = 1e-4 ):
    """  """
    ct = compiled_toolboxes( "reco_cpu", "reco_gpu" )
    M = ct.ring_corr_matrix( ring_traces, regul )
    return M
    # C, _ = scipy.linalg.cho_factor( M, False )
    # return C


@filter( gpu_args = [ "ring_traces", "reco" ], gpu_ret = True )
def ring_corr_apply( ring_traces, reco, M ):
    """  """
    ct = compiled_toolboxes( "reco_cpu", "reco_gpu" )
    V = ct.ring_corr_vector( ring_traces, reco )
    R = numpy.linalg.solve( M, V )

    res = reco.copy()
    ct.ring_corr_apply( ring_traces, res, R )
    return res

def ring_corr( reco, part_rotation, regul, step_size = 1 ):
    """  """
    rgtr = ring_traces( part_rotation, step_size )
    return ring_corr_apply( rgtr, reco, ring_corr_factorized_matrix( rgtr, regul ) )


@filter( gpu_args = [ "reco" ], gpu_ret = True )
def binarisation_max_disc( reco, threshold, radius ):
    """  """
    ct = compiled_toolboxes( "reco_cpu", "reco_gpu" )
    return ct.binarisation_max_disc( reco, threshold, int( radius ) )


@filter()
def max_median_on_discs( reco, radius, thickness = 0 ):
    """  """
    ct = compiled_toolboxes( "reco_cpu" )
    return ct.max_median_on_discs( reco, int( radius ), int( thickness ) )

def reco_msh( msh = "src/tomo_poumon/cpp/lib/exp_disc.stl" ):
    ct = compiled_toolboxes( "reco_msh" )
    msh = ct.Msh( msh )
    
@filter( save = 0, force = 1 )
def proj_cg( shape, reco, base_mesh, base_nodes ):
    ct = compiled_toolboxes( "reco_cg" )
    return ct.proj( reco, np.empty( shape ), base_mesh[ "elements" ], base_nodes )

@filter( save = 0, force = 1 )
def reco_cg( projections, base_mesh, base_nodes, space_scale = 1, nb_level_multiscale = 3 ):
    ct = compiled_toolboxes( "reco_cg" )

    min_pos_reco = np.array( [ np.min( base_nodes[ 0, :, d ] ) for d in range( 3 ) ] )
    max_pos_reco = np.array( [ np.max( base_nodes[ 0, :, d ] ) for d in range( 3 ) ] )

    reco_shape = [ int( ( max_pos_reco[ d ] - min_pos_reco[ d ] ) / space_scale ) for d in range( 3 ) ]
    reco_act = np.zeros( reco_shape, dtype = np.float )
    projections = - np.log( projections ) #  + 1e-6

    from .VisuClient import VisuClient
    visu_client = VisuClient()

    cpt = 0
    for level_multiscale in range( nb_level_multiscale - 1, -1, -1 ):
        print( "level_multiscale", level_multiscale, projections.shape[ 0 ] )

        beg_n = projections.shape[ 0 ] * ( level_multiscale + 0 )
        end_n = projections.shape[ 0 ] * ( level_multiscale + 1 )
        nodes = base_nodes[ beg_n : end_n, :, : ]

        reco_dirs = [ np.ones_like( reco_act ) ]
        proj_dirs = [ ct.proj( reco_dirs[ 0 ], projections, base_mesh[ "elements" ], nodes ) ]
        for i in range( 4 ):
            reco_dirs.append( np.zeros_like( reco_act ) )
            proj_dirs.append( np.zeros_like( projections ) )

        # 20 0.010070743107927799 [0.14618005 0.1949003  0.03311596] 0.049505383
        # 3 => 0.0028547086508402823 [0.1436598  0.05685046 0.04000768 0.01183433] 0.038226604
        # 4 => 20 0.000596170615164399 [0.47207053 0.08791817 0.37575487 0.08806433 0.05469045] 0.028848832
        # 5 => 0.0001213323516927883 [0.728626   0.5194851  0.16156582 0.15465766 0.04128221 0.0094315 ] 0.025594661

        for num_iter in range( 21 ):
            proj_act = projections - ct.proj( reco_act, projections, base_mesh[ "elements" ], nodes )

            reco_dir = ct.rdir( reco_act, proj_act, base_mesh[ "elements" ], nodes )
            proj_dir = ct.proj( reco_dir, proj_act, base_mesh[ "elements" ], nodes )

            reco_dirs = [ reco_dir ] + reco_dirs[ :-1 ]
            proj_dirs = [ proj_dir ] + proj_dirs[ :-1 ]

            visu_client.send_image( "reco", reco_act[ :, 0, : ] )
            visu_client.send_image( "pdir", reco_dir[ :, 0, : ] )

            M = np.empty( ( len( proj_dirs ), len( proj_dirs ) ) )
            V = np.empty( ( len( proj_dirs ), ) )
            for i in range( len( proj_dirs ) ):
                V[ i ] = np.dot( proj_dirs[ i ].flatten(), proj_act.flatten() )
                for j in range( i + 1 ):
                    val = np.dot( proj_dirs[ i ].flatten(), proj_dirs[ j ].flatten() )
                    M[ i, j ] = val
                    if i != j:
                        M[ j, i ] = val
                    else:
                        M[ j, i ] += 1e-9

            R = np.linalg.solve( M, V )
            print( num_iter, M[ 0, 0 ], R, np.linalg.norm( proj_act ) )

            for i, reco_dir in enumerate( reco_dirs ):
                reco_act += R[ i ] * reco_dir
                # proj_act -= R[ 0 ] * proj_dir + R[ 1 ] * proj_old

            # from matplotlib import pyplot
            # pyplot.imsave( 'reco_{}.png'.format( cpt ), reco_act[ :, 0, : ], cmap = "gray" )
            # pyplot.imsave( 'rdir_{}.png'.format( cpt ), reco_dir[ :, 0, : ], cmap = "gray" )
            # pyplot.imsave( 'proj_{}.png'.format( cpt ), proj_act[ :, 0, : ], cmap = "gray" )
            # cpt += 1

        if level_multiscale:
            min_x = int( reco_shape[ 2 ] * 1 / 4 )
            max_x = int( reco_shape[ 2 ] * 3 / 4 )
            min_z = int( reco_shape[ 0 ] * 1 / 4 )
            max_z = int( reco_shape[ 0 ] * 3 / 4 )

            reco_act = rescale( reco_act[ min_z : max_z, :, min_x : max_x ], ( 2, 1, 2 ), multichannel=False )

    return reco_act


