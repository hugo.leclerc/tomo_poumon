from .compiled_toolboxes import compiled_toolboxes
from .config import output_directory, info
from .Cache import cache_cpu, cache_gpu
from .cuda import check_cuda
import os, time, inspect, urllib, sys
import numpy as np
import pickle

class Filter:
    """
    """

    def __init__( self, func, kargs, save, cache, force, gpu_args, gpu_ret, name, use_pickle ):
        """ 
            save    : 0 => never save the result. 1 => save if it takes more than 0.2 seconds. 2 => always save
            cache   : 0 => never save in cache. 1 => always save
            force   : 0 => make the computation if an input has changed. 1 => always do the computation
            gpu_args: [ name of arguments expected to be in the gpu ]. argument "" describes the return value.
        """ 
        self.use_pickle = use_pickle
        self.gpu_args   = gpu_args
        self.gpu_ret    = gpu_ret
        self.kargs      = kargs
        self.cache      = cache
        self.force      = force
        self.save       = save
        self.func       = func
        self.name       = name

    @property
    def shape( self ):
        fn = os.path.join( output_directory, self.dirname(), "value" + self._save_ext() )

        # in a cache ?
        v = cache_cpu.get( fn ) or cache_gpu.get( fn )
        if v:
            return v.value.shape
  
        # in the disk ?
        if os.path.exists( fn ) and not self.force:
            return self._load_func( fn ).shape

        # else, make it
        return self.value( self.gpu_ret ).shape


    def dirname( self ):
        dir = []
        res = self.name
        for k, v in sorted( self.kargs.items() ):
            if isinstance( v, Filter ):
                dir.append( v.dirname() )
            elif isinstance( v, list ) or isinstance( v, tuple ) or isinstance( v, np.ndarray ):
                res += "_" + str( len( v ) )
                for i in v:
                    res += "_" + str( i )
            elif isinstance( v, float ):
                s = str( v )
                if s.endswith( '.0' ):
                    s = s[ : -2 ]
                res += "_" + s
            else:
                res += "_" + urllib.parse.quote_plus( str( v ) )
        return os.path.join( *dir, res )


    def _to_gpu( self, data, msg, disp_msg = True ):
        if disp_msg:
            info( "sending {} bytes to the gpu".format( sys.getsizeof( data ) ) + msg )
        ct = compiled_toolboxes( "GpuVol" )
        if type( data ) == tuple:
            return tuple( ct.to_gpu( item ) for item in data )
        if type( data ) == dict:
            return dict( ( name, ct.to_gpu( item ) ) for name, item in data.items() )
        return ct.to_gpu( data )

    def _to_cpu( self, data, msg, disp_msg = True ):
        if disp_msg:
            info( "sending {} bytes to the cpu".format( sys.getsizeof( data ) ) + msg )
        if type( data ) == tuple:
            return tuple( item.to_cpu() for item in data )
        if type( data ) == dict:
            return dict( ( name, item.to_cpu() ) for name, item in data.items() )
        return data.to_cpu()


    def value( self, gpu = False ):
        """ gpu => where we want to find the data """

        fn = os.path.join( output_directory, self.dirname(), "value" + self._save_ext() )
        cuda = check_cuda()
        if not cuda:
            gpu = False

        # already in the cache ?
        if self.cache and not self.force:
            if gpu: # we want the result in the GPU
                v = cache_gpu.get( fn )
                if v:
                    info( "{} is in the gpu cache".format( fn ) )
                    return v.value
                v = cache_cpu.get( fn )
                if v:
                    info( "{} is in the cpu cache".format( fn ) )
                    res = self._to_gpu( v.value, " (transfer from cpu cache)" )
                    if self.cache:
                        cache_gpu.set( fn, res )
                    return res
            else:
                v = cache_cpu.get( fn )
                if v:
                    info( "{} is in the cpu cache".format( fn ) )
                    return v.value
                v = cache_gpu.get( fn )
                if v:
                    info( "{} is in the gpu cache".format( fn ) )
                    res = self._to_cpu( v.value, " (transfer from gpu cache)" )
                    if self.cache:
                        return cache_cpu.set( res )
                    return res

        # already in the disk ?
        if os.path.exists( fn ) and not self.force:
            res = self._load_func( fn )
            info( "{} loaded from disk".format( fn ) )
            if gpu:
                res = self._to_gpu( res, " (disk -> gpu)" )
            if self.cache:
                c = cache_gpu if gpu else cache_cpu
                c.set( fn, res )
            return res

        # get argument values (and send them to cpu/gpu if necessary)
        def arg_val( k, v ):
            if isinstance( v, Filter ):
                return v.value( k in self.gpu_args )
            # gpu -> cpu
            if Filter._in_gpu( v ) and k not in self.gpu_args:
                return self._to_cpu( v, f" (for arg { k })" )
            # cpu -> gpu
            if cuda and k in self.gpu_args and not Filter._in_gpu( v ):
                return self._to_gpu( v, f" (for arg { k })" )
            return v

        l_kargs = {}
        for k, v in self.kargs.items():
            l_kargs[ k ] = arg_val( k, v )

        # make it
        info( "{} in construction".format( fn ) )
        b = time.monotonic()
        res = self.func( **l_kargs )
        e = time.monotonic()
        info( "{} done ({} seconds)".format( fn, e - b ) )

        # by default, store in disk if time to get the result is > 0.2 seconds
        store_in_disk = self.save == 2 or ( self.save == 1 and e - b > 0.2 )

        # cache/save/transfer
        if Filter._in_gpu( res ):
            if self.cache:
                cache_gpu.set( fn, res )
            if store_in_disk or not gpu:
                cpu = self._to_cpu( res, " (transfer gpu result)" )
                if store_in_disk:
                    self._save_func( fn, cpu )
                if self.cache:
                    cache_cpu.set( fn, cpu )
                if not gpu:
                    res = cpu
        else:
            if self.cache:
                cache_cpu.set( fn, res )
            if store_in_disk:
                self._save_func( fn, res )
            if gpu:
                res = self._to_gpu( res, " (transfer cpu result)" )
                if self.cache:
                    cache_gpu.set( fn, res )

        return res

    def _save_ext( self ):
        """ extenstion when save in disk. may be redefined """
        if self.use_pickle:
            return ".pkl"
        return ".npy"

    def _save_func( self, filename, data ):
        """ function to save value in disk. may be redefined """
        os.makedirs( os.path.dirname( filename ), exist_ok = True )
        info( "{} in disk".format( filename ) )
        if self.use_pickle:
            with open( filename, 'wb' ) as f:
                pickle.dump( data, f, protocol=4 )
        else:
            np.save( filename, data, allow_pickle = True )

    def _load_func( self, filename ):
        """ how to load data from disk. may be redefined """
        if self.use_pickle:
            with open( filename, 'rb') as f:
                ret_di = pickle.load( f )
            return ret_di
        else:
            return np.load( filename, allow_pickle = True )

    @staticmethod
    def _in_gpu( val ):
        return "Gpu" in type( val ).__name__

class filter:
    def __init__( self, save = 1, cache = 1, force = 0, gpu_args = [], gpu_ret = False, name = None, output_type = Filter, use_pickle = True ):
        """
            save: 0 => never save the result. 1 => save if it takes more than 0.2 seconds. 2 => always save
            cache: 0 => never save in cache. 1 => always save
            force: 0 => make the computation if an input has changed. 1 => always do the computation
            gpu_args: [ name of arguments expected to be in the gpu ]
        """
        self.output_type = output_type
        self.use_pickle  = use_pickle
        self.gpu_args    = gpu_args
        self.gpu_ret     = gpu_ret
        self.cache       = cache
        self.force       = force
        self.name        = name
        self.save        = save

    def __call__( self, func ):
        def wrapper( *args, **kargs ):
            if args:
                l = list( inspect.signature( func ).parameters )
                for i in range( len( args ) ):
                    kargs[ l[ i ] ] = args[ i ]
            name = self.name or func.__name__
            return self.output_type( func, kargs, name = name, save = self.save, cache = self.cache, force = self.force, gpu_args = self.gpu_args, gpu_ret = self.gpu_ret, use_pickle = self.use_pickle )
        return wrapper



