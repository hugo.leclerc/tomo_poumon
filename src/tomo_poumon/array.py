from .Filter import filter
import numpy as np

@filter( save = 0, cache = 0 )
def cst_val( size = [ 400, 400 ], value = 0 ):
    return np.full( size, value )

@filter( save = 0, cache = 0 )
def exp_neg( arr ):
    return np.exp( - arr )
