from .config import info
import sys

class CacheItem:
    def __init__( self, value, date, size ):
        self.value = value 
        self.date = date
        self.size = size


class Cache:
    def __init__( self, max_mem = 16 * 1024**3, name = "cpu" ):
        self.max_mem = max_mem
        self.items = {} # name => CacheItem
        self.name = name
        self.date = 0
        self.size = 0

    def get( self, name ): 
        """ return a CacheItem or None """
        if name in self.items:
            info( "{} found in {} cache".format( name, self.name, self.size ) )
            self.date += 1

            res = self.items[ name ]
            res.date = self.date
            return res
        return None

    def set( self, name, value ):
        # remove old value
        if name in self.items:
            self.size -= self.items[ name ].size
            self.items.pop( name )
            info( "{} replaced in {} cache".format( name, self.size, self.name ) )

        # preparation to add item
        item_size = sys.getsizeof( value )
        self.size += item_size
        self.date += 1

        # else, clear enough items in the cache
        while self.size > self.max_mem and self.items:
            kv = min( self.items.items(), key = lambda kv: kv[ 1 ].date )
            self.size -= kv[ 1 ].size
            self.items.pop( kv[ 0 ] )
            info( "{} rem from {} cache (size={})".format( kv[ 0 ], self.name, kv[ 1 ].size ) )

        # add it
        self.items[ name ] = CacheItem( value, self.date, item_size )
        info( "{} now in {} cache (size={})".format( name, self.name, self.size ) )

        return value

    def clear( self ):
        self.items.clear()
        self.size = 0

    def compute_size( self ):
        res = 0
        for v in self.items.values():
            res += v.size
        return res

cache_cpu = Cache( max_mem = 9 * 1024**3, name = "cpu" )
cache_gpu = Cache( max_mem = 6 * 1024**3, name = "gpu" )
