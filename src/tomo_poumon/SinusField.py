from tomo_poumon import VolumeFilter
import numpy as np


class SinusField( VolumeFilter ):
    def __init__( self, period, lx, ly, lz = 0 ):
        def make_sin( period, lx, ly, lz ):
            if lz:
                x, y, z = np.meshgrid( 1 + np.arange( lx ), 1 + np.arange( ly ), 1 + np.arange( lz ) )
                return np.sin( x * np.pi / period ) * np.sin( y * np.pi / period ) * np.sin( z * np.pi / period )
            x, y = np.meshgrid( np.arange( lx ), np.arange( ly ) )
            return np.sin( x * np.pi / period ) * np.sin( y * np.pi / period )
        super().__init__( make_sin, period = period, lx = lx, ly = ly, lz = lz, save = False )
