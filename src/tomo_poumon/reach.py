from .Filter import filter
import numpy as np

@filter()
def reach( input, step = 1 ):
    """ vector of nb voxels such as value > step * i... input must be a dist volume """
    v = np.arange( 0, np.max( input ) + 1e-6, step )
    r = np.empty( ( v.size, 2 ) )
    for i in range( v.size ):
        r[ i, 0 ] = v[ i ]
        r[ i, 1 ] = np.sum( np.logical_and( input >= 0, input <= v[ i ] ) )
    return r
