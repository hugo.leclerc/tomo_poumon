from .load_projections import load_projections
from .VolumeFilter import VolumeFilter

class ProjectionLoader( VolumeFilter ):
    """ ... """
    def __init__( self, name, scale_xy = 1, scale_a = 1 ):
        super().__init__( load_projections, [], name = name, scale_xy = scale_xy, scale_a = scale_a )
