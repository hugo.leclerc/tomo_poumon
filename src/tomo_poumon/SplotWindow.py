import pyqtgraph.opengl as gl
from PyQt5.Qt import Qt
import numpy as np
import pyqtgraph


class SplotWindow( gl.GLViewWidget ):
    def __init__( self, connection, name ):
        super().__init__()

        self.setAttribute( Qt.WA_ShowWithoutActivating )
        self.setWindowFlags( Qt.WindowStaysOnTopHint )
        self.setWindowTitle( name )

        self.plot_item = None

        self.connection = connection
        self.name = name

    def keyPressEvent( self, event ):
        if event.key() == Qt.Key_Q:
            self.connection.quit()
            return

        super().keyPressEvent( event )

    def setData( self, img ):
        if not self.plot_item:
            g = gl.GLGridItem()
            g.scale( 0.25, 0.25, 1 )
            g.setDepthValue( 10 ) # draw grid after surfaces since they may be translucent
            self.addItem( g )

            self.plot_item = gl.GLSurfacePlotItem( shader = 'shaded' ) # ( x = np.arange( img.shape[ 1 ] ), y = np.arange( img.shape[ 0 ] ), z = img, shader = 'shaded' )
            self.plot_item.scale( 5 / img.shape[ 1 ], 5 / img.shape[ 0 ], 5 / np.max( img ) )
            self.plot_item.translate( -2.5, -2.5, - 5 * np.min( img ) / np.max( img ) )
            self.addItem( self.plot_item )

        self.plot_item.setData( x = np.arange( img.shape[ 0 ] ), y = np.arange( img.shape[ 1 ] ), z = img )
