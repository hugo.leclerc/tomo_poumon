from .                import reconstruction_toolbox_cpu as rt_cpu
from .                import reconstruction_toolbox_gpu as rt_gpu
from .load_or_execute import load_or_execute, output_filename
from .back_filter     import back_filter
from .info_rat        import PathInfo

import time, sys, os
import numpy as np

# def make( inp_filename ):
def make_ring( reco, nb_angles = 1500, nb_rings_per_step = 65, ring_range = ( 0.0, 1.0 ) ):
    pi = PathInfo( os.path.dirname( reco ) )
    print( pi )
    print( pi.threshold() )

    # b = int( pi.tot_x * ring_range[ 0 ] )
    # e = int( pi.tot_x * ring_range[ 1 ] )
    # e += e == b

    # # 
    # reco = np.load( reco )
    # ring = reco * 1.0
    # for beg_ring in range( b, e, int( 0.75 * nb_rings_per_step ) ):
    #     print( beg_ring, time.time() )

    #     # make the ring traces
    #     rings = np.zeros( ( pi.len_x, nb_rings_per_step, pi.len_z ), dtype = np.float32 )
    #     for off_ring in range( max( beg_ring, 0 ), min( beg_ring + nb_rings_per_step, pi.tot_x ) ):
    #         proj = np.zeros( ( nb_angles, 1, pi.tot_x ), dtype = np.float32 )
    #         proj[ :, 0, off_ring ] = 1

    #         proj = bf.back_filter( proj )

    #         rota = rt_cpu.make_rotations(
    #             [ 0.0 * proj.shape[ 2 ], 0.0 * proj.shape[ 1 ], 0.0 * proj.shape[ 2 ] ],
    #             [ 1.0 * proj.shape[ 2 ], 1.0 * proj.shape[ 1 ], 1.0 * proj.shape[ 2 ] ],
    #             [ pi.x_rot             , 0.0 * proj.shape[ 1 ], 0.5 * proj.shape[ 2 ] ],
    #             [ pi.x_rot             , 1.0 * proj.shape[ 1 ], 0.5 * proj.shape[ 2 ] ],
    #             proj.shape[ 0 ]        , 0, 0, 0
    #         )

    #         beg_x = pi.cnt_x - 0.5 * pi.len_x
    #         beg_z = pi.cnt_z - 0.5 * pi.len_z

    #         #
    #         o = off_ring - beg_ring
    #         proj_gpu = rt_gpu.new_gpu_vol( proj )
    #         rings[ :, o : o + 1, : ] = rt_gpu.reconstruct( proj_gpu, rota, 
    #             beg_x, 1, pi.len_x,
    #             0.5  , 1, 1       ,
    #             beg_z, 1, pi.len_z,
    #             0
    #         )
    #         rt_gpu.del_gpu_vol( proj_gpu )

    #     # optimize the reconstruction
    #     for y in range( 0, pi.len_y ):
    #         ring[  :, y, : ] = rt_gpu.optimize_reco( ring[  :, y, : ], rings, 4e-14 )

    # # save
    # rt_cpu.save_xdmf( ring, path + "/" + dest )
    # np.save( path + "/" + dest + ".npy", ring )

    # return ring

def ring( inp_filename, name = "ring" ):
    return load_or_execute( os.path.dirname( inp_filename ), [], name, inp_filename )
