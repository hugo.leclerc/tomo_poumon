import numpy as np
import os

from . import config

def to_str( val ):
    if isinstance( val, list ):
        return "_".join( map( to_str, val ) )
    return str( val )

def output_filename( experiment, parameters, name, absolute = True ):
    filename = experiment
    for i in range( 0, len( parameters ), 2 ):
        filename = os.path.join( filename, parameters[ i + 0 ] + "_" + to_str( parameters[ i + 1 ] ) )
    if name:
        if len( os.path.splitext( name )[ 1 ] ) == 0:
            name += '.npy'
        filename = os.path.join( filename, name )

    if absolute and not os.path.exists( filename ):
        filename = os.path.join( config.output_directory, filename )
        os.makedirs( os.path.dirname( filename ), exist_ok = True )

    return filename

def load_or_execute( experiment, parameters, name, func, *args ):
    """ try to load and return a file (assumed to be a .npy for now). If this file does not exist, create the date using func and args, and save it under filename """
    filename = output_filename( experiment, parameters, name )

    try:
        print( "\rTrying to Load {}".format( filename ) )
        result = np.load( filename )
    except:
        print( "\r  => Make {}".format( filename ) )
        result = func( *args )

        print( "\r  => Done {}".format( filename ) )
        np.save( filename, result )

    return result
