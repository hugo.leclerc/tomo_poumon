from PyQt5.QtWidgets import QApplication, QLabel
from PyQt5.Qt import Qt, QIODevice, QFont
import json


class ParamWindow( QLabel ):
    def __init__( self, connection, param_dict ):
        # qlabel
        super().__init__( "<h1>Parameters</h1>" )
        self.setFont( QFont( "Courier" ) )
        self.setMargin( 10 )

        # attributes
        self.connection = connection
        self.focus_var = ""
        self.mod_var = ""
        self.mod_str = ""
        self.params = {}

        # update params
        for k, v in param_dict.items():
            self.params[ k ] = {
                "center"   : v[ "center" ],
                "sampling" : v[ "sampling" ],
                "increment": v[ "width" ] / v[ "sampling" ],
            }

        # draw
        self.update_text()

    def update_text( self ):
        # updates (focus_var)
        if len( self.params ) and not ( self.focus_var in self.params ):
            self.focus_var = next( iter( self.params ) )

        #
        txt = ""

        # styles
        txt += r"<style>"
        txt += r"  .selected { background-color: blue }"
        txt += r"</style>"

        txt += "<h3>Parameters</h3>"
        txt += "<table cellpadding=2>"
        txt += "  <thead>"
        txt += "    <tr>"
        txt += "      <th>Name</th>"
        txt += "      <th>Value</th>"
        txt += "      <th>Increment</th>"
        txt += "    </tr>"
        txt += "  </thead>"
        txt += "  <tbody>"
        for name, param in self.params.items():
            txt += "<tr>"
            txt += "  <td>{}</td>".format( name )
            if name == self.focus_var:
                if name == self.mod_var:
                    txt += "  <td>{}<span class='selected'>&nbsp;</span></td>".format( self.mod_str )
                else:
                    txt += "  <td><span class='selected'>{}</span></td>".format( param_center_str( param ) )
            else:
                txt += "  <td>{}</td>".format( param_center_str( param ) )
            txt += "  <td>{}</td>".format( param[ "increment" ] )
            txt += "</tr>"
        txt += "  </tbody>"
        txt += "</table>"

        txt += "<h3>Bindings</h3>"

        txt += "<table cellpadding=2>"
        txt += "  <tbody>"
        txt += "    <tr><td></td>[0..9e+-.] </td><td> change the value of the current variable</td></tr>"
        txt += "    <tr><td></td>ctrl+right </td><td> change current increment</td></tr>"
        txt += "    <tr><td></td>ctrl+left  </td><td> change current increment</td></tr>"
        txt += "    <tr><td></td>enter      </td><td> validate the changes</td></tr>"
        txt += "    <tr><td></td>right      </td><td> increment the current variable</td></tr>"
        txt += "    <tr><td></td>left       </td><td> decrement the current variable</td></tr>"
        txt += "    <tr><td></td>down       </td><td> go to next variable</td></tr>"
        txt += "    <tr><td></td>up         </td><td> go to previous variable</td></tr>"
        txt += "    <tr><td></td>o          </td><td> optimize a parameter</td></tr>"
        txt += "    <tr><td></td>r          </td><td> reset the plots</td></tr>"
        txt += "    <tr><td></td>q          </td><td> quit</td></tr>"
        # txt += "    <tr><td></td>c        </td><td> hide the curves</td></tr>"
        # txt += "    <tr><td></td>C        </td><td> show the curves</td></tr>"
        txt += "  </tbody>"
        txt += "</table>"

        self.setText( txt )

    def keyPressEvent( self, event ):
        if event.key() == Qt.Key_Q:
            self.connection.quit()
            return

        if event.key() == Qt.Key_Left and event.modifiers() & Qt.ControlModifier:
            self.params[ self.focus_var ][ "increment" ] /= 2
            self.update_text()
            return
        if event.key() == Qt.Key_Right and event.modifiers() & Qt.ControlModifier:
            self.params[ self.focus_var ][ "increment" ] *= 2
            self.update_text()
            return

        if event.key() == Qt.Key_Down:
            n = min( list( self.params ).index( self.focus_var ) + 1, len( self.params ) - 1 )
            self.focus_var = list( self.params )[ n ]
            self.validate_mod()
            return
        if event.key() == Qt.Key_Up:
            n = max( list( self.params ).index( self.focus_var ) - 1, 0 )
            self.focus_var = list( self.params )[ n ]
            self.validate_mod()
            return

        if event.key() == Qt.Key_Left:
            self.params[ self.focus_var ][ "center" ] -= self.params[ self.focus_var ][ "increment" ]
            self.send_parameters()
            self.update_text()
            return
        if event.key() == Qt.Key_Right:
            self.params[ self.focus_var ][ "center" ] += self.params[ self.focus_var ][ "increment" ]
            self.send_parameters()
            self.update_text()
            return

        if event.key() in [ Qt.Key_Enter, Qt.Key_Return ]:
            self.validate_mod()
            self.send_parameters()
            return

        if event.key() == Qt.Key_Backspace:
            if not self.mod_str:
                self.mod_str = param_center_str( self.params[ self.focus_var ] )
                self.mod_var = self.focus_var
            self.mod_str = self.mod_str[ :-1 ]
            self.update_text()
            return

        if event.key() in [ Qt.Key_0, Qt.Key_1, Qt.Key_2, Qt.Key_3, Qt.Key_4, Qt.Key_5, Qt.Key_6, Qt.Key_7, Qt.Key_8, Qt.Key_9, Qt.Key_E, Qt.Key_Minus, Qt.Key_Plus, Qt.Key_Period ]:
            if self.mod_var != self.focus_var:
                self.mod_var = self.focus_var
            self.mod_str += event.text()
            self.update_text()
            return

        if event.key() == Qt.Key_R:
            self.connection.send( "reset_plots" )
            return

        # print( "unhandled key " + str( event.key() ) )

        
    def validate_mod( self ):
        if self.mod_var:
            if self.mod_str:
                self.params[ self.mod_var ][ "center" ] = float( self.mod_str )
                self.mod_str = ""
            self.mod_var = ""
        self.update_text()
        
    def send_parameters( self ):
        params = {}
        for k, v in self.params.items():
            params[ k ] = { "center": v[ "center" ], "width": v[ "increment" ] * v[ "sampling" ], "sampling": v[ "sampling" ], "index": 0 }
        self.connection.send( "params " + json.dumps( params ) )


def param_center_str( param ):
    v = param[ "center" ]
    if abs( v - round( v ) ) <= 1e-12 * round( v ):
        return str( int( round( v ) ) )
    return str( v )
