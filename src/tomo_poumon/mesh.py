from .Filter import filter
import numpy as np

@filter( force = 0 )
def make_box( min_point, max_point, density ):
    import gmsh

    gmsh.initialize()
    gmsh.option.setNumber( "General.Terminal", 1 )

    model = gmsh.model
    factory = model.geo

    factory.addPoint( max_point[ 2 ], max_point[ 1 ], max_point[ 0 ], density )
    factory.addPoint( max_point[ 2 ], max_point[ 1 ], min_point[ 0 ], density )
    factory.addPoint( min_point[ 2 ], max_point[ 1 ], max_point[ 0 ], density )
    factory.addPoint( min_point[ 2 ], min_point[ 1 ], max_point[ 0 ], density )
    factory.addPoint( max_point[ 2 ], min_point[ 1 ], max_point[ 0 ], density )
    factory.addPoint( max_point[ 2 ], min_point[ 1 ], min_point[ 0 ], density )
    factory.addPoint( min_point[ 2 ], max_point[ 1 ], min_point[ 0 ], density )
    factory.addPoint( min_point[ 2 ], min_point[ 1 ], min_point[ 0 ], density )

    factory.addLine( 3, 1 )
    factory.addLine( 3, 7 )
    factory.addLine( 7, 2 )
    factory.addLine( 2, 1 )
    factory.addLine( 1, 5 )
    factory.addLine( 5, 4 )
    factory.addLine( 4, 8 )
    factory.addLine( 8, 6 )
    factory.addLine( 6, 5 )
    factory.addLine( 6, 2 )
    factory.addLine( 3, 4 )
    factory.addLine( 8, 7 )

    factory.addCurveLoop( [ -6, -5, -1, 11 ], 13 )
    factory.addPlaneSurface( [ 13 ], 14 )

    factory.addCurveLoop( [ 4, 5, -9, 10 ], 15 )
    factory.addPlaneSurface( [ 15 ], 16 )

    factory.addCurveLoop( [ -3, -12, 8, 10 ], 17 )
    factory.addPlaneSurface( [ 17 ], 18 )

    factory.addCurveLoop( [ 7, 12, -2, 11 ], 19 )
    factory.addPlaneSurface( [ 19 ], 20 )

    factory.addCurveLoop( [ -4,-3,-2,1 ], 21 )
    factory.addPlaneSurface( [ 21 ], 22 )

    factory.addCurveLoop( [ 8, 9, 6, 7 ], 23 )
    factory.addPlaneSurface( [ 23 ], 24 )

    factory.addSurfaceLoop( [ 14,24,-18,22,16,-20 ], 25 )
    factory.addVolume( [ 25 ], 26 )

    factory.synchronize()

    gmsh.model.mesh.generate( 3 )

    nodes = np.empty([])
    elements = np.empty([])
    elementTypes, elementTags, nodeTags = model.mesh.getElements( 3 )
    for num_type_elem, type_elem in enumerate( elementTypes ):
        nodes = []
        node_corr = {} # node tag => node number
        elements = np.empty( [ elementTags[ num_type_elem ].size, 4 ], dtype = np.int )
        for n, elem_tag in enumerate( elementTags[ num_type_elem ] ):
            l = []
            for node_tag in gmsh.model.mesh.getElement( elem_tag )[ 1 ]:
                if node_tag not in node_corr:
                    node_corr[ node_tag ] = len( nodes )
                    nodes.append( gmsh.model.mesh.getNode( node_tag )[ 0 ] )
                l.append( node_corr[ node_tag ] )

            # check orientation
            p0 = np.array( nodes[ l[ 0 ] ] )
            p1 = np.array( nodes[ l[ 1 ] ] ) - p0
            p2 = np.array( nodes[ l[ 2 ] ] ) - p0
            p3 = np.array( nodes[ l[ 3 ] ] ) - p0
            me = p1[ 2 ] * ( p2[ 1 ] * p3[ 0 ] - p3[ 1 ] * p2[ 0 ] ) - p1[ 1 ] * ( p2[ 2 ] * p3[ 0 ] - p3[ 2 ] * p2[ 0 ] ) + p1[ 0 ] * ( p2[ 2 ] * p3[ 1 ] - p3[ 2 ] * p2[ 1 ] )
            if me < 0:
                l[ 2 ], l[ 3 ] = l[ 3 ], l[ 2 ]

            # store
            elements[ n, : ] = l

        nodes = np.array( nodes )

    # gmsh.write( "src/tomo_poumon/cpp/lib/exp_disc.stl" )
    # gmsh.fltk.run()
    gmsh.finalize()

    return { "nodes": nodes, "elements": elements }

@filter()
def make_rotations( nodes, angles, rot_0 = None, rot_1 = None, off_rot = 0, beg_angle = 0, end_angle = np.pi, nb_scales = 1 ):
    """ angles can be a number (=> nb angles), an 1D array (angle values) or a nD array with n > 1 (nb_angles = angles.shape[0]) """
    if type( nodes ) == dict:
        nodes = nodes[ "nodes" ]

    if rot_0 is None:
        rot_0 = np.array( [ angles.shape[ 2 ] / 2, 0, angles.shape[ 2 ] / 2 + off_rot ] )
    if rot_1 is None:
        rot_1 = rot_0 + np.array( [ 0, 1, 0 ] )

    if isinstance( angles, np.ndarray ) and angles.ndim > 1:
        angles = angles.shape[ 0 ]
    if isinstance( angles, int ):
        angles = np.linspace( beg_angle, end_angle, angles, endpoint = False )

    min_p = np.array( [ np.min( nodes[ :, d ] ) for d in range( 3 ) ] )
    max_p = np.array( [ np.max( nodes[ :, d ] ) for d in range( 3 ) ] )
    cnt_p = 0.5 * ( min_p + max_p )

    res = np.empty( ( angles.size * nb_scales, nodes.shape[ 0 ], nodes.shape[ 1 ] ) )
    for num_scale in range( nb_scales ):
        for num_angle, theta in enumerate( angles ):
            # rotation axis
            p1 = rot_1[ 0 ] - rot_0[ 0 ]
            p2 = rot_1[ 1 ] - rot_0[ 1 ]
            p3 = rot_1[ 2 ] - rot_0[ 2 ]
            n = np.sqrt( p1 * p2 + p2 * p2 + p3 * p3 )
            p1 /= n; p2 /= n; p3 /= n

            # rotation matrix
            co = np.cos( theta )
            si = np.sin( theta )
            rot_m = np.array( [
                [ co + ( 1 - co ) * p1 * p1     , ( 1 - co ) * p1 * p2 - si * p3, ( 1 - co ) * p1 * p3 + si * p2 ],
                [ ( 1 - co ) * p2 * p1 + si * p3, co + ( 1 - co ) * p2 * p2     , ( 1 - co ) * p2 * p3 - si * p1 ],
                [ ( 1 - co ) * p3 * p1 - si * p2, ( 1 - co ) * p3 * p2 + si * p1, co + ( 1 - co ) * p3 * p3      ],
            ] )
            for num_node, node in enumerate( nodes ):
                pos = cnt_p + ( node - cnt_p ) * 2**num_scale
                pos[ 1 ] = node[ 1 ]
                
                res[ num_scale * angles.size + num_angle, num_node, : ] = rot_0 + rot_m @ ( pos - rot_0 )

    return res


def vtk_output( filename, nodes, elements ):
    from pyevtk.hl import unstructuredGridToVTK
    from pyevtk.vtk import VtkTetra
    import numpy as np
    
    unstructuredGridToVTK(
        filename, 
        np.ascontiguousarray( nodes[ :, 2 ] ),
        np.ascontiguousarray( nodes[ :, 1 ] ), 
        np.ascontiguousarray( nodes[ :, 0 ] ), 
        connectivity = elements.flatten(),
        offsets = 4 + 4 * np.arange( elements.shape[ 0 ], dtype = np.int ), 
        cell_types = np.ones( elements.shape[ 0 ], dtype = np.int ) * VtkTetra.tid, 
        cellData = None, 
        pointData = None
    )


