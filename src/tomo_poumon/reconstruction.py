from .compiled_toolboxes import compiled_toolboxes

def reconstruction( input, cx, cy, cz, lx, ly, lz, x_slip, y_slip, z_slip, x_off, inter_order = 1 ):
    x_rot = 0.5 * proj.shape[ 2 ] + x_off # / scale_xy

    ix = 1
    iy = 1
    iz = 1

    bx = cx - 0.5 * ( ix * lx + x_slip )
    by = cy - 0.5 * ( iy * ly + y_slip )
    bz = cz - 0.5 * ( iz * lz + z_slip )
    # if not ( proj_gpu is None ):
    #     rt_gpu.del_gpu_vol( proj_gpu )
    # proj_gpu = rt_gpu.new_gpu_vol( proj )

    # definition of the rotations
    rota = compiled_toolboxes.cpu.make_rotations(
        [ 0.0 * input.shape[ 2 ], 0.0 * input.shape[ 1 ], 0.0 * input.shape[ 2 ] ],
        [ 1.0 * input.shape[ 2 ], 1.0 * input.shape[ 1 ], 1.0 * input.shape[ 2 ] ],
        [ x_rot                 , 0.0 * input.shape[ 1 ], 0.5 * input.shape[ 2 ] ],
        [ x_rot                 , 1.0 * input.shape[ 1 ], 0.5 * input.shape[ 2 ] ],
        input.shape[ 0 ]        , x_slip, y_slip, z_slip
    )

    #
    reco = compiled_toolboxes.gpu.reconstruct( input, rota, 
        bx, ix, lx,
        by, iy, ly,
        bz, iz, lz,
        inter_order
    )

    return reco
