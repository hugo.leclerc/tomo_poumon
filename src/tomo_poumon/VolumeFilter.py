from .Filter import Filter, filter
import numpy as np


class VolumeFilter( Filter ):
    """
    """

    def __init__( self, func, kargs, save, cache, force, gpu_args, gpu_ret, name ):
        super().__init__( func, kargs = kargs, save = save, cache = cache, force = force, gpu_args = gpu_args, gpu_ret = gpu_ret, name = name )

    # common filters ------------------------------------------------------------------------------
    def paganin( self, coeff = 0 ):
        from .paganin import paganin
        return VolumeFilter( paganin, input = self, coeff = coeff )

    def back_filter( self ):
        from .back_filter import back_filter
        return VolumeFilter( back_filter, input = self )

    def reconstruction( self, cx, cy, cz, lx, ly, lz, x_slip = 0, y_slip = 0, z_slip = 0, x_off = 0 ):
        from .reconstruction import reconstruction
        return VolumeFilter( reconstruction, input = self, cx = cx, cy = cy, cz = cz, lx = lx, ly = ly, lz = lz, x_slip = x_slip, y_slip = y_slip, z_slip = z_slip, x_off = x_off, gpu_args = [ "input" ] )

    def median_discs( self, length, thickness = 0 ):
        """ max of the median on all the disks of radius `length` around the current point """
        from .compiled_toolboxes import compiled_toolboxes
        return VolumeFilter( compiled_toolboxes.cpu.make_medi, input = self, length = round( length ), thickness = round( thickness ) )

    @filter( save = 0, cache = 0 ) # , output_type = VolumeFilter
    def add_ball( self, position, radius ):
        """ modify a dist function to have a ball... """
        if self.ndims == 3:
            x = np.arange( input.shape[ 2 ] ) - position[ 2 ]
            y = np.arange( input.shape[ 1 ] ) - position[ 1 ]
            z = np.arange( input.shape[ 0 ] ) - position[ 0 ]
            Z, Y, X = np.meshgrid( z, y, x, indexing = "ij" )
            b = radius - ( X**2 + Y**2 + Z**2 )**0.5
        return np.maximum( input, b )


class volume_filter( filter ):
    def __init__( self, save = 1, cache = 1, force = 0, gpu_args = [], gpu_ret = False, name = None ):
        super().__init__( save = save, cache = cache, force = force, gpu_args = gpu_args, gpu_ret = gpu_ret, name = name, output_type = VolumeFilter )
