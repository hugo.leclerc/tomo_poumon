from PyQt5.QtNetwork import QTcpServer, QHostAddress
from PyQt5.QtWidgets import QApplication
from .VisuServerConnection import VisuServerConnection
import sys


class VisuServer:
    """
        Accept connection to display/modify parameters, curves or images
    """
    def __init__( self ):
        self.tcp_server = QTcpServer()
        self.tcp_server.listen( QHostAddress.Any, int( sys.argv[ 1 ] ) )
        self.tcp_server.newConnection.connect( self.on_connect )
        self.clients = []

    def on_connect( self ):
        c = self.tcp_server.nextPendingConnection()
        self.clients.append( VisuServerConnection( app, c ) )


app = QApplication( sys.argv )
visu_server = VisuServer()
app.exec_()

