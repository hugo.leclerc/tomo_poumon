from .compiled_toolboxes import compiled_toolboxes
from .VolumeFilter import VolumeFilter

def reco_rings( part_rotation, rx, rz, bx, lx ):
    return None
    # pass

class RecoRings( VolumeFilter ):
    def __init__( self, part_rotation, rx, rz, bx, lx ):
        """
           ( rx, rz ) size of the 2D reconstruction
           bx: position of the first screen point to consider
           lx: nb screen points to consider

           return a volume of size ( rz, lx, rx )
        """
        super().__init__( reco_rings, part_rotation = part_rotation, rx = rx, rz = rz, bx = bx, lx = lx )
