#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <fstream>
#include <queue>
#include <cmath>

#include "ThreadPool.h"
#include "base_types.h"
#include "VolCpu.h"
#include "Stream.h"

//// nsmake cpp_flag -march=native
//// nsmake cpp_flag -O3
//// nsmake cpp_flag -g3

namespace py = pybind11;

void py_save_xdmf( py::array_t<float> &content, const std::string &base_name ) {
    // bin
    std::string filename_bin = base_name + ".bin";
    std::ofstream bin( filename_bin.c_str() );
    bin.write( (char *)content.request().ptr, sizeof( float ) * content.size() );

    // xdmf
    std::string filename_xdmf = base_name + ".xdmf";
    std::ofstream xdmf( filename_xdmf.c_str() );

    xdmf << "<?xml version='1.0' ?>\n";
    xdmf << "<!DOCTYPE Xdmf SYSTEM 'Xdmf.dtd' []>\n";
    xdmf << "<Xdmf xmlns:xi='http://www.w3.org/2001/XInclude' Version='2.0'>\n";
    xdmf << "<Domain>\n";
    xdmf << "    <Topology name='topo' TopologyType='3DCoRectMesh'\n";
    xdmf << "        Dimensions='" << content.shape( 0 ) << " " << content.shape( 1 ) << " " << content.shape( 2 ) << "'>\n";
    xdmf << "    </Topology>\n";
    xdmf << "    <Geometry name='geo' Type='ORIGIN_DXDYDZ'>\n";
    xdmf << "        <!-- Origin -->\n";
    xdmf << "        <DataItem Format='XML' Dimensions='3'>\n";
    xdmf << "            0.0 0.0 0.0\n";
    xdmf << "        </DataItem>\n";
    xdmf << "        <!-- DxDyDz -->\n";
    xdmf << "        <DataItem Format='XML' Dimensions='3'>\n";
    xdmf << "            1.0 1.0 1.0\n";
    xdmf << "        </DataItem>\n";
    xdmf << "    </Geometry>\n";
    xdmf << "    <Grid Name='T1' GridType='Uniform'>\n";
    xdmf << "        <Topology Reference='/Xdmf/Domain/Topology[1]'/>\n";
    xdmf << "        <Geometry Reference='/Xdmf/Domain/Geometry[1]'/>\n";
    xdmf << "        <Attribute Name='value' Center='Node'>\n";
    xdmf << "            <DataItem Format='Binary'\n";
    xdmf << "             DataType='Float' Precision='4' Endian='Little'\n";
    xdmf << "             Dimensions='" << content.shape( 0 ) << " " << content.shape( 1 ) << " " << content.shape( 2 ) << "'>\n";
    xdmf << "                " << base_name.substr( base_name.rfind( "/" ) + 1 ) << ".bin\n";
    xdmf << "            </DataItem>\n";
    xdmf << "        </Attribute>\n";
    xdmf << "    </Grid>\n";
    xdmf << "</Domain>\n";
    xdmf << "</Xdmf>\n";
}

py::array_t<float> py_make_rotations( py::array_t<float> &corner_0, py::array_t<float> &corner_7, py::array_t<float> &rot_0, py::array_t<float> &rot_1, int nb_angles, double x_slip, double y_slip, double z_slip ) {
    using std::sin;
    using std::cos;

    py::array_t<float> res; res.resize( { py::ssize_t( nb_angles ), py::ssize_t( 8 ), py::ssize_t( 3 ) } );
    for( int num_angle = 0; num_angle < nb_angles; ++num_angle ) {
        // rotation axis
        float p1 = rot_1.at( 0 ) - rot_0.at( 0 ), p2 = rot_1.at( 1 ) - rot_0.at( 1 ), p3 = rot_1.at( 2 ) - rot_0.at( 2 );
        float n = sqrt( p1 * p2 + p2 * p2 + p3 * p3 );
        p1 /= n; p2 /= n; p3 /= n;

        // rotation matrix
        float pos_in_cycle = float( num_angle ) / nb_angles;
        float theta = M_PI * pos_in_cycle;
        float co = cos( theta ), si = sin( theta );
        float rot_m[] = {
            co + ( 1 - co ) * p1 * p1     , ( 1 - co ) * p1 * p2 - si * p3, ( 1 - co ) * p1 * p3 + si * p2,
            ( 1 - co ) * p2 * p1 + si * p3, co + ( 1 - co ) * p2 * p2     , ( 1 - co ) * p2 * p3 - si * p1,
            ( 1 - co ) * p3 * p1 - si * p2, ( 1 - co ) * p3 * p2 + si * p1, co + ( 1 - co ) * p3 * p3     ,
        };

        // values
        for( int i = 0; i < 8; ++i ) {
            // point to be transformed
            float base_point[ 3 ];
            for( int d = 0; d < 3; ++d )
                base_point[ d ] = i & ( 1 << d ) ? corner_7.at( d ) : corner_0.at( d );

            base_point[ 0 ] += pos_in_cycle * x_slip;
            base_point[ 1 ] += pos_in_cycle * y_slip;
            base_point[ 2 ] += pos_in_cycle * z_slip;

            // transform and store
            for( int d = 0; d < 3; ++d )
                res.mutable_at( num_angle, i, d ) = rot_0.at( d ) +
                        rot_m[ 3 * d + 0 ] * ( base_point[ 0 ] - rot_0.at( 0 ) ) +
                        rot_m[ 3 * d + 1 ] * ( base_point[ 1 ] - rot_0.at( 1 ) ) +
                        rot_m[ 3 * d + 2 ] * ( base_point[ 2 ] - rot_0.at( 2 ) ) ;
        }
    }
    return res;
}


py::array_t<float> py_make_dist( py::array_t<float> &content, float val, int nb_points ) {
    using TI = std::int64_t;
    using std::sqrt;

    struct Point {
        bool operator<( const Point &p ) const { return dist() < p.dist(); }
        TI   dist     () const { return x * x + y * y + z * z; }
        TI   x, y, z;
    };

    std::vector<Point> points;
    for( TI z = -100; z <= 100; z++ ) {
        for( TI y = -100; y <= 100; y++ ) {
            for( TI x = -100; x <= 100; x++ ) {
                TI d = x * x + y * y + z * z;
                if ( d > 100 * 100 )
                    continue;
                points.push_back( { x, y, z } );
            }
        }
    }
    std::sort( points.begin(), points.end() );

    std::vector<TI> offsets;
    for( TI o = 0, old_dist = -1; o < points.size(); ++o ) {
        TI new_dist = points[ o ].dist();
        if ( old_dist != new_dist ) {
            offsets.push_back( o );
            old_dist = new_dist;
        }
    }
    offsets.push_back( points.size() );

    //
    py::array_t<float> res( { content.shape( 0 ), content.shape( 1 ), content.shape( 2 ) } );

    std::size_t nb_jobs = thread_pool.nb_threads();
    thread_pool.execute( nb_jobs, [&]( std::size_t num_job, int ) {
        TI beg_z = ( num_job + 0 ) * content.shape( 0 ) / nb_jobs;
        TI end_z = ( num_job + 1 ) * content.shape( 0 ) / nb_jobs;

        for( TI z = beg_z; z < end_z; z++ ) {
            if ( num_job + 1 == nb_jobs )
                std::cout << "dist: " << z << " / " << end_z << std::endl;

            for( TI y = 0; y < content.shape( 1 ); y++ ) {
                for( TI x = 0; x < content.shape( 2 ); x++ ) {
                    float acc = 0, nacc = 0, mean_dist = 0;
                    for( TI no = 1; no < offsets.size(); ++no ) {
                        for( TI np = offsets[ no - 1 ]; np < offsets[ no ]; ++np ) {
                            Point p = points[ np ];
                            TI nx = x + p.x;
                            TI ny = y + p.y;
                            TI nz = z + p.z;
                            if ( nx < 0 || nx >= content.shape( 2 ) || ny < 0 || ny >= content.shape( 1 ) || nz < 0 || nz >= content.shape( 0 ) )
                                continue;

                            float ins = content.at( nx, ny, nz ) > val;
                            mean_dist += ins * sqrt( p.dist() );
                            nacc += ! ins;
                            acc += ins;
                        }
                        if ( acc >= nb_points )
                            break;
                    }

                    // res.mutable_at( x, y, z ) = acc > nacc ? mean_dist / acc : 0;
                    res.mutable_at( x, y, z ) = mean_dist / acc;
                }
            }
        }
    } );

    return res;
}

float median( std::vector<float> &v ) {
    std::size_t n = v.size() / 2;
    nth_element( v.begin(), v.begin() + n, v.end() );
    return v[ n ];
}

#include "(make_medi_disks.cpp sphere.msh).h"

py::array_t<float> py_make_medi( py::array_t<float> &content, int length, int thickness ) {
    using std::round;
    using std::max;
    using std::min;

    if ( length <= 0 )
        return content;

    bool only_inside = true;
    int ol = only_inside * length;

    py::array_t<float> res( {
        content.shape( 0 ) - 2 * ol,
        content.shape( 1 ) - 2 * ol,
        content.shape( 2 ) - 2 * ol
    } );

    std::cout << content.shape( 0 ) << std::endl;
    std::cout << content.shape( 1 ) << std::endl;
    std::cout << content.shape( 2 ) << std::endl;
    

    //
    MediDisk md = medi_disk( min( length, 8 ), thickness * 0 );
    if ( md.nb_disks < 0 ) {
        std::cerr << "wrong length (medi_disk has not been compiled for this length)" << std::endl;
        return res;
    }

    std::size_t nb_jobs = thread_pool.nb_threads();
    thread_pool.execute( nb_jobs, [&]( std::size_t num_job, int ) {
        std::int64_t beg_z = ( num_job + 0 ) * res.shape( 0 ) / nb_jobs;
        std::int64_t end_z = ( num_job + 1 ) * res.shape( 0 ) / nb_jobs;

        std::vector<float> values;
        for( std::int64_t z = beg_z; z < end_z; z++ ) {
            if ( num_job + 1 == nb_jobs )
                std::cout << "  progress: " << z << " / " << end_z << std::endl;
            for( std::int64_t y = 0; y < res.shape( 1 ); y++ ) {
                for( std::int64_t x = 0; x < res.shape( 2 ); x++ ) {
                    float val = - std::numeric_limits<float>::max();

                    for( int num_disk = 0; num_disk < md.nb_disks; ++num_disk ) {
                        values.clear();
                        for( int off = md.offsets[ num_disk + 0 ]; off < md.offsets[ num_disk + 1 ]; off += 3 ) {
                            std::int64_t nx = x + ol + md.points[ off + 0 ];
                            std::int64_t ny = y + ol + md.points[ off + 1 ];
                            std::int64_t nz = z + ol + md.points[ off + 2 ];
                            if ( nx < 0 || nx >= content.shape( 2 ) || ny < 0 || ny >= content.shape( 1 ) || nz < 0 || nz >= content.shape( 0 ) )
                                continue;
                            values.push_back( content.at( nz, ny, nx ) );
                        }
                        val = max( val, median( values ) );
                    }

                    res.mutable_at( z, y, x ) = val;
                }
            }
        }
    } );

    return res;
}

py::array_t<float> py_make_conn( py::array_t<float> &content, float lim ) {
    py::array_t<float> res( { content.shape( 0 ), content.shape( 1 ), content.shape( 2 ) } );

    // clear
    for( std::int64_t z = 0; z < content.shape( 0 ); z++ )
        for( std::int64_t y = 0; y < content.shape( 1 ); y++ )
            for( std::int64_t x = 0; x < content.shape( 2 ); x++ )
                res.mutable_at( x, y, z ) = -1;

    //
    std::vector<std::pair<int,int>> sizes; // size, index
    for( int ns = 0; ; ++ns ) {
        auto find_point = [&]() -> std::array<std::int64_t,3> {
            for( std::int64_t z = 0; z < content.shape( 0 ); z++ )
                for( std::int64_t y = 0; y < content.shape( 1 ); y++ )
                    for( std::int64_t x = 0; x < content.shape( 2 ); x++ )
                        if ( res.at( x, y, z ) < 0 && content.at( x, y, z ) < lim )
                            return { x, y, z };
            return { -1, -1, -1 };
        };

        std::array<std::int64_t,3> p = find_point();
        if ( p[ 0 ] < 0 )
            break;

        std::queue<std::array<std::int64_t,3>> q;
        res.mutable_at( p[ 0 ], p[ 1 ], p[ 2 ] ) = ns;
        q.push( p );

        int size = 0;
        while ( ! q.empty() ) {
            std::array<std::int64_t,3> p = q.front();
            q.pop();

            ++size;

            for( std::int64_t nz = p[ 2 ] - 1; nz <= p[ 2 ] + 1; nz++ ) {
                for( std::int64_t ny = p[ 1 ] - 1; ny <= p[ 1 ] + 1; ny++ ) {
                    for( std::int64_t nx = p[ 0 ] - 1; nx <= p[ 0 ] + 1; nx++ ) {
                        if ( nx >= 0 && nx < res.shape( 2 ) && ny >= 0 && ny < res.shape( 1 ) && nz >= 0 && nz < res.shape( 0 ) && res.at( nx, ny, nz ) < 0 && content.at( nx, ny, nz ) < lim ) {
                            res.mutable_at( nx, ny, nz ) = ns;
                            q.push( { nx, ny, nz } );
                        }
                    }
                }
            }
        }

        std::cout << ns << " " << size << std::endl;
        sizes.emplace_back( size, ns );
    }

    std::sort( sizes.begin(), sizes.end() );

    std::vector<int> repl( sizes.size() );
    for( std::size_t i = 0; i < sizes.size(); i++ )
        repl[ sizes[ i ].second ] = i;

    for( std::int64_t z = 0; z < content.shape( 0 ); z++ )
        for( std::int64_t y = 0; y < content.shape( 1 ); y++ )
            for( std::int64_t x = 0; x < content.shape( 2 ); x++ )
                res.mutable_at( x, y, z ) = repl[ res.at( x, y, z ) ];

    return res;
}

py::array_t<float> py_make_sque( py::array_t<float> &py_dist ) {
    // using Vol = VolCpu<float>;
    // using Pos = Vol::Shape;

    // VolCpu<float> dist( { py_dist.shape( 2 ), py_dist.shape( 1 ), py_dist.shape( 0 ) } );
    // memcpy( dist.content, py_dist.data<float>(), dist.nb_bytes() );

    // Pos starting_point{ 0, 0, 0 };
    // dist.for_each_coord( [&]( Pos point ) {
    //     if ( dist( starting_point ) < dist( point ) )
    //         starting_point = point;
    // } );

    // PR( starting_point );
    // PR( dist( starting_point ) );

    py::array_t<float> res( { 0 } );
    return res;
}

py::array_t<float> py_remove_islands( py::array_t<float> &content, float min_nb_voxels, float threshold ) {
    py::array_t<float> res( { content.shape( 0 ), content.shape( 1 ), content.shape( 2 ) } );

    // clear
    for( std::int64_t z = 0; z < content.shape( 0 ); z++ )
        for( std::int64_t y = 0; y < content.shape( 1 ); y++ )
            for( std::int64_t x = 0; x < content.shape( 2 ); x++ )
                res.mutable_at( z, y, x ) = -1;

    std::vector<std::pair<int,int>> sizes; // size, index
    std::cout << min_nb_voxels << " threshold:" << threshold << std::endl; 

    // (actually only 1 pass is necessary)
    for( int wanted_value : { 1 } ) {
	std::int64_t fz = 0;
        for( int ns = 0; ; ++ns ) {
            auto find_point = [&]() -> std::array<std::int64_t,3> {
                for( ; fz < content.shape( 0 ); fz++ )
                    for( std::int64_t y = 0; y < content.shape( 1 ); y++ )
                        for( std::int64_t x = 0; x < content.shape( 2 ); x++ )
                            if ( res.at( fz, y, x ) < 0 && ( content.at( fz, y, x ) > threshold ) == wanted_value )
                                return { x, y, fz };
                return { -1, -1, -1 };
            };

            std::array<std::int64_t,3> p = find_point();
            if ( p[ 0 ] < 0 )
                break;

            std::queue<std::array<std::int64_t,3>> q;
            res.mutable_at( p[ 2 ], p[ 1 ], p[ 0 ] ) = ns;
            q.push( p );

            int size = 0;
            while ( ! q.empty() ) {
                std::array<std::int64_t,3> p = q.front();
                q.pop();

                ++size;

                for( std::int64_t nz = p[ 2 ] - 1; nz <= p[ 2 ] + 1; nz++ ) {
                    for( std::int64_t ny = p[ 1 ] - 1; ny <= p[ 1 ] + 1; ny++ ) {
                        for( std::int64_t nx = p[ 0 ] - 1; nx <= p[ 0 ] + 1; nx++ ) {
                            if ( nx >= 0 && nx < res.shape( 2 ) && ny >= 0 && ny < res.shape( 1 ) && nz >= 0 && nz < res.shape( 0 ) && 
					    res.at( nz, ny, nx ) < 0 && ( content.at( nz, ny, nx ) > threshold ) == wanted_value ) {
                                res.mutable_at( nz, ny, nx ) = ns;
                                q.push( { nx, ny, nz } );
                            }
                        }
                    }
                }
            }

            std::cout << "  size: " << size << " fz:" << fz << std::endl;
            sizes.emplace_back( size, ns );
        }

        std::vector<std::pair<int,int>> cp = sizes; 
        std::sort( cp.begin(), cp.end() );
	std::cout << cp.back().first << std::endl;

        // std::vector<int> repl( sizes.size() );
        // for( std::size_t i = 0; i < sizes.size(); i++ )
        //     repl[ sizes[ i ].second ] = i;

        // for( std::int64_t z = 0; z < content.shape( 0 ); z++ )
        //     for( std::int64_t y = 0; y < content.shape( 1 ); y++ )
        //         for( std::int64_t x = 0; x < content.shape( 2 ); x++ )
        //             if ( content.at( z, y, x ) == wanted_value )
        //                 res.mutable_at( z, y, x ) = repl[ res.at( z, y, x ) ];
    }

    // re-binarisation (inversion if voxel is from a small island)
    for( std::int64_t z = 0; z < content.shape( 0 ); z++ ) {
        for( std::int64_t y = 0; y < content.shape( 1 ); y++ ) {
            for( std::int64_t x = 0; x < content.shape( 2 ); x++ ) {
                if ( sizes[ res.mutable_at( z, y, x ) ].first < min_nb_voxels )
                    res.mutable_at( z, y, x ) = content.at( z, y, x ) > threshold ? threshold - 100 : threshold + 100;
                else
                    res.mutable_at( z, y, x ) = content.at( z, y, x );
            }
        }
    }

    return res;
}

py::array_t<float> py_recompose( py::array_t<float> &dist, float min_value, float sep_value ) {
    using TI = std::int64_t;
    using std::sqrt;
    using std::min;
    using std::max;

    // label array
    py::array_t<TI> label( { dist.shape( 0 ), dist.shape( 1 ), dist.shape( 2 ) } );
    for( TI z = 0; z < dist.shape( 0 ); z++ )
        for( TI y = 0; y < dist.shape( 1 ); y++ )
            for( TI x = 0; x < dist.shape( 2 ); x++ ) 
                label.mutable_at( z, y, x ) = -1;

    // init queue with local maxima
    struct Cell {
        bool             operator<( const Cell &that ) const { return dist > that.dist; }
        TI               label;
        TF               dist;
        TI               x;
        TI               y;
        TI               z;
    };
    std::priority_queue<Cell> queue;

    auto is_a_maximum = [&]( TI x, TI y, TI z ) {
        float v = dist.at( z, y, x );
        if ( v <= sep_value )
            return false;
        for( TI nz = z - 1; nz <= z + 1; nz++ ) {
            for( TI ny = y - 1; ny <= y + 1; ny++ ) {
                for( TI nx = x - 1; nx <= x + 1; nx++ ) {
                    if ( nx == x && ny == y && nz == z )
                        continue;
                    if ( v <= dist.at( nz, ny, nx ) )
                        return false;
                }
            }
        }
        return true;
    };
    TI nb_labels = 0;
    for( TI z = 1; z < dist.shape( 0 ) - 1; z++ )
        for( TI y = 1; y < dist.shape( 1 ) - 1; y++ )
            for( TI x = 1; x < dist.shape( 2 ) - 1; x++ ) 
                if ( is_a_maximum( x, y, z ) )
                    queue.push( Cell{ nb_labels++, 0.0, x, y, z } );

    // P( nb_labels );

    // Dijkstra dist
    py::array_t<float> djk_dist( { dist.shape( 0 ), dist.shape( 1 ), dist.shape( 2 ) } );
    for( TI z = 0; z < dist.shape( 0 ); z++ )
        for( TI y = 0; y < dist.shape( 1 ); y++ )
            for( TI x = 0; x < dist.shape( 2 ); x++ ) 
                djk_dist.mutable_at( z, y, x ) = std::numeric_limits<float>::max();

    // Dijkstra's algorithm from local maxima
    while ( ! queue.empty() ) {
        Cell cell = queue.top();
        queue.pop();

        TF &cv = djk_dist.mutable_at( cell.z, cell.y, cell.x );
        if ( cv <= cell.dist )
            continue;
        cv = cell.dist;
        label.mutable_at( cell.z, cell.y, cell.x ) = cell.label;

        for( TI nz = max( 0l, cell.z - 1 ); nz < min( dist.shape( 0 ), cell.z + 2 ); nz++ ) {
            for( TI ny = max( 0l, cell.y - 1 ); ny < min( dist.shape( 1 ), cell.y + 2 ); ny++ ) {
                for( TI nx = max( 0l, cell.x - 1 ); nx < min( dist.shape( 2 ), cell.x + 2 ); nx++ ) {
                    if ( nx == cell.x && ny == cell.y && nz == cell.z )
                        continue;
                    if ( dist.at( nz, ny, nx ) <= sep_value )
                        continue;
                    TF ncv = cv + sqrt( pow( nx - cell.x, 2 ) + pow( ny - cell.y, 2 ) + pow( nz - cell.z, 2 ) );
                    if ( ncv < djk_dist.at( nz, ny, nx ) )
                        queue.push( Cell{ cell.label, ncv, nx, ny, nz } );
                }
            }
        }
    }

    // connected ids
    std::vector<std::set<TI>> connections( nb_labels );
    for( TI z = 1; z < dist.shape( 0 ) - 1; z++ ) {
        for( TI y = 1; y < dist.shape( 1 ) - 1; y++ ) {
            for( TI x = 1; x < dist.shape( 2 ) - 1; x++ ) {
                if ( label.at( z, y, x ) < 0 || dist.at( z, y, x ) <= sep_value )
                    continue;
                for( TI nz = max( 0l, z - 1 ); nz < min( dist.shape( 0 ), z + 2 ); nz++ )
                    for( TI ny = max( 0l, y - 1 ); ny < min( dist.shape( 1 ), y + 2 ); ny++ )
                        for( TI nx = max( 0l, x - 1 ); nx < min( dist.shape( 2 ), x + 2 ); nx++ )
                            if ( label.at( nz, ny, nx ) >= 0 )
                                connections[ label.at( z, y, x ) ].insert( label.at( nz, ny, nx ) );
            }
        }
    }

    // Dijkstra's algorithm from boundaries of dist.at( nz, ny, nx ) == sep_value
    for( TI z = 0; z < dist.shape( 0 ); z++ ) {
        for( TI y = 0; y < dist.shape( 1 ); y++ ) {
            for( TI x = 0; x < dist.shape( 2 ); x++ ) {
                if ( label.mutable_at( z, y, x ) >= 0 ) {
                    djk_dist.mutable_at( z, y, x ) = 0;
                } else {
                    auto has_ng_with_label = [&]() -> TI {
                        for( TI nz = max( 0l, z - 1 ); nz < min( dist.shape( 0 ), z + 2 ); nz++ )
                            for( TI ny = max( 0l, y - 1 ); ny < min( dist.shape( 1 ), y + 2 ); ny++ )
                                for( TI nx = max( 0l, x - 1 ); nx < min( dist.shape( 2 ), x + 2 ); nx++ )
                                    if ( label.at( nz, ny, nx ) >= 0 )
                                        return label.at( nz, ny, nx );
                        return -1;
                    };
                    TI l = has_ng_with_label();
                    if ( l >= 0 )
                        queue.push( Cell{ l, 0.0, x, y, z } );
                }
            }
        }
    }
    while ( ! queue.empty() ) {
        Cell cell = queue.top();
        queue.pop();

        TF &cv = djk_dist.mutable_at( cell.z, cell.y, cell.x );
        if ( cv <= cell.dist )
            continue;
        cv = cell.dist;
        label.mutable_at( cell.z, cell.y, cell.x ) = cell.label;

        for( TI nz = max( 0l, cell.z - 1 ); nz < min( dist.shape( 0 ), cell.z + 2 ); nz++ ) {
            for( TI ny = max( 0l, cell.y - 1 ); ny < min( dist.shape( 1 ), cell.y + 2 ); ny++ ) {
                for( TI nx = max( 0l, cell.x - 1 ); nx < min( dist.shape( 2 ), cell.x + 2 ); nx++ ) {
                    if ( nx == cell.x && ny == cell.y && nz == cell.z )
                        continue;
                    if ( dist.at( nz, ny, nx ) <= min_value )
                        continue;
                    TF ncv = cv + sqrt( pow( nx - cell.x, 2 ) + pow( ny - cell.y, 2 ) + pow( nz - cell.z, 2 ) );
                    if ( ncv < djk_dist.at( nz, ny, nx ) )
                        queue.push( Cell{ cell.label, ncv, nx, ny, nz } );
                }
            }
        }
    }


    // white if allowed connection
    py::array_t<float> res( { dist.shape( 0 ), dist.shape( 1 ), dist.shape( 2 ) } );
    for( TI z = 0; z < dist.shape( 0 ); z++ ) {
        for( TI y = 0; y < dist.shape( 1 ); y++ ) {
            for( TI x = 0; x < dist.shape( 2 ); x++ ) {
                TF id = label.mutable_at( z, y, x );
                if ( id < 0 ) {
                    res.mutable_at( z, y, x ) = 0;
                    continue;
                }

                auto allowed_connection = [&]() {
                    for( TI nz = max( 0l, z - 1 ); nz < min( dist.shape( 0 ), z + 2 ); nz++ )
                        for( TI ny = max( 0l, y - 1 ); ny < min( dist.shape( 1 ), y + 2 ); ny++ )
                            for( TI nx = max( 0l, x - 1 ); nx < min( dist.shape( 2 ), x + 2 ); nx++ )
                                if ( label.at( nz, ny, nx ) >= 0 && connections[ id ].count( label.at( nz, ny, nx ) ) == 0 )
                                    return false;
                    return true;
                };
                res.mutable_at( z, y, x ) = allowed_connection() ? dist.at( z, y, x ) : 0;
            }
        }
    }

    return res;
}

py::array_t<int> py_make_spin( py::array_t<float> &medi, float r, float s, float threshold ) {
    using std::max;

    py::array_t<float> dist( { medi.shape( 0 ), medi.shape( 1 ), medi.shape( 2 ) } );
    py::array_t<int  > tsed( { medi.shape( 0 ), medi.shape( 1 ), medi.shape( 2 ) } );
    py::array_t<int  > id  ( { medi.shape( 0 ), medi.shape( 1 ), medi.shape( 2 ) } );
    for( std::int64_t z = 0; z < id.shape( 0 ); z++ ) {
        for( std::int64_t y = 0; y < id.shape( 1 ); y++ ) {
            for( std::int64_t x = 0; x < id.shape( 2 ); x++ ) {
                dist.mutable_at( x, y, z ) = std::numeric_limits<float>::max();
                tsed.mutable_at( x, y, z ) = 0;
                id  .mutable_at( x, y, z ) = -1;
            }
        }
    }

    auto calc_w = [&]( std::int64_t cx, std::int64_t cy, std::int64_t cz ) -> float {
        float res = 0;
        for( std::int64_t z = - r; z <= r; z++ )
            for( std::int64_t y = - r; y <= r; y++ )
                for( std::int64_t x = - r; x <= r; x++ )
                    if ( x * x + y * y + z * z <= r * r )
                        if ( cx + x >= 0 && cy + y >= 0 && cz + z >= 0 && cx + x < id.shape( 2 ) && cy + y < id.shape( 1 ) && cz + z < id.shape( 0 ) )
                            res += medi.at( cx + x, cy + y, cz + z ) > threshold;
        return res;
    };

    auto fill_sphere = [&]( std::int64_t cx, std::int64_t cy, std::int64_t cz, int nid ) {
        for( std::int64_t z = - r; z <= r; z++ ) {
            for( std::int64_t y = - r; y <= r; y++ ) {
                for( std::int64_t x = - r; x <= r; x++ ) {
                    if ( cx + x >= 0 && cy + y >= 0 && cz + z >= 0 && cx + x < id.shape( 2 ) && cy + y < id.shape( 1 ) && cz + z < id.shape( 0 ) ) {
                        float &cd = dist.mutable_at( cx + x, cy + y, cz + z );
                        float d = x * x + y * y + z * z;
                        if ( d <= r * r && cd > d ) {
                            cd = d;
                            id.mutable_at( cx + x, cy + y, cz + z ) = nid;
                        }
                    }
                }
            }
        }
    };

    for( int nid = 0; ; ++nid ) {
        // look up for a black enough zone (from a non tested point)
        float best_w = std::numeric_limits<float>::max();
        std::int64_t best_x, best_y, best_z;
        for( std::int64_t z = r; z < id.shape( 0 ) - r; z++ ) {
            for( std::int64_t y = r; y < id.shape( 1 ) - r; y++ ) {
                for( std::int64_t x = r; x < id.shape( 2 ) - r; x++ ) {
                    if ( id.at( x, y, z ) >= 0 )
                        continue;
                    float w = calc_w( x, y, z );
                    if ( best_w > w ) {
                        best_w = w;
                        best_x = x;
                        best_y = y;
                        best_z = z;
                    }
                }
            }
        }
        if ( best_w > s ) 
            break;
        std::cout << "bx=" << best_x << std::endl;

        // start
        std::deque<std::tuple<std::int64_t,std::int64_t,std::int64_t>> front;
        front.push_back( { best_x, best_y, best_z } );
        tsed.mutable_at( best_x, best_y, best_z ) = 1;


        // extension
        while ( ! front.empty() ) {
            std::int64_t x = std::get<0>( front.back() );
            std::int64_t y = std::get<1>( front.back() );
            std::int64_t z = std::get<2>( front.back() );
            front.pop_back();

            fill_sphere( x, y, z, nid );

            // try
            auto test = [&]( std::int64_t nx, std::int64_t ny, std::int64_t nz ) {
                // out of bound ?
                if ( nx < 0 || ny < 0 || nz < 0 || nx >= id.shape( 2 ) || ny >= id.shape( 1 ) || nz >= id.shape( 0 ) )
                    return;

                // already tested ?
                if ( tsed.at( nx, ny, nz ) )
                    return;
                tsed.mutable_at( nx, ny, nz ) = 1;

                // too much white ?
                float w = calc_w( nx, ny, nz );
                if ( w > s )
                    return;

                // else, push it
                front.push_back( { nx, ny, nz } );
            };
            test( x - 1, y, z );
            test( x + 1, y, z );
            test( x, y - 1, z );
            test( x, y + 1, z );
            test( x, y, z - 1 );
            test( x, y, z + 1 );
        }
    }

    return id;
}

PYBIND11_MODULE( reconstruction_toolbox_cpu, m ) {
    m.doc() = "reconstruction toolbox";

    m.def( "remove_islands", &py_remove_islands );
    m.def( "make_rotations", &py_make_rotations );
    m.def( "recompose"     , &py_recompose      );
    m.def( "save_xdmf"     , &py_save_xdmf      );
    m.def( "make_dist"     , &py_make_dist, py::arg( "input" ), py::arg( "threshold" ), py::arg( "nb_points" ) );
    m.def( "make_medi"     , &py_make_medi, py::arg( "input" ), py::arg( "length"    ), py::arg( "thickness" ) );
    m.def( "make_conn"     , &py_make_conn      );
    m.def( "make_sque"     , &py_make_sque      );
    m.def( "make_spin"     , &py_make_spin      );
}

