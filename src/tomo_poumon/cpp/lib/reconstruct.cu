//// nsmake lib_name cudart
//// nsmake cxx_name nvcc
//// nsmake cpp_flag -O3

#include <cuda_runtime.h>
#include <cmath>

#include "reconstruct.h"
#include "Stream.h"
#include "N.h"

using std::floor;

template<int order> __global__
void __reconstruct(
        TF *reco, const TF *proj, const TF *rota, 
        TI len_reco_x, TI len_reco_y, TI len_reco_z, 
        TF beg_reco_x, TF beg_reco_y, TF beg_reco_z, 
        TF inc_reco_x, TF inc_reco_y, TF inc_reco_z,
        TI len_proj_x, TI len_proj_y, TI len_proj_a, 
        TI needed_offset, N<order> ) {

    const TF sx = TF( 1.0 ) / len_proj_x;
    const TF sy = TF( 1.0 ) / len_proj_y;
    const TF sz = TF( 1.0 ) / len_proj_x;

    const TI nb_points = len_reco_x * len_reco_y * len_reco_z;
    for( TI num_point = TI( blockIdx.x ) * blockDim.x + threadIdx.x; num_point < nb_points; num_point += gridDim.x * blockDim.x ) {
        TI ix = num_point % len_reco_x, ny = num_point / len_reco_x, iy = ny % len_reco_y, iz = ny / len_reco_y;

        TF cx = sx * ( beg_reco_x + ix * inc_reco_x );
        TF cy = sy * ( beg_reco_y + iy * inc_reco_y );
        TF cz = sz * ( beg_reco_z + iz * inc_reco_z );

        TF res = 0.0;
        for( TI num_angle = 0; num_angle < len_proj_a - 1; ++num_angle ) {
            TF hp[ 3 ] = { 0, 0, 0 };
            const TF *bhp = rota + 3 * 8 * num_angle;
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 1.0f - cx ) * ( 1.0f - cy ) * ( 1.0f - cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 0.0f + cx ) * ( 1.0f - cy ) * ( 1.0f - cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 1.0f - cx ) * ( 0.0f + cy ) * ( 1.0f - cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 0.0f + cx ) * ( 0.0f + cy ) * ( 1.0f - cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 1.0f - cx ) * ( 1.0f - cy ) * ( 0.0f + cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 0.0f + cx ) * ( 1.0f - cy ) * ( 0.0f + cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 1.0f - cx ) * ( 0.0f + cy ) * ( 0.0f + cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 0.0f + cx ) * ( 0.0f + cy ) * ( 0.0f + cz ) * TF( *( bhp++ ) );

            TI px = floor( hp[ 0 ] ), py = floor( hp[ 1 ] );

            if ( order == 7 ) {
                if ( px >= needed_offset && py >= 3 && px + 1 < len_proj_x - needed_offset && py + 4 < len_proj_y ) {
                    #define MAKE_V_7( X, Y ) TF v##X##Y = proj[ len_proj_x * ( len_proj_y * num_angle + ( py + Y - 3 ) ) + ( px + X ) ]
                    MAKE_V_7( 0, 0 ); MAKE_V_7( 1, 0 );
                    MAKE_V_7( 0, 1 ); MAKE_V_7( 1, 1 );
                    MAKE_V_7( 0, 2 ); MAKE_V_7( 1, 2 );
                    MAKE_V_7( 0, 3 ); MAKE_V_7( 1, 3 );
                    MAKE_V_7( 0, 4 ); MAKE_V_7( 1, 4 );
                    MAKE_V_7( 0, 5 ); MAKE_V_7( 1, 5 );
                    MAKE_V_7( 0, 6 ); MAKE_V_7( 1, 6 );
                    MAKE_V_7( 0, 7 ); MAKE_V_7( 1, 7 );
                    TF fx = hp[ 0 ] - TF( px );
                    TF fy = hp[ 1 ] - TF( py );

                    #define H0_7( F ) ( 1.0f - F )
                    #define H1_7( F ) ( 0.0f + F )

                    #define I0_7( F ) 1.0f / ( -5040 ) *                ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
                    #define I1_7( F ) 1.0f / (   720 ) * ( F + 3.0f ) *                ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
                    #define I2_7( F ) 1.0f / (  -240 ) * ( F + 3.0f ) * ( F + 2.0f ) *                ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
                    #define I3_7( F ) 1.0f / (   144 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) *                ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
                    #define I4_7( F ) 1.0f / (  -144 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) *                ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
                    #define I5_7( F ) 1.0f / (   240 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )                * ( F - 3.0f ) * ( F - 4.0f )
                    #define I6_7( F ) 1.0f / (  -720 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) *                ( F - 4.0f )
                    #define I7_7( F ) 1.0f / (  5040 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )

                    res += H0_7( fx ) * I0_7( fy ) * v00; res += H1_7( fx ) * I0_7( fy ) * v10;
                    res += H0_7( fx ) * I1_7( fy ) * v01; res += H1_7( fx ) * I1_7( fy ) * v11;
                    res += H0_7( fx ) * I2_7( fy ) * v02; res += H1_7( fx ) * I2_7( fy ) * v12;
                    res += H0_7( fx ) * I3_7( fy ) * v03; res += H1_7( fx ) * I3_7( fy ) * v13;
                    res += H0_7( fx ) * I4_7( fy ) * v04; res += H1_7( fx ) * I4_7( fy ) * v14;
                    res += H0_7( fx ) * I5_7( fy ) * v05; res += H1_7( fx ) * I5_7( fy ) * v15;
                    res += H0_7( fx ) * I6_7( fy ) * v06; res += H1_7( fx ) * I6_7( fy ) * v16;
                    res += H0_7( fx ) * I7_7( fy ) * v07; res += H1_7( fx ) * I7_7( fy ) * v17;
                } else
                    res = nan( "" );
            }

            if ( order == 5 ) {
                if ( px >= needed_offset && py >= 2 && px + 1 < len_proj_x - needed_offset && py + 3 < len_proj_y ) {
                    #define MAKE_V_5( X, Y ) TF v##X##Y = proj[ len_proj_x * ( len_proj_y * num_angle + ( py + Y - 2 ) ) + ( px + X ) ]
                    MAKE_V_5( 0, 0 ); MAKE_V_5( 1, 0 );
                    MAKE_V_5( 0, 1 ); MAKE_V_5( 1, 1 );
                    MAKE_V_5( 0, 2 ); MAKE_V_5( 1, 2 );
                    MAKE_V_5( 0, 3 ); MAKE_V_5( 1, 3 );
                    MAKE_V_5( 0, 4 ); MAKE_V_5( 1, 4 );
                    MAKE_V_5( 0, 5 ); MAKE_V_5( 1, 5 );
                    TF fx = hp[ 0 ] - TF( px );
                    TF fy = hp[ 1 ] - TF( py );

                    #define H0_5( F ) ( 1.0f - F )
                    #define H1_5( F ) ( 0.0f + F )

                    #define I0_5( F ) 1.0f / ( -120 ) *                ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
                    #define I1_5( F ) 1.0f / (   24 ) * ( F + 2.0f ) *                ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
                    #define I2_5( F ) 1.0f / (  -12 ) * ( F + 2.0f ) * ( F + 1.0f ) *                ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
                    #define I3_5( F ) 1.0f / (   12 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) *                ( F - 2.0f ) * ( F - 3.0f )
                    #define I4_5( F ) 1.0f / (  -24 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )                * ( F - 3.0f )
                    #define I5_5( F ) 1.0f / (  120 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f )

                    res += H0_5( fx ) * I0_5( fy ) * v00; res += H1_5( fx ) * I0_5( fy ) * v10;
                    res += H0_5( fx ) * I1_5( fy ) * v01; res += H1_5( fx ) * I1_5( fy ) * v11;
                    res += H0_5( fx ) * I2_5( fy ) * v02; res += H1_5( fx ) * I2_5( fy ) * v12;
                    res += H0_5( fx ) * I3_5( fy ) * v03; res += H1_5( fx ) * I3_5( fy ) * v13;
                    res += H0_5( fx ) * I4_5( fy ) * v04; res += H1_5( fx ) * I4_5( fy ) * v14;
                    res += H0_5( fx ) * I5_5( fy ) * v05; res += H1_5( fx ) * I5_5( fy ) * v15;
                } else
                    res = nan( "" );
            }

            if ( order == 3 ) {
                if ( px >= needed_offset && py >= 1 && px + 1 < len_proj_x - needed_offset && py + 2 < len_proj_y ) {
                    #define MAKE_V_3( X, Y ) TF v##X##Y = proj[ len_proj_x * ( len_proj_y * num_angle + ( py + Y - 1 ) ) + ( px + X ) ]
                    MAKE_V_3( 0, 0 ); MAKE_V_3( 1, 0 );
                    MAKE_V_3( 0, 1 ); MAKE_V_3( 1, 1 );
                    MAKE_V_3( 0, 2 ); MAKE_V_3( 1, 2 );
                    MAKE_V_3( 0, 3 ); MAKE_V_3( 1, 3 );
                    TF fx = hp[ 0 ] - TF( px );
                    TF fy = hp[ 1 ] - TF( py );

                    #define H0_3( F ) ( 1.0f - F )
                    #define H1_3( F ) ( 0.0f + F )

                    #define I0_3( F ) 1.0f / 6 * ( 0.0f - F ) * ( F - 1.0f ) * ( F - 2.0f )
                    #define I1_3( F ) 1.0f / 2 * ( F + 1.0f ) * ( F - 1.0f ) * ( F - 2.0f )
                    #define I2_3( F ) 1.0f / 2 * ( F + 1.0f ) * ( 0.0f - F ) * ( F - 2.0f )
                    #define I3_3( F ) 1.0f / 6 * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )

                    res += H0_3( fx ) * I0_3( fy ) * v00; res += H1_3( fx ) * I0_3( fy ) * v10;
                    res += H0_3( fx ) * I1_3( fy ) * v01; res += H1_3( fx ) * I1_3( fy ) * v11;
                    res += H0_3( fx ) * I2_3( fy ) * v02; res += H1_3( fx ) * I2_3( fy ) * v12;
                    res += H0_3( fx ) * I3_3( fy ) * v03; res += H1_3( fx ) * I3_3( fy ) * v13;
                } else
                    res = nan( "" );
            }

            if ( order == 1 ) {
                if ( px >= needed_offset && py >= 0 && px + 1 < len_proj_x - needed_offset && py + 1 < len_proj_y ) {
                    #define MAKE_V_1( X, Y ) TF v##X##Y = proj[ len_proj_x * ( len_proj_y * num_angle + py + Y ) + px + X ]
                    MAKE_V_1( 0, 0 ); MAKE_V_1( 1, 0 );
                    MAKE_V_1( 0, 1 ); MAKE_V_1( 1, 1 );
                    TF fx = hp[ 0 ] - TF( px );
                    TF fy = hp[ 1 ] - TF( py );

                    res += ( TF( 1 ) - fx ) * ( TF( 1 ) - fy ) * v00;
                    res += ( TF( 0 ) + fx ) * ( TF( 1 ) - fy ) * v10;
                    res += ( TF( 1 ) - fx ) * ( TF( 0 ) + fy ) * v01;
                    res += ( TF( 0 ) + fx ) * ( TF( 0 ) + fy ) * v11;
                } else
                    res *= nan( "" );
            }

            if ( order == 0 ) {
                if ( px >= needed_offset && py >= 0 && px + 1 < len_proj_x - needed_offset && py < len_proj_y ) {
                    #define MAKE_V_0( X, Y ) TF v##X##Y = proj[ len_proj_x * ( len_proj_y * num_angle + py + Y ) + px + X ]
                    MAKE_V_0( 0, 0 ); MAKE_V_0( 1, 0 );
                    TF fx = hp[ 0 ] - TF( px );

                    res += ( TF( 1 ) - fx ) * v00;
                    res += ( TF( 0 ) + fx ) * v10;
                } else
                    res *= nan( "" );
            }
        }
 
        reco[ num_point ] = res;
    }
}


__global__
void __reco_ring( TF *reco, TI reco_x, TI reco_y, TI reco_z, TI proj_x, TI proj_y, TI proj_a, const TF *rota, TI beg_y, TI len_y, TI pos_x, TI pos_y ) {
    using    TF        = TF;
    using    TF        = TF;
    using    TF        = TF;

    const TF sx        = TF( 1.0 ) / reco_x;
    const TF sy        = TF( 1.0 ) / reco_y;
    const TF sz        = TF( 1.0 ) / reco_z;

    const TI beg_x     = 0;
    const TI beg_z     = 0;
    const TI len_x     = reco_x;
    const TI len_z     = reco_z;
    const TI nb_points = len_x * len_y * len_z;

    for( TI num_point = TI( blockIdx.x ) * blockDim.x + threadIdx.x; num_point < nb_points; num_point += gridDim.x * blockDim.x ) {
        TI ix = num_point % len_x, ny = num_point / len_x, iy = ny % len_y, iz = ny / len_y;

        TF cx = sx * ( beg_x + ix );
        TF cy = sy * ( beg_y + iy );
        TF cz = sz * ( beg_z + iz );

        TF res = 0.0;
        for( TI num_angle = 0; num_angle < proj_a - 1; ++num_angle ) {
            TF hp[ 3 ] = { 0, 0, 0 };
            const TF *bhp = rota + 3 * 8 * num_angle;
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 1.0f - cx ) * ( 1.0f - cy ) * ( 1.0f - cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 0.0f + cx ) * ( 1.0f - cy ) * ( 1.0f - cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 1.0f - cx ) * ( 0.0f + cy ) * ( 1.0f - cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 0.0f + cx ) * ( 0.0f + cy ) * ( 1.0f - cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 1.0f - cx ) * ( 1.0f - cy ) * ( 0.0f + cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 0.0f + cx ) * ( 1.0f - cy ) * ( 0.0f + cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 1.0f - cx ) * ( 0.0f + cy ) * ( 0.0f + cz ) * TF( *( bhp++ ) );
            for( TI d = 0; d < 3; ++d ) hp[ d ] += ( 0.0f + cx ) * ( 0.0f + cy ) * ( 0.0f + cz ) * TF( *( bhp++ ) );

            #if ORDER==7
            TODO;
            TI px = floor( hp[ 0 ] ), py = floor( hp[ 1 ] );
            if ( px >= needed_offset && py >= 3 && px + 1 < proj_x - needed_offset && py + 4 < proj_y ) {
                #define MAKE_V( X, Y ) TF v##X##Y = proj[ proj_x * ( proj_y * num_angle + ( py + Y - 3 ) ) + ( px + X ) ]
                MAKE_V( 0, 0 ); MAKE_V( 1, 0 );
                MAKE_V( 0, 1 ); MAKE_V( 1, 1 );
                MAKE_V( 0, 2 ); MAKE_V( 1, 2 );
                MAKE_V( 0, 3 ); MAKE_V( 1, 3 );
                MAKE_V( 0, 4 ); MAKE_V( 1, 4 );
                MAKE_V( 0, 5 ); MAKE_V( 1, 5 );
                MAKE_V( 0, 6 ); MAKE_V( 1, 6 );
                MAKE_V( 0, 7 ); MAKE_V( 1, 7 );
                TF fx = hp[ 0 ] - TF( px );
                TF fy = hp[ 1 ] - TF( py );

                #define H0( F ) ( 1.0f - F )
                #define H1( F ) ( 0.0f + F )

                #define I0( F ) 1.0f / ( -5040 ) *                ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
                #define I1( F ) 1.0f / (   720 ) * ( F + 3.0f ) *                ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
                #define I2( F ) 1.0f / (  -240 ) * ( F + 3.0f ) * ( F + 2.0f ) *                ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
                #define I3( F ) 1.0f / (   144 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) *                ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
                #define I4( F ) 1.0f / (  -144 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) *                ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
                #define I5( F ) 1.0f / (   240 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )                * ( F - 3.0f ) * ( F - 4.0f )
                #define I6( F ) 1.0f / (  -720 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) *                ( F - 4.0f )
                #define I7( F ) 1.0f / (  5040 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )

                res += H0( fx ) * I0( fy ) * v00; res += H1( fx ) * I0( fy ) * v10;
                res += H0( fx ) * I1( fy ) * v01; res += H1( fx ) * I1( fy ) * v11;
                res += H0( fx ) * I2( fy ) * v02; res += H1( fx ) * I2( fy ) * v12;
                res += H0( fx ) * I3( fy ) * v03; res += H1( fx ) * I3( fy ) * v13;
                res += H0( fx ) * I4( fy ) * v04; res += H1( fx ) * I4( fy ) * v14;
                res += H0( fx ) * I5( fy ) * v05; res += H1( fx ) * I5( fy ) * v15;
                res += H0( fx ) * I6( fy ) * v06; res += H1( fx ) * I6( fy ) * v16;
                res += H0( fx ) * I7( fy ) * v07; res += H1( fx ) * I7( fy ) * v17;
            } else
                res = nan( "" );
            #endif

            #if ORDER==5
            TODO;
            if ( px >= needed_offset && py >= 2 && px + 1 < projections.shape[ 0 ] - needed_offset && py + 3 < projections.shape[ 1 ] ) {
                #define MAKE_V( X, Y ) St v##X##Y = projections.content[ projections.storage_acc[ 1 ] * num_angle + projections.storage_acc[ 0 ] * ( py + Y - 2 ) + ( px + X ) ]
                MAKE_V( 0, 0 ); MAKE_V( 1, 0 );
                MAKE_V( 0, 1 ); MAKE_V( 1, 1 );
                MAKE_V( 0, 2 ); MAKE_V( 1, 2 );
                MAKE_V( 0, 3 ); MAKE_V( 1, 3 );
                MAKE_V( 0, 4 ); MAKE_V( 1, 4 );
                MAKE_V( 0, 5 ); MAKE_V( 1, 5 );
                TF fx = hp[ 0 ] - TF( px );
                TF fy = hp[ 1 ] - TF( py );

                #define H0( F ) ( 1.0f - F )
                #define H1( F ) ( 0.0f + F )

                #define I0( F ) 1.0f / ( -120 ) *                ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
                #define I1( F ) 1.0f / (   24 ) * ( F + 2.0f ) *                ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
                #define I2( F ) 1.0f / (  -12 ) * ( F + 2.0f ) * ( F + 1.0f ) *                ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
                #define I3( F ) 1.0f / (   12 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) *                ( F - 2.0f ) * ( F - 3.0f )
                #define I4( F ) 1.0f / (  -24 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )                * ( F - 3.0f )
                #define I5( F ) 1.0f / (  120 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f )

                res += H0( fx ) * I0( fy ) * v00; res += H1( fx ) * I0( fy ) * v10;
                res += H0( fx ) * I1( fy ) * v01; res += H1( fx ) * I1( fy ) * v11;
                res += H0( fx ) * I2( fy ) * v02; res += H1( fx ) * I2( fy ) * v12;
                res += H0( fx ) * I3( fy ) * v03; res += H1( fx ) * I3( fy ) * v13;
                res += H0( fx ) * I4( fy ) * v04; res += H1( fx ) * I4( fy ) * v14;
                res += H0( fx ) * I5( fy ) * v05; res += H1( fx ) * I5( fy ) * v15;
            } else
                res = nan( "" );
            #endif

            #if ORDER==3
            TODO;
            if ( px >= needed_offset && py >= 1 && px + 1 < projections.shape[ 0 ] - needed_offset && py + 2 < projections.shape[ 1 ] ) {
                #define MAKE_V( X, Y ) St v##X##Y = projections.content[ projections.storage_acc[ 1 ] * num_angle + projections.storage_acc[ 0 ] * ( py + Y - 1 ) + ( px + X ) ]
                MAKE_V( 0, 0 ); MAKE_V( 1, 0 );
                MAKE_V( 0, 1 ); MAKE_V( 1, 1 );
                MAKE_V( 0, 2 ); MAKE_V( 1, 2 );
                MAKE_V( 0, 3 ); MAKE_V( 1, 3 );
                TF fx = hp[ 0 ] - TF( px );
                TF fy = hp[ 1 ] - TF( py );

                #define H0( F ) ( 1.0f - F )
                #define H1( F ) ( 0.0f + F )

                #define I0( F ) 1.0f / 6 * ( 0.0f - F ) * ( F - 1.0f ) * ( F - 2.0f )
                #define I1( F ) 1.0f / 2 * ( F + 1.0f ) * ( F - 1.0f ) * ( F - 2.0f )
                #define I2( F ) 1.0f / 2 * ( F + 1.0f ) * ( 0.0f - F ) * ( F - 2.0f )
                #define I3( F ) 1.0f / 6 * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )

                res += H0( fx ) * I0( fy ) * v00; res += H1( fx ) * I0( fy ) * v10;
                res += H0( fx ) * I1( fy ) * v01; res += H1( fx ) * I1( fy ) * v11;
                res += H0( fx ) * I2( fy ) * v02; res += H1( fx ) * I2( fy ) * v12;
                res += H0( fx ) * I3( fy ) * v03; res += H1( fx ) * I3( fy ) * v13;
            } else
                res = nan( "" );
            #endif

            #if ORDER==1
            TI px = floor( hp[ 0 ] ), py = floor( hp[ 1 ] );
            #define RAKE_V( X, Y ) TF v##X##Y = py + Y == pos_y && px + X == pos_x
            RAKE_V( 0, 0 ); RAKE_V( 1, 0 );
            RAKE_V( 0, 1 ); RAKE_V( 1, 1 );
            TF fx = hp[ 0 ] - TF( px );
            TF fy = hp[ 1 ] - TF( py );

            res += ( TF( 1 ) - fx ) * ( TF( 1 ) - fy ) * v00;
            res += ( TF( 0 ) + fx ) * ( TF( 1 ) - fy ) * v10;
            res += ( TF( 1 ) - fx ) * ( TF( 0 ) + fy ) * v01;
            res += ( TF( 0 ) + fx ) * ( TF( 0 ) + fy ) * v11;
            #endif
        }

        reco[ num_point ] = res;
    }
}

void reconstruct( 
        TF *reco, const TF *proj_gpu, const TF *rota, 
        TI len_reco_x, TI len_reco_y, TI len_reco_z, 
        TF beg_reco_x, TF beg_reco_y, TF beg_reco_z, 
        TF inc_reco_x, TF inc_reco_y, TF inc_reco_z,
        TI len_proj_x, TI len_proj_y, TI len_proj_a,
        int order
) {

    TF *rota_gpu; cudaMalloc( &rota_gpu, sizeof( TF ) * 8 * 3 * len_proj_a );
    cudaMemcpy( rota_gpu, rota, sizeof( TF ) * 8 * 3 * len_proj_a, cudaMemcpyHostToDevice );

    TF *reco_gpu; cudaMalloc( &reco_gpu, sizeof( TF ) * len_reco_x * len_reco_y * len_reco_z );

    if ( order == 7 )
        __reconstruct<<<128,512>>>( reco_gpu, proj_gpu, rota_gpu, 
            len_reco_x, len_reco_y, len_reco_z, 
            beg_reco_x, beg_reco_y, beg_reco_z, 
            inc_reco_x, inc_reco_y, inc_reco_z,
            len_proj_x, len_proj_y, len_proj_a,
            20, N<7>()
        );
    else if ( order == 5 )
        __reconstruct<<<128,512>>>( reco_gpu, proj_gpu, rota_gpu, 
            len_reco_x, len_reco_y, len_reco_z, 
            beg_reco_x, beg_reco_y, beg_reco_z, 
            inc_reco_x, inc_reco_y, inc_reco_z,
            len_proj_x, len_proj_y, len_proj_a,
            20, N<5>()
        );
    else if ( order == 3 )
        __reconstruct<<<128,512>>>( reco_gpu, proj_gpu, rota_gpu, 
            len_reco_x, len_reco_y, len_reco_z, 
            beg_reco_x, beg_reco_y, beg_reco_z, 
            inc_reco_x, inc_reco_y, inc_reco_z,
            len_proj_x, len_proj_y, len_proj_a,
            20, N<3>()
        );
    else if ( order == 1 )
        __reconstruct<<<128,512>>>( reco_gpu, proj_gpu, rota_gpu, 
            len_reco_x, len_reco_y, len_reco_z, 
            beg_reco_x, beg_reco_y, beg_reco_z, 
            inc_reco_x, inc_reco_y, inc_reco_z,
            len_proj_x, len_proj_y, len_proj_a,
            20, N<1>()
        );
    else
        __reconstruct<<<128,512>>>( reco_gpu, proj_gpu, rota_gpu, 
            len_reco_x, len_reco_y, len_reco_z, 
            beg_reco_x, beg_reco_y, beg_reco_z, 
            inc_reco_x, inc_reco_y, inc_reco_z,
            len_proj_x, len_proj_y, len_proj_a,
            20, N<0>()
        );

    cudaMemcpy( reco, reco_gpu, sizeof( TF ) * len_reco_x * len_reco_y * len_reco_z, cudaMemcpyDeviceToHost );

    cudaFree( reco_gpu );
    cudaFree( rota_gpu );

    if ( auto err = cudaGetLastError() )
        PE( err );
}

void reco_ring( TF *reco, TI reco_x, TI reco_y, TI reco_z, TI proj_x, TI proj_y, TI proj_a, const TF *rota, TI beg_y, TI len_y, TI pos_x, TI pos_y ) {
    TF *rota_gpu; cudaMalloc( &rota_gpu, sizeof( TF ) * 8 * 3 * proj_a );
    cudaMemcpy( rota_gpu, rota, sizeof( TF ) * 8 * 3 * proj_a, cudaMemcpyHostToDevice );

    TF *reco_gpu; cudaMalloc( &reco_gpu, sizeof( TF ) * reco_x * len_y * reco_z );

    __reco_ring<<<128,512>>>( reco_gpu, reco_x, reco_y, reco_z, proj_x, proj_y, proj_a, rota_gpu, beg_y, len_y, pos_x, pos_y );

    cudaMemcpy( reco, reco_gpu, sizeof( TF ) * reco_x * len_y * reco_z, cudaMemcpyDeviceToHost );

    cudaFree( reco_gpu );
    cudaFree( rota_gpu );

    if ( auto err = cudaGetLastError() )
        PE( err );
}

#include "(make_medi_disks.cpp sphere.msh).h"

//declare constant memory
// __constant__ std::int8_t cangle[ medi_disks_off_8_0[ medi_disks_len_8_0 ] ];


// __global__
// void __make_medi( TF *medi, const TF *orig, TI orig_x, TI orig_y, TI orig_z, TI length, TI thickness ) {
//     TI medi_x = orig_x - 2 * length;
//     TI medi_y = orig_y - 2 * length;
//     TI medi_z = orig_z - 2 * length;

//     const TI nb_points = medi_x * medi_y * medi_z;
//     for( TI num_point = TI( blockIdx.x ) * blockDim.x + threadIdx.x; num_point < nb_points; num_point += gridDim.x * blockDim.x ) {
//         TI ix = num_point % medi_x, ny = num_point / medi_x, iy = ny % medi_y, iz = ny / medi_y;

//         TI cx = ix + length;
//         TI cy = iy + length;
//         TI cz = iz + length;

//         medi[ num_point ] = orig[ orig_y * orig_x * cz + orig_x * cy + cx ];
//     }
// }

void make_medi( TF *medi, const TF *orig, TI orig_x, TI orig_y, TI orig_z, TI length, TI thickness ) {
    // TI medi_x = orig_x - 2 * length;
    // TI medi_y = orig_y - 2 * length;
    // TI medi_z = orig_z - 2 * length;

    // TF *orig_gpu; cudaMalloc( &orig_gpu, sizeof( TF ) * orig_x * orig_y * orig_z );
    // cudaMemcpy( orig_gpu, orig, sizeof( TF ) * orig_x * orig_y * orig_z, cudaMemcpyHostToDevice );

    // //  
    // MediDisk md = medi_disk( length, thickness );
    // if ( md.nb_disks < 0 ) {
    //     std::cerr << "wrong length" << std::endl;
    //     return res;
    // }

    

    // TF *medi_gpu; cudaMalloc( &medi_gpu, sizeof( TF ) * medi_x * medi_y * medi_z );

    // __make_medi<<<128,512>>>( medi_gpu, orig_gpu, orig_x, orig_y, orig_z, length, thickness );

    // cudaMemcpy( medi, medi_gpu, sizeof( TF ) * medi_x * medi_y * medi_z, cudaMemcpyDeviceToHost );

    // cudaFree( orig_gpu );
    // cudaFree( medi_gpu );

    // if ( auto err = cudaGetLastError() )
    //     PE( err );
}


