l = 10;
n = 10;

For t In {0:n}
    Point( t ) = { Cos[ 2 * Pi * t / n ], Sin[ 2 * Pi * t / n ], 0, 1 };
EndFor

cl = {};
For t In {0:n}
    Line( t ) = { t, ( t + 1 ) % n };
    cl( t ) = t;
    
    If ( t > ( t + 1 ) % n )
        cl( t ) = - t;
    EndIf
EndFor

Printf( cl );

Curve Loop( 0 ) = { cl[] };

// Plane Surface(1) = {1};



