#include "reco_rings.h"

using TI = GpuVolRef::TI;
using TF = GpuVolRef::TF;

void reco_rings( GpuVolRef reco, GpuVolRef rota, TI /*bx*/, TI /*ix*/ ) {
    for( TI z = 0; z < reco.lz; ++z ) {
        for( TI y = 0; y < reco.ly; ++y ) {
            for( TI x = 0; x < reco.lx; ++x ) {
                reco( x, y, z ) = x;
            }
        }
    }
}
