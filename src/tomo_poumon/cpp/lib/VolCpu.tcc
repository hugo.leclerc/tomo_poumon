#include "Assert.h"
#include "VolCpu.h"
#include "Stream.h"
#include "Math.h"

#include <algorithm>
#include <fstream>
#include <cstring>
#include <sstream>
// #include <cnpy.h>

template<class TF,class PT>
VolCpu<TF,PT>::VolCpu( Shape shape, PT alig ) : VolCpu( shape, { ceil( shape[ 0 ], alig ), shape[ 1 ], shape[ 2 ] }, alig ) {
}

template<class TF,class PT>
VolCpu<TF,PT>::VolCpu( Shape shape, Shape storage, PT /*alig*/ ) : storage( storage ), shape( shape ) {
    storage_acc[ 0 ] = storage[ 0 ];
    storage_acc[ 1 ] = storage_acc[ 0 ] * storage[ 1 ];
    storage_acc[ 2 ] = storage_acc[ 1 ] * storage[ 2 ];

    content = storage_acc[ 2 ] ? (TF *)malloc( storage_acc[ 2 ] * sizeof( TF ) ) : nullptr;
}

template<class TF,class PT>
VolCpu<TF,PT>::VolCpu( std::string filename ) : VolCpu() {
    cnpy::NpyArray ilot = cnpy::npy_load( filename );
    Shape s{ PT( ilot.shape[ 2 ] ), PT( ilot.shape[ 1 ] ), PT( ilot.shape[ 0 ] ) };
    resize( s, s );

    ASSERT( ilot.word_size == sizeof( TF ), "..." );
    std::memcpy( content, ilot.data<TF>(), storage_acc[ 2 ] * sizeof( TF ) );
}

template<class TF,class PT>
VolCpu<TF,PT>::~VolCpu() {
    free( content );
}

template<class TF,class PT>
void VolCpu<TF,PT>::for_each_coord( const std::function<void(Shape)> &f ) const {
    Shape coord;
    for( coord[ 2 ] = 0; coord[ 2 ] < shape[ 2 ]; ++coord[ 2 ] )
        for( coord[ 1 ] = 0; coord[ 1 ] < shape[ 1 ]; ++coord[ 1 ] )
            for( coord[ 0 ] = 0; coord[ 0 ] < shape[ 0 ]; ++coord[ 0 ] )
                f( coord );
}

template<class TF, class PT>
TF &VolCpu<TF,PT>::operator()( Shape coords ) const {
    return content[ storage_acc[ 1 ] * coords[ 2 ] + storage_acc[ 0 ] * coords[ 1 ] + coords[ 0 ] ];
}

template<class TF, class PT>
TF &VolCpu<TF,PT>::operator()( PT x, PT y, PT z ) const {
    return content[ storage_acc[ 1 ] * z + storage_acc[ 0 ] * y + x ];
}

template<class TF,class PT>
void VolCpu<TF,PT>::resize( Shape shape, int alig ) {
    resize( shape, { ceil( shape[ 0 ], alig ), shape[ 1 ], shape[ 2 ] } );
}

template<class TF,class PT>
void VolCpu<TF,PT>::resize( Shape shape, Shape storage ) {
    if ( content )
        free( content );

    this->shape      = shape;
    this->storage    = storage;
    storage_acc[ 0 ] = storage[ 0 ];
    storage_acc[ 1 ] = storage_acc[ 0 ] * storage[ 1 ];
    storage_acc[ 2 ] = storage_acc[ 1 ] * storage[ 2 ];

    content = storage_acc[ 2 ] ? (TF *)malloc( storage_acc[ 2 ] * sizeof( TF ) ) : nullptr;
}

template<class TF,class PT>
void VolCpu<TF,PT>::save_xdmf( const std::string &filename ) const {
    // .bin
    std::string filename_bin = filename + ".bin";
    std::ofstream bin( filename_bin.c_str() );
    bin.write( (const char *)content, storage_acc[ 2 ] * sizeof( TF ) );

    // .xdmf
    std::string filename_xdmf = filename + ".xdmf";
    std::ofstream xdmf( filename_xdmf.c_str() );

    xdmf << "<?xml version='1.0' ?>\n";
    xdmf << "<!DOCTYPE Xdmf SYSTEM 'Xdmf.dtd' []>\n";
    xdmf << "<Xdmf xmlns:xi='http://www.w3.org/2001/XInclude' Version='2.0'>\n";
    xdmf << "<Domain>\n";
    xdmf << "    <Topology name='topo' TopologyType='3DCoRectMesh'\n";
    xdmf << "        Dimensions='" << shape[ 2 ] + 1 << " " << shape[ 1 ] + 1 << " " << shape[ 0 ] + 1 << "'>\n";
    xdmf << "    </Topology>\n";
    xdmf << "    <Geometry name='geo' Type='ORIGIN_DXDYDZ'>\n";
    xdmf << "        <!-- Origin -->\n";
    xdmf << "        <DataItem Format='XML' Dimensions='3'>\n";
    xdmf << "            0.0 0.0 0.0\n";
    xdmf << "        </DataItem>\n";
    xdmf << "        <!-- DxDyDz -->\n";
    xdmf << "        <DataItem Format='XML' Dimensions='3'>\n";
    xdmf << "            1.0 1.0 1.0\n";
    xdmf << "        </DataItem>\n";
    xdmf << "    </Geometry>\n";
    xdmf << "    <Grid Name='T1' GridType='Uniform'>\n";
    xdmf << "        <Topology Reference='/Xdmf/Domain/Topology[1]'/>\n";
    xdmf << "        <Geometry Reference='/Xdmf/Domain/Geometry[1]'/>\n";
    xdmf << "        <Attribute Name='value' Center='Cell'>\n";
    xdmf << "            <DataItem Format='Binary'\n";
    xdmf << "             DataType='Float' Precision='4' Endian='Little'\n";
    xdmf << "             Dimensions='" << shape[ 2 ] << " " << shape[ 1 ] << " " << storage_acc[ 0 ] << "'>\n";
    xdmf << "                " << filename << ".bin\n";
    xdmf << "            </DataItem>\n";
    xdmf << "        </Attribute>\n";
    xdmf << "    </Grid>\n";
    xdmf << "</Domain>\n";
    xdmf << "</Xdmf>\n";
}

template<class TF,class PT>
void VolCpu<TF,PT>::load_xdmf( const std::string &filename ) {
    // xdmf
    std::ifstream xdmf( filename.c_str() );
    std::string bin_filename;
    std::string line;
    Shape dimensions;
    bool prev_is_dim = false;
    while ( std::getline( xdmf, line ) ) {
        auto p = line.find( "Dimensions=" );
        if ( p != line.npos ) {
            auto d = line.rfind( "'" );
            std::istringstream in( line.substr( p + 12, d - ( p + 12 ) ) );
            in >> dimensions[ 2 ] >> dimensions[ 1 ] >> dimensions[ 0 ];
            prev_is_dim = true;
        } else if ( prev_is_dim ) {
            bin_filename = line;
            prev_is_dim = false;
        }
    }
    bin_filename.erase( bin_filename.begin(), std::find_if( bin_filename.begin(), bin_filename.end(), []( int ch ) {
        return ! std::isspace( ch );
    } ) );
    bin_filename.erase( std::find_if( bin_filename.rbegin(), bin_filename.rend(), []( int ch ) {
        return ! std::isspace( ch );
    } ).base(), bin_filename.end() );

    // bin
    resize( dimensions );
    std::ifstream bin( bin_filename.c_str() );
    bin.readsome( (char *)content, storage_acc[ 2 ] * sizeof( TF ) );
}

//template<class TF,class PT>
//void VolCpu<TF,PT>::save_xdmf( const std::string &filename, int num_der ) const {
//    // binary data
//    std::vector<TF> whole_copy( storage_acc[ 2 ] );
//    cudaMemcpy( whole_copy.data(), content, storage_acc[ 2 ] * sizeof( TF ), cudaMemcpyDeviceToHost );

//    std::vector<typename TF::T> copy( storage_acc[ 2 ] );
//    for( std::size_t i = 0; i < storage_acc[ 2 ]; ++i )
//        copy[ i ] = whole_copy[ i ].values[ num_der ];

//    std::string filename_bin = filename + ".bin";
//    std::ofstream bin( filename_bin.c_str() );
//    bin.write( (const char *)copy.data(), storage_acc[ 2 ] * sizeof( typename TF::T ) );

//    // xdmf
//    std::string filename_xdmf = filename + ".xdmf";
//    std::ofstream xdmf( filename_xdmf.c_str() );

//    xdmf << "<?xml version='1.0' ?>\n";
//    xdmf << "<!DOCTYPE Xdmf SYSTEM 'Xdmf.dtd' []>\n";
//    xdmf << "<Xdmf xmlns:xi='http://www.w3.org/2001/XInclude' Version='2.0'>\n";
//    xdmf << "<Domain>\n";
//    xdmf << "    <Topology name='topo' TopologyType='3DCoRectMesh'\n";
//    xdmf << "        Dimensions='" << shape[ 2 ] + 1 << " " << shape[ 1 ] + 1 << " " << shape[ 0 ] + 1 << "'>\n";
//    xdmf << "    </Topology>\n";
//    xdmf << "    <Geometry name='geo' Type='ORIGIN_DXDYDZ'>\n";
//    xdmf << "        <!-- Origin -->\n";
//    xdmf << "        <DataItem Format='XML' Dimensions='3'>\n";
//    xdmf << "            0.0 0.0 0.0\n";
//    xdmf << "        </DataItem>\n";
//    xdmf << "        <!-- DxDyDz -->\n";
//    xdmf << "        <DataItem Format='XML' Dimensions='3'>\n";
//    xdmf << "            1.0 1.0 1.0\n";
//    xdmf << "        </DataItem>\n";
//    xdmf << "    </Geometry>\n";
//    xdmf << "    <Grid Name='T1' GridType='Uniform'>\n";
//    xdmf << "        <Topology Reference='/Xdmf/Domain/Topology[1]'/>\n";
//    xdmf << "        <Geometry Reference='/Xdmf/Domain/Geometry[1]'/>\n";
//    xdmf << "        <Attribute Name='value' Center='Cell'>\n";
//    xdmf << "            <DataItem Format='Binary'\n";
//    xdmf << "             DataType='Float' Precision='4' Endian='Little'\n";
//    xdmf << "             Dimensions='" << shape[ 2 ] << " " << shape[ 1 ] << " " << storage_acc[ 0 ] << "'>\n";
//    xdmf << "                " << filename.substr( filename.rfind( "/" ) + 1 ) << ".bin\n";
//    xdmf << "            </DataItem>\n";
//    xdmf << "        </Attribute>\n";
//    xdmf << "    </Grid>\n";
//    xdmf << "</Domain>\n";
//    xdmf << "</Xdmf>\n";
//}

