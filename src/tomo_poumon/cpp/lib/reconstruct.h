#pragma once

#include "GpuVol.h"

void reco_ring(
    GpuVol &reco, TI reco_x, TI reco_y, TI reco_z, TI proj_x, TI proj_y, TI proj_a, const GpuVol &rota, TI beg_y, TI len_y, TI pos_x, TI pos_y
);

//void reconstruct(
//    GpuVol &reco, const GpuVol &proj, GpuVol &rota,
//    GpuVol::TI len_reco_x, GpuVol::TI len_reco_y, GpuVol::TI len_reco_z,
//    GpuVol::TF beg_reco_x, GpuVol::TF beg_reco_y, GpuVol::TF beg_reco_z,
//    GpuVol::TF inc_reco_x, GpuVol::TF inc_reco_y, GpuVol::TF inc_reco_z,
//    GpuVol::TI len_proj_x, GpuVol::TI len_proj_y, GpuVol::TI len_proj_a,
//    int order
//);


//void make_medi( TF *medi, const TF *orig, TI orig_x, TI orig_y, TI orig_z, TI length, TI thickness );
