#pragma once

#include <functional>
#include <array>

/**
*/
template<class TF,class _PT=std::size_t>
class VolCpu {
public:
    using    Shape                 = std::array<_PT,3>;
    using    PT                    = _PT;

    /* */    VolCpu                ( Shape shape = { 0, 0, 0 }, PT alig = 128 / sizeof( TF ) );
    /* */    VolCpu                ( Shape shape, Shape storage, PT alig );
    /* */    VolCpu                ( std::string filename );
    /* */   ~VolCpu                ();

    TF&      operator()            ( Shape coords ) const;
    TF&      operator()            ( PT x, PT y, PT z ) const;

    void     for_each_coord        ( const std::function<void( Shape coord )> &f ) const;

    void     save_xdmf             ( const std::string &base_filename ) const;
    void     load_xdmf             ( const std::string &base_filename );

    void     resize                ( Shape shape, int alig = 128 / sizeof( TF ) );
    void     resize                ( Shape shape, Shape storage );

    PT       nb_bytes              () const { return storage_acc[ 2 ] * sizeof( TF ); }

    Shape    storage_acc;          ///< inclusive cumulative product of shape
    TF      *content;
    Shape    storage;
    Shape    shape;
};

#include "VolCpu.tcc"

