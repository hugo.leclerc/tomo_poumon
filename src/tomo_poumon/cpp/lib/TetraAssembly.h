#pragma once

#include "VtkOutput.h"
#include "Point3.h"
#include <functional>

/**
*/
class TetraAssembly {
public:
    using                    TI             = int;
    using                    TF             = double;
    using                    Pt             = Point3<TF>;

    /* */                    TetraAssembly  ( const TetraAssembly &that );
    /* */                    TetraAssembly  ();
    /* */                   ~TetraAssembly  ();

    void                     operator=      ( const TetraAssembly &that );

    void                     init_with_tetra( Pt A, Pt B, Pt C, Pt D );
    void                     init_with_box  ( Pt min_pos, Pt max_pos );

    void                     write_to_stream( std::ostream &os ) const;
    void                     display_vtk    ( VtkOutput &vo );
    Pt                       barycenter     () const;
    TI                       nb_tetras      () const { return tetras_size; }
    TF                       measure        () const;
    bool                     empty          () const { return tetras_size == 0; }

    void                     for_each_node  ( const std::function<void(TF &x,TF &y,TF &z)> &f );
    void                     plane_cut      ( Pt pos, Pt dir );

private:
    using                    F4             = std::array<TF,4>;
    static F4*               alloc_F4       ( TI size ) { return new ( aligned_alloc( alignof( F4 ), sizeof( F4 ) * size ) ) F4[ size ]; }

    void                     check_tetra    ( const F4 *tetra_xs, const F4 *tetra_ys, const F4 *tetra_zs, TI num_tetra );

    TI                       tetras_size;
    TI                       tetras_rese;
    F4*                      tetra_xs;      ///<
    F4*                      tetra_ys;      ///<
    F4*                      tetra_zs;      ///<

    TI                       new_tetras_rese;
    F4*                      new_tetra_xs;  ///<
    F4*                      new_tetra_ys;  ///<
    F4*                      new_tetra_zs;  ///<
};
