#include "TetraAssembly.h"
#include "Stream.h"
#include "Assert.h"
#include <cstring>

TetraAssembly::TetraAssembly( const TetraAssembly &that ) : TetraAssembly() {
    std::memcpy( tetra_xs, that.tetra_xs, sizeof( F4 ) * that.tetras_size );
    std::memcpy( tetra_ys, that.tetra_ys, sizeof( F4 ) * that.tetras_size );
    std::memcpy( tetra_zs, that.tetra_zs, sizeof( F4 ) * that.tetras_size );
    tetras_size = that.tetras_size;
}

TetraAssembly::TetraAssembly() {
    tetras_size = 0;
    tetras_rese = 1024 * 8;
    tetra_xs = alloc_F4( tetras_rese );
    tetra_ys = alloc_F4( tetras_rese );
    tetra_zs = alloc_F4( tetras_rese );

    new_tetras_rese = 1024 * 8;
    new_tetra_xs = alloc_F4( new_tetras_rese );
    new_tetra_ys = alloc_F4( new_tetras_rese );
    new_tetra_zs = alloc_F4( new_tetras_rese );
}

TetraAssembly::~TetraAssembly() {
    std::free( tetra_xs );
    std::free( tetra_ys );
    std::free( tetra_zs );

    std::free( new_tetra_xs );
    std::free( new_tetra_ys );
    std::free( new_tetra_zs );
}

void TetraAssembly::operator=( const TetraAssembly &that ) {
    std::memcpy( tetra_xs, that.tetra_xs, sizeof( F4 ) * that.tetras_size );
    std::memcpy( tetra_ys, that.tetra_ys, sizeof( F4 ) * that.tetras_size );
    std::memcpy( tetra_zs, that.tetra_zs, sizeof( F4 ) * that.tetras_size );
    tetras_size = that.tetras_size;
}

void TetraAssembly::init_with_tetra( Pt A, Pt B, Pt C, Pt D ) {
    tetras_size = 1;

    tetra_xs[ 0 ] = { A[ 0 ], B[ 0 ], C[ 0 ], D[ 0 ] };
    tetra_ys[ 0 ] = { A[ 1 ], B[ 1 ], C[ 1 ], D[ 1 ] };
    tetra_zs[ 0 ] = { A[ 2 ], B[ 2 ], C[ 2 ], D[ 2 ] };
}

void TetraAssembly::init_with_box( Pt min_pos, Pt max_pos ) {
    tetras_size = 5;

    //                0;            4;            5;            6;
    tetra_xs[ 0 ] = { min_pos[ 0 ], min_pos[ 0 ], max_pos[ 0 ], min_pos[ 0 ] };
    tetra_ys[ 0 ] = { min_pos[ 1 ], min_pos[ 1 ], min_pos[ 1 ], max_pos[ 1 ] };
    tetra_zs[ 0 ] = { min_pos[ 2 ], max_pos[ 2 ], max_pos[ 2 ], max_pos[ 2 ] };

    //                3;            5;            7;            6;
    tetra_xs[ 1 ] = { max_pos[ 0 ], max_pos[ 0 ], max_pos[ 0 ], min_pos[ 0 ] };
    tetra_ys[ 1 ] = { max_pos[ 1 ], min_pos[ 1 ], max_pos[ 1 ], max_pos[ 1 ] };
    tetra_zs[ 1 ] = { min_pos[ 2 ], max_pos[ 2 ], max_pos[ 2 ], max_pos[ 2 ] };

    //                0;            6;            3;            2;
    tetra_xs[ 2 ] = { min_pos[ 0 ], min_pos[ 0 ], max_pos[ 0 ], min_pos[ 0 ] };
    tetra_ys[ 2 ] = { min_pos[ 1 ], max_pos[ 1 ], max_pos[ 1 ], max_pos[ 1 ] };
    tetra_zs[ 2 ] = { min_pos[ 2 ], max_pos[ 2 ], min_pos[ 2 ], min_pos[ 2 ] };

    //                0;            1;            3;            5;
    tetra_xs[ 3 ] = { min_pos[ 0 ], max_pos[ 0 ], max_pos[ 0 ], max_pos[ 0 ] };
    tetra_ys[ 3 ] = { min_pos[ 1 ], min_pos[ 1 ], max_pos[ 1 ], min_pos[ 1 ] };
    tetra_zs[ 3 ] = { min_pos[ 2 ], min_pos[ 2 ], min_pos[ 2 ], max_pos[ 2 ] };

    //                0;            6;            5;            3;
    tetra_xs[ 4 ] = { min_pos[ 0 ], min_pos[ 0 ], max_pos[ 0 ], max_pos[ 0 ] };
    tetra_ys[ 4 ] = { min_pos[ 1 ], max_pos[ 1 ], min_pos[ 1 ], max_pos[ 1 ] };
    tetra_zs[ 4 ] = { min_pos[ 2 ], max_pos[ 2 ], max_pos[ 2 ], min_pos[ 2 ] };
}

void TetraAssembly::write_to_stream( std::ostream &os ) const {
    for( TI i = 0; i < tetras_size; ++i ) {
        for( TI j = 0; j < 4; ++j )
            os << ( j ? ";" : "" ) << tetra_xs[ i ][ j ] << "," << tetra_ys[ i ][ j ] << "," << tetra_zs[ i ][ j ];
        os << "\n";
    }
}

void TetraAssembly::display_vtk( VtkOutput &vo ) {
    for( TI i = 0; i < tetras_size; ++i ) {
        std::array<VtkOutput::Pt,4> p;
        for( TI j = 0; j < 4; ++j )
            p[ j ] = { tetra_xs[ i ][ j ], tetra_ys[ i ][ j ], tetra_zs[ i ][ j ] };
        vo.add_tetra( p );
    }
}

TetraAssembly::Pt TetraAssembly::barycenter() const {
    Pt res = TF( 0 );
    TF wgt = 0;

    for( TI i = 0; i < tetras_size; ++i ) {
        TF x0 = tetra_xs[ i ][ 0 ];
        TF y0 = tetra_ys[ i ][ 0 ];
        TF z0 = tetra_zs[ i ][ 0 ];

        TF x1 = tetra_xs[ i ][ 1 ];
        TF y1 = tetra_ys[ i ][ 1 ];
        TF z1 = tetra_zs[ i ][ 1 ];

        TF x2 = tetra_xs[ i ][ 2 ];
        TF y2 = tetra_ys[ i ][ 2 ];
        TF z2 = tetra_zs[ i ][ 2 ];

        TF x3 = tetra_xs[ i ][ 3 ];
        TF y3 = tetra_ys[ i ][ 3 ];
        TF z3 = tetra_zs[ i ][ 3 ];

        Pt C( x0 + x1 + x2 + x3, y0 + y1 + y2 + y3, z0 + z1 + z2 + z3 );

        x1 -= x0;
        y1 -= y0;
        z1 -= z0;

        x2 -= x0;
        y2 -= y0;
        z2 -= z0;

        x3 -= x0;
        y3 -= y0;
        z3 -= z0;

        TF pa = x1 * ( y2 * z3 - y3 * z2 ) - y1 * ( x2 * z3 - x3 * z2 ) + z1 * ( x2 * y3 - x3 * y2 );

        res += pa * C;
        wgt += pa;
    }

    return TF( 1 ) / 4 * res / ( wgt + ( wgt == 0 ) );
}

TetraAssembly::TF TetraAssembly::measure() const {
    TF res = 0;

    for( TI i = 0; i < tetras_size; ++i ) {
        TF x0 = tetra_xs[ i ][ 0 ];
        TF y0 = tetra_ys[ i ][ 0 ];
        TF z0 = tetra_zs[ i ][ 0 ];

        TF x1 = tetra_xs[ i ][ 1 ] - x0;
        TF y1 = tetra_ys[ i ][ 1 ] - y0;
        TF z1 = tetra_zs[ i ][ 1 ] - z0;

        TF x2 = tetra_xs[ i ][ 2 ] - x0;
        TF y2 = tetra_ys[ i ][ 2 ] - y0;
        TF z2 = tetra_zs[ i ][ 2 ] - z0;

        TF x3 = tetra_xs[ i ][ 3 ] - x0;
        TF y3 = tetra_ys[ i ][ 3 ] - y0;
        TF z3 = tetra_zs[ i ][ 3 ] - z0;

        TF pa = x1 * ( y2 * z3 - y3 * z2 ) - y1 * ( x2 * z3 - x3 * z2 ) + z1 * ( x2 * y3 - x3 * y2 );
        res += pa;
    }

    return TF( 1 ) / 6 * res;
}

void TetraAssembly::for_each_node( const std::function<void( TF &, TF &, TF &)> &f) {
    for( TI i = 0; i < tetras_size; ++i ) {
        f( tetra_xs[ i ][ 0 ], tetra_ys[ i ][ 0 ], tetra_zs[ i ][ 0 ] );
        f( tetra_xs[ i ][ 1 ], tetra_ys[ i ][ 1 ], tetra_zs[ i ][ 1 ] );
        f( tetra_xs[ i ][ 2 ], tetra_ys[ i ][ 2 ], tetra_zs[ i ][ 2 ] );
        f( tetra_xs[ i ][ 3 ], tetra_ys[ i ][ 3 ], tetra_zs[ i ][ 3 ] );
    }
}

void TetraAssembly::plane_cut( Pt pos, Pt dir ) {
    TI new_tetras_size = 0;
    for( TI num_tetra = 0; num_tetra < tetras_size; ++num_tetra ) {
        TF x0 = tetra_xs[ num_tetra ][ 0 ], y0 = tetra_ys[ num_tetra ][ 0 ], z0 = tetra_zs[ num_tetra ][ 0 ];
        TF x1 = tetra_xs[ num_tetra ][ 1 ], y1 = tetra_ys[ num_tetra ][ 1 ], z1 = tetra_zs[ num_tetra ][ 1 ];
        TF x2 = tetra_xs[ num_tetra ][ 2 ], y2 = tetra_ys[ num_tetra ][ 2 ], z2 = tetra_zs[ num_tetra ][ 2 ];
        TF x3 = tetra_xs[ num_tetra ][ 3 ], y3 = tetra_ys[ num_tetra ][ 3 ], z3 = tetra_zs[ num_tetra ][ 3 ];

        TF s0 = ( x0 - pos.x ) * dir.x + ( y0 - pos.y ) * dir.y + ( z0 - pos.z ) * dir.z;
        TF s1 = ( x1 - pos.x ) * dir.x + ( y1 - pos.y ) * dir.y + ( z1 - pos.z ) * dir.z;
        TF s2 = ( x2 - pos.x ) * dir.x + ( y2 - pos.y ) * dir.y + ( z2 - pos.z ) * dir.z;
        TF s3 = ( x3 - pos.x ) * dir.x + ( y3 - pos.y ) * dir.y + ( z3 - pos.z ) * dir.z;

        bool o0 = s0 > 0;
        bool o1 = s1 > 0;
        bool o2 = s2 > 0;
        bool o3 = s3 > 0;

        #define ADD_TETRA( a, b, c, d ) \
            if ( ( x##b - x##a ) * ( ( y##c - y##a ) * ( z##d - z##a ) - ( y##d - y##a ) * ( z##c - z##a ) ) - \
                 ( y##b - y##a ) * ( ( x##c - x##a ) * ( z##d - z##a ) - ( x##d - x##a ) * ( z##c - z##a ) ) + \
                 ( z##b - z##a ) * ( ( x##c - x##a ) * ( y##d - y##a ) - ( x##d - x##a ) * ( y##c - y##a ) ) > 1e-6 ) { \
                new_tetra_xs[ new_tetras_size ] = { x##a, x##b, x##c, x##d }; \
                new_tetra_ys[ new_tetras_size ] = { y##a, y##b, y##c, y##d }; \
                new_tetra_zs[ new_tetras_size ] = { z##a, z##b, z##c, z##d }; \
                ++new_tetras_size; \
            }

        auto cut_1_outside = [&]( TF x0, TF y0, TF z0, TF x1, TF y1, TF z1, TF x2, TF y2, TF z2, TF x3, TF y3, TF z3, TF s4, TF s5, TF s6 ) {
            TF x4 = x0 + s4 * ( x1 - x0 );
            TF x5 = x0 + s5 * ( x2 - x0 );
            TF x6 = x0 + s6 * ( x3 - x0 );

            TF y4 = y0 + s4 * ( y1 - y0 );
            TF y5 = y0 + s5 * ( y2 - y0 );
            TF y6 = y0 + s6 * ( y3 - y0 );

            TF z4 = z0 + s4 * ( z1 - z0 );
            TF z5 = z0 + s5 * ( z2 - z0 );
            TF z6 = z0 + s6 * ( z3 - z0 );

            ADD_TETRA( 1, 2, 5, 3 );
            ADD_TETRA( 1, 5, 4, 3 );
            ADD_TETRA( 4, 6, 3, 5 );
        };


        auto cut_2_outside = [&]( TF x0, TF y0, TF z0, TF x1, TF y1, TF z1, TF x2, TF y2, TF z2, TF x3, TF y3, TF z3, TF s02, TF s03, TF s12, TF s13 ) {
            TF x4 = x0 + s02 * ( x2 - x0 );
            TF x5 = x0 + s03 * ( x3 - x0 );
            TF x6 = x1 + s12 * ( x2 - x1 );
            TF x7 = x1 + s13 * ( x3 - x1 );

            TF y4 = y0 + s02 * ( y2 - y0 );
            TF y5 = y0 + s03 * ( y3 - y0 );
            TF y6 = y1 + s12 * ( y2 - y1 );
            TF y7 = y1 + s13 * ( y3 - y1 );

            TF z4 = z0 + s02 * ( z2 - z0 );
            TF z5 = z0 + s03 * ( z3 - z0 );
            TF z6 = z1 + s12 * ( z2 - z1 );
            TF z7 = z1 + s13 * ( z3 - z1 );

            ADD_TETRA( 2, 4, 6, 5 );
            ADD_TETRA( 2, 3, 5, 6 );
            ADD_TETRA( 3, 7, 5, 6 );
        };


        auto cut_3_outside = [&]( TF x0, TF y0, TF z0, TF x1, TF y1, TF z1, TF x2, TF y2, TF z2, TF x3, TF y3, TF z3, TF s0, TF s1, TF s2 ) {
            TF x4 = x3 + s0 * ( x0 - x3 );
            TF x5 = x3 + s1 * ( x1 - x3 );
            TF x6 = x3 + s2 * ( x2 - x3 );

            TF y4 = y3 + s0 * ( y0 - y3 );
            TF y5 = y3 + s1 * ( y1 - y3 );
            TF y6 = y3 + s2 * ( y2 - y3 );

            TF z4 = z3 + s0 * ( z0 - z3 );
            TF z5 = z3 + s1 * ( z1 - z3 );
            TF z6 = z3 + s2 * ( z2 - z3 );

            ADD_TETRA( 4, 5, 6, 3 );
        };

        #define C1( A, B, C, D ) cut_1_outside( x##A, y##A, z##A, x##B, y##B, z##B, x##C, y##C, z##C, x##D, y##D, z##D, s##A / ( s##A - s##B ), s##A / ( s##A - s##C ), s##A / ( s##A - s##D ) )
        #define C2( A, B, C, D ) cut_2_outside( x##A, y##A, z##A, x##B, y##B, z##B, x##C, y##C, z##C, x##D, y##D, z##D, s##A / ( s##A - s##C ), s##A / ( s##A - s##D ), s##B / ( s##B - s##C ), s##B / ( s##B - s##D ) )
        #define C3( A, B, C, D ) cut_3_outside( x##A, y##A, z##A, x##B, y##B, z##B, x##C, y##C, z##C, x##D, y##D, z##D, s##D / ( s##D - s##A ), s##D / ( s##D - s##B ), s##D / ( s##D - s##C ) )

        switch ( 1 * o0 + 2 * o1 + 4 * o2 + 8 * o3 ) {
        case 1 * 0 + 2 * 0 + 4 * 0 + 8 * 0: // all inside
            new_tetra_xs[ new_tetras_size ] = tetra_xs[ num_tetra ];
            new_tetra_ys[ new_tetras_size ] = tetra_ys[ num_tetra ];
            new_tetra_zs[ new_tetras_size ] = tetra_zs[ num_tetra ];
            ++new_tetras_size;
            break;
        case 1 * 1 + 2 * 0 + 4 * 0 + 8 * 0: // o0
            C1( 0, 1, 2, 3 );
            break;
        case 1 * 0 + 2 * 1 + 4 * 0 + 8 * 0: // o1
            C1( 1, 2, 0, 3 );
            break;
        case 1 * 1 + 2 * 1 + 4 * 0 + 8 * 0: // o0, o1
            C2( 0, 1, 2, 3 );
            break;
        case 1 * 0 + 2 * 0 + 4 * 1 + 8 * 0: // o2
            C1( 2, 0, 1, 3 );
            break;
        case 1 * 1 + 2 * 0 + 4 * 1 + 8 * 0: // o0, o2
            C2( 0, 2, 3, 1 );
            break;
        case 1 * 0 + 2 * 1 + 4 * 1 + 8 * 0: // o1, o2
            C2( 1, 2, 0, 3 );
            break;
        case 1 * 1 + 2 * 1 + 4 * 1 + 8 * 0: // o0, o1, o2
            C3( 0, 1, 2, 3 );
            break;
        case 1 * 0 + 2 * 0 + 4 * 0 + 8 * 1: // o3
            C1( 3, 0, 2, 1 );
            break;
        case 1 * 1 + 2 * 0 + 4 * 0 + 8 * 1: // o0, o3
            C2( 0, 3, 1, 2 );
            break;
        case 1 * 0 + 2 * 1 + 4 * 0 + 8 * 1: // o1, o3
            C2( 1, 3, 2, 0 );
            break;
        case 1 * 1 + 2 * 1 + 4 * 0 + 8 * 1: // o0, o1, o3
            C3( 0, 3, 1, 2 );
            break;
        case 1 * 0 + 2 * 0 + 4 * 1 + 8 * 1: // o2, o3
            C2( 2, 3, 0, 1 );
            break;
        case 1 * 1 + 2 * 0 + 4 * 1 + 8 * 1: // o0, o2, o3
            C3( 0, 2, 3, 1 );
            break;
        case 1 * 0 + 2 * 1 + 4 * 1 + 8 * 1: // o1, o2, o3
            C3( 1, 3, 2, 0 );
            break;
        case 1 * 1 + 2 * 1 + 4 * 1 + 8 * 1: // o0, o1, o2, o3
            // all outside
            break;
        }
    }

    tetras_size = new_tetras_size;
    std::swap( tetra_xs, new_tetra_xs );
    std::swap( tetra_ys, new_tetra_ys );
    std::swap( tetra_zs, new_tetra_zs );

    ASSERT( new_tetras_size <= new_tetras_rese, "" );
}

void TetraAssembly::check_tetra( const F4 *tetra_xs, const F4 *tetra_ys, const F4 *tetra_zs, TI num_tetra ) {
    TF x0 = tetra_xs[ num_tetra ][ 0 ];
    TF y0 = tetra_ys[ num_tetra ][ 0 ];
    TF z0 = tetra_zs[ num_tetra ][ 0 ];

    TF x1 = tetra_xs[ num_tetra ][ 1 ] - x0;
    TF y1 = tetra_ys[ num_tetra ][ 1 ] - y0;
    TF z1 = tetra_zs[ num_tetra ][ 1 ] - z0;

    TF x2 = tetra_xs[ num_tetra ][ 2 ] - x0;
    TF y2 = tetra_ys[ num_tetra ][ 2 ] - y0;
    TF z2 = tetra_zs[ num_tetra ][ 2 ] - z0;

    TF x3 = tetra_xs[ num_tetra ][ 3 ] - x0;
    TF y3 = tetra_ys[ num_tetra ][ 3 ] - y0;
    TF z3 = tetra_zs[ num_tetra ][ 3 ] - z0;

    TF pa = x1 * ( y2 * z3 - y3 * z2 ) - y1 * ( x2 * z3 - x3 * z2 ) + z1 * ( x2 * y3 - x3 * y2 );
    ASSERT( pa >= -1e-6, "" );
}


