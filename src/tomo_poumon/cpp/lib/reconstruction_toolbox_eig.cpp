#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <fstream>
#include <queue>
#include <cmath>

#include <eigen3/Eigen/Eigenvalues>
#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/Dense>
#include "ThreadPool.h"
#include "Stream.h"
#include "Ad.h"

//// nsmake cpp_flag -march=native
//// nsmake cpp_flag -O3

namespace py = pybind11;
using TI = std::int64_t;
using TF = float;

py::array_t<TF> py_spheriticity( py::array_t<TF> &dist ) {
    std::size_t nb_jobs = thread_pool.nb_threads();
    using TM = Eigen::Matrix<TF,3,3>;
    struct Pt { TI x, y, z; TF c; };
    using std::min;
    using std::exp;
    using std::pow;

    //
    TF th = 1e40;
    for( TI z = 0; z < dist.shape( 0 ); z++ )
        for( TI y = 0; y < dist.shape( 1 ); y++ )
            for( TI x = 0; x < dist.shape( 2 ); x++ )
                th = min( th, dist.at( x, y, z ) );

    // get points on a sphere
    std::vector<Pt> pts;
    const TF std_dev = 6;
    const TI ri = 3 * std_dev;
    for( TI z = -ri; z <= ri; z++ ) {
        for( TI y = -ri; y <= ri; y++ ) {
            for( TI x = -ri; x <= ri; x++ ) {
                TF c = exp( - ( x * x + y * y + z * z ) / ( 2 * std_dev * std_dev ) );
                if ( c >= 0.05 )
                    pts.push_back( { x, y, z, c } );
            }
        }
    }

    // sort them for eviction at the local min
    std::sort( pts.begin(), pts.end(), []( const Pt &a, const Pt &b ) {
        return a.x * a.x + a.y * a.y + a.z * a.z < b.x * b.x + b.y * b.y + b.z * b.z;
    } );

    // true if local maxima for each voxel
    py::array_t<short> locm( { dist.shape( 0 ), dist.shape( 1 ), dist.shape( 2 ) } );
    for( TI z = 0; z < locm.shape( 0 ); z++ )
        for( TI y = 0; y < locm.shape( 1 ); y++ )
            for( TI x = 0; x < locm.shape( 2 ); x++ )
                locm.mutable_at( x, y, z ) = 0;
    thread_pool.execute( nb_jobs, [&]( std::size_t num_job, int ) {
        TI beg_z = 1 + ( num_job + 0 ) * ( dist.shape( 0 ) - 2 ) / nb_jobs;
        TI end_z = 1 + ( num_job + 1 ) * ( dist.shape( 0 ) - 2 ) / nb_jobs;
        for( TI z = beg_z; z < end_z; z++ ) {
            for( TI y = 1; y < dist.shape( 1 ) - 1; y++ ) {
                for( TI x = 1; x < dist.shape( 2 ) - 1; x++ ) {
                    if ( dist.at( x, y, z ) <= th + 1.0 )
                        continue;

                    short is_a_local_max = 1;
                    for( TI nz = z - 1; nz <= z + 1; nz++ )
                        for( TI ny = y - 1; ny <= y + 1; ny++ )
                            for( TI nx = x - 1; nx <= x + 1; nx++ )
                                is_a_local_max &= dist.at( nx, ny, nz ) <= dist.at( x, y, z );

                    locm.mutable_at( x, y, z ) = is_a_local_max;
                }
            }
        }
    } );


    //
    py::array_t<TF> sphe( { dist.shape( 0 ), dist.shape( 1 ), dist.shape( 2 ) } );
    thread_pool.execute( nb_jobs, [&]( std::size_t num_job, int ) {
        TI beg_z = ( num_job + 0 ) * sphe.shape( 0 ) / nb_jobs;
        TI end_z = ( num_job + 1 ) * sphe.shape( 0 ) / nb_jobs;
        for( TI z = beg_z; z < end_z; z++ ) {
            if ( num_job + 1 == nb_jobs )
                std::cout << z << " / " << end_z << std::endl;

            for( TI y = 0; y < sphe.shape( 1 ); y++ ) {
                for( TI x = 0; x < sphe.shape( 2 ); x++ ) {
                    if ( dist.at( x, y, z ) <= th ) {
                        sphe.mutable_at( x, y, z ) = -0.1;
                        continue;
                    }

                    // look for a local minima (for eviction)
                    bool has_local_max = false;
                    TF dx, dy, dz, ds;
                    for( const Pt &pt : pts ) {
                        TI nx = x + pt.x;
                        TI ny = y + pt.y;
                        TI nz = z + pt.z;
                        if ( nx < 0 || ny < 0 || nz < 0 || nx >= dist.shape( 2 ) || ny >= dist.shape( 1 ) || nz >= dist.shape( 0 ) )
                            continue;
                        if ( locm.at( nx, ny, nz ) ) {
                            has_local_max = true;
                            dx = pt.x;
                            dy = pt.y;
                            dz = pt.z;
                            break;
                        }
                    }

                    if ( has_local_max && dx == 0 && dy == 0 && dz == 0 ) {
                        sphe.mutable_at( x, y, z ) = -2.0;
                        continue;
                    }

                    // identification of a polynomial 1 x y z 0.5*x**2 x*y x*z 0.5*y**2 y*z 0.5*z**2
                    const TI nu = 1 + 3 + 3 * ( 3 + 1 ) / 2;
                    Eigen::Matrix<TF,nu,nu> mi;
                    Eigen::Matrix<TF,nu,1> vi;
                    mi.setZero();
                    vi.setZero();

                    for( const Pt &pt : pts ) {
                        TI nx = x + pt.x;
                        TI ny = y + pt.y;
                        TI nz = z + pt.z;
                        if ( nx < 0 || ny < 0 || nz < 0 || nx >= dist.shape( 2 ) || ny >= dist.shape( 1 ) || nz >= dist.shape( 0 ) || dist.at( nx, ny, nz ) <= th + 1.0 )
                            continue;

                        if ( has_local_max && dx * pt.x + dy * pt.y + dz * pt.z >= dx * dx + dy * dy + dz * dz )
                            continue;

                        TF coeffs[] = { 1.0f, 1.0f * pt.x, 1.0f * pt.y, 1.0f * pt.z, 0.5f * pt.x * pt.x, 1.0f * pt.x * pt.y, 1.0f * pt.x * pt.z, 0.5f * pt.y * pt.y, 1.0f * pt.y * pt.z, 0.5f * pt.z * pt.z };
                        for( TI r = 0; r < nu; r++ ) {
                            for( TI c = 0; c < nu; c++ )
                                mi.coeffRef( r, c ) += pt.c * coeffs[ r ] * coeffs[ c ];
                            vi[ r ] += pt.c * coeffs[ r ] * dist.at( nx, ny, nz );
                        }
                    }

                    Eigen::LLT<Eigen::Matrix<TF,nu,nu>> llt( mi );
                    auto si = llt.solve( vi );

                    //  
                    TM m;
                    m.coeffRef( 0, 0 ) = si[ 4 ];
                    m.coeffRef( 0, 1 ) = si[ 5 ];
                    m.coeffRef( 0, 2 ) = si[ 6 ];
                    m.coeffRef( 1, 0 ) = si[ 5 ];
                    m.coeffRef( 1, 1 ) = si[ 7 ];
                    m.coeffRef( 1, 2 ) = si[ 8 ];
                    m.coeffRef( 2, 0 ) = si[ 6 ];
                    m.coeffRef( 2, 1 ) = si[ 8 ];
                    m.coeffRef( 2, 2 ) = si[ 9 ];

                    Eigen::EigenSolver<TM> es( m );
                    auto ev = es.eigenvalues();

                    std::array<TF,3> e{ ev[ 0 ].real(), ev[ 1 ].real(), ev[ 2 ].real() };
                    std::sort( e.begin(), e.end() );

                    if ( e[ 2 ] == e[ 0 ] ) {
                        sphe.mutable_at( x, y, z ) = -2.0;
                        continue;
                    }

                    sphe.mutable_at( x, y, z ) = ( e[ 1 ] - e[ 0 ] ) / ( e[ 2 ] - e[ 0 ] );
                }
            }
        }
    } );

    // unsolved points
    for( TI z = 0; z < sphe.shape( 0 ); z++ ) {
        for( TI y = 0; y < sphe.shape( 1 ); y++ ) {
            for( TI x = 0; x < sphe.shape( 2 ); x++ ) {
                if ( sphe.at( x, y, z ) <= -1.0 || std::isnan( sphe.at( x, y, z ) ) ) {
                    for( const Pt &pt : pts ) {
                        TI nx = x + pt.x;
                        TI ny = y + pt.y;
                        TI nz = z + pt.z;
                        if ( nx < 0 || ny < 0 || nz < 0 || nx >= dist.shape( 2 ) || ny >= dist.shape( 1 ) || nz >= dist.shape( 0 ) )
                            continue;
                        if ( sphe.at( nx, ny, nz ) >= 0 && sphe.at( nx, ny, nz ) <= 1 ) {
                            sphe.mutable_at( x, y, z ) = sphe.at( nx, ny, nz );
                            break;
                        }
                    }
                }
            }
        }
    }


    return sphe;
}

/// domain >= 0 for zones to fill
py::array_t<TF> py_make_lapl( py::array_t<TF> &domain, TF default_value ) {
    using TV = Eigen::Matrix<TF,Eigen::Dynamic,1>;
    using TM = Eigen::SparseMatrix<TF>;
    using Tr = Eigen::Triplet<TF>;

    TI nb_unknowns = 0;
    for( TI z = 0; z < domain.shape( 0 ); ++z )
        for( TI y = 0; y < domain.shape( 1 ); ++y )
            for( TI x = 0; x < domain.shape( 2 ); ++x )
                if ( domain.at( x, y, z ) >= 0 )
                    domain.mutable_at( x, y, z ) = nb_unknowns++;

    TV v( nb_unknowns );
    v.setZero();

    std::vector<Tr> triplets;
    for( TI z = 0; z < domain.shape( 0 ); ++z ) {
        for( TI y = 0; y < domain.shape( 1 ); ++y ) {
            for( TI x = 0; x < domain.shape( 2 ); ++x ) {
                TI num = domain.at( x, y, z );
                if ( num < 0 )
                    continue;

                auto add = [&]( TI ovn ) {
                    if ( ovn >= 0 )
                        triplets.emplace_back( num, ovn, -1 );
                    else if ( ovn == -2 )
                        triplets.emplace_back( num, num, -1 );
                };

                triplets.emplace_back( num, num, 6 );
                add( domain.at( x + 1, y, z ) );
                add( domain.at( x - 1, y, z ) );
                add( domain.at( x, y + 1, z ) );
                add( domain.at( x, y - 1, z ) );
                add( domain.at( x, y, z + 1 ) );
                add( domain.at( x, y, z - 1 ) );
                v[ num ] += 1;
            }
        }
    }

    TM m( nb_unknowns, nb_unknowns );
    m.setFromTriplets( triplets.begin(), triplets.end() );

    Eigen::SparseLU<TM> llt( m );
    TV s = llt.solve( v );

    py::array_t<TF> res( { domain.shape( 0 ), domain.shape( 1 ), domain.shape( 2 ) } );
    for( TI z = 0; z < domain.shape( 0 ); ++z )
        for( TI y = 0; y < domain.shape( 1 ); ++y )
            for( TI x = 0; x < domain.shape( 2 ); ++x )
                res.mutable_at( x, y, z ) = domain.at( x, y, z ) >= 0 ? s[ domain.at( x, y, z ) ] : default_value;
    return res;
}

// ---------------------------- identif -----------------------------
// struct Indentif {
//     using TF = double;

//     template<class T> static T p2( const T &a ) { return a * a; }
//     template<class T> static T p3( const T &a ) { return a * a * a; }

//     template<class T>
//     std::array<T,2> static shape_functions( T c, T m, T x ) {
//         using std::max;
//         return {
//             TF( 4 * M_PI / 3 ) * ( p3( c ) - p3( max( c - x, T( 0 ) ) ) ),
//             p2( m ) - p2( max( m - x, T( 0 ) ) )
//         };
//     }

//     template<class T>
//     T static error( T c, T m, T o, T s0, T s1, py::array_t<TF> &xs, py::array_t<TF> &ys ) {
//         T res = 0;
//         for( std::size_t i = 0; i < ys.shape( 0 ); ++i ) {
//             std::array<T,2> s = shape_functions( c, m, T( xs.at( i ) ) + o );
//             T v = s0 * s[ 0 ] + s1 * s[ 1 ];
//             res += p2( v - ys.at( i ) );
//         }
//         return res;
//     }

//     template<class T>
//     std::vector<T> static values( T c, T m, T o, T s0, T s1, py::array_t<TF> &xs, py::array_t<TF> &ys ) {
//         std::vector<T> res( ys.size() );
//         for( std::size_t i = 0; i < ys.size(); ++i ) {
//             std::array<T,2> s = shape_functions( c, m, T( xs.at( i ) ) + o );
//             res[ i ] = s0 * s[ 0 ] + s1 * s[ 1 ];
//         }
//         return res;
//     }

//     py::array_t<TF> static error_vec( TF c, TF m, TF o, TF s0, TF s1, py::array_t<TF> &xs, py::array_t<TF> &ys ) {
//         py::array_t<TF> res( pybind11::size_t( ys.shape( 0 ) ) );
//         for( std::size_t i = 0; i < ys.shape( 0 ); ++i ) {
//             std::array<TF,2> s = shape_functions( c, m, xs.at( i ) + o );
//             TF v = s0 * s[ 0 ] + s1 * s[ 1 ];
//             res.mutable_at( i ) = v - ys.at( i );
//         }
//         return res;
//     }

//     ResFederer static py_identif( py::array_t<TF> &xs, py::array_t<TF> &ys ) {
//         TF max_x = xs.at( xs.size() - 1 );

//         // init
//         std::array<TF,5> best_V, V; // c, m, o, s0, s1
//         TF best_e = std::numeric_limits<TF>::max();
//         for( V[ 0 ] = 0; V[ 0 ] < max_x; V[ 0 ] += 0.05 ) {
//             for( V[ 1 ] = 0; V[ 1 ] < 2 * max_x; V[ 1 ] += 0.05 ) {
//                 for( V[ 2 ] = 0.0; V[ 2 ] <= 1; V[ 2 ] += 10 ) {
//                     using T = Ad<TF,2,2>;
//                     T s0( 0 ), s1( 0 );
//                     s0.v[ 0 ] = 1;
//                     s1.v[ 1 ] = 1;
//                     T d = error( T( V[ 0 ] ), T( V[ 1 ] ), T( V[ 2 ] ), s0, s1, xs, ys );

//                     TF u = d.m.coeffRef( 0, 0 ) + d.m.coeffRef( 1, 1 );
//                     d.m.coeffRef( 0, 0 ) += 1e-6 * u;
//                     d.m.coeffRef( 1, 1 ) += 1e-6 * u;

//                     Eigen::Matrix<TF,2,1> x = - d.m.colPivHouseholderQr().solve( d.v );
//                     if ( x[ 0 ] < 0 ) x[ 0 ] = 0;
//                     if ( x[ 1 ] < 0 ) x[ 1 ] = 0;
//                     TF e = error( V[ 0 ], V[ 1 ], V[ 2 ], x[ 0 ], x[ 1 ], xs, ys );
//                     if ( best_e > e ) {
//                         best_e = e;
//                         best_V = { V[ 0 ], V[ 1 ], V[ 2 ], x[ 0 ], x[ 1 ] };
//                     }
//                 }
//             }
//         }

//         //
//         V = best_V;
//         // P( best_e, V );

//         // adjust
//         for( std::size_t cpt_iter = 0; cpt_iter < 50; ++cpt_iter ) {
//             using T = Ad<TF,5,2>;
//             std::array<T,5> W;
//             for( std::size_t i = 0; i < V.size(); ++i ) {
//                 W[ i ] = V[ i ];
//                 W[ i ].v[ i ] = 1;
//             }
//             T d = error( W[ 0 ], W[ 1 ], W[ 2 ], W[ 3 ], W[ 4 ], xs, ys );

//             auto x = d.m.colPivHouseholderQr().solve( d.v );
//             std::cout << "e=" << d.x << " v=" << x.transpose() << std::endl;

//             for( std::size_t i = 0; i < V.size(); ++i )
//                 V[ i ] -= 0.1 * x[ i ];
//         }

//         ResFederer res;
//         res.error = error_vec( V[ 0 ], V[ 1 ], V[ 2 ], V[ 3 ], V[ 4 ], xs, ys );
//         res.c  = V[ 0 ];
//         res.m  = V[ 1 ];
//         res.o  = V[ 2 ];
//         res.s0 = V[ 3 ];
//         res.s1 = V[ 4 ];
//         return res;
//     }
// };


// ----------------------------  -----------------------------
struct ResFederer {
    using TF = double;

    TF              radius_cylinder;
    TF              length_cylinder;
    TF              radius_sphere; 
    TF              nb_spheres; 
    py::array_t<TF> error_vec;
    TF              error;
};

struct Federer {
    using TF = double;

    template<class T> static T p2( const T &a ) { return a * a; }
    template<class T> static T p3( const T &a ) { return a * a * a; }
    template<class T> static T pp( const T &a ) { using std::max; return max( a, T( 0 ) ); }

    std::array<TF,2> static shape_functions( TF radius_cylinder, TF radius_sphere, TF r ) {
        return {
            4 * M_PI / 3 * ( p3( radius_sphere ) - p3( pp( radius_sphere - r ) ) ),
            M_PI * ( p2( radius_cylinder ) - p2( pp( radius_cylinder - r ) ) )
        };
    }

    ResFederer static test( py::array_t<TF> &reach, TF radius_cylinder, TF radius_sphere ) {
        // std::cout << radius_cylinder << " " << radius_sphere << std::endl;
        ResFederer res;
        res.radius_cylinder = radius_cylinder;
        res.radius_sphere = radius_sphere;

        Eigen::Matrix<TF,2,2> M;
        Eigen::Matrix<TF,2,1> V;
        M.setZero();
        V.setZero();
        for( pybind11::size_t i = 0; i < reach.shape( 0 ); ++i ) {
            auto coeffs = shape_functions( radius_cylinder, radius_sphere, reach.at( i, 0 ) );
            for( int r = 0; r < 2; ++r ) {
                for( int c = 0; c < 2; ++c )
                    M.coeffRef( r, c ) += coeffs[ r ] * coeffs[ c ];
                V.coeffRef( r ) += coeffs[ r ] * reach.at( i, 1 );
            }
        }

        Eigen::Matrix<TF,2,1> X = M.colPivHouseholderQr().solve( V );
        res.length_cylinder = X[ 1 ];
        res.nb_spheres = X[ 0 ];

        res.error_vec = py::array_t<TF>{ pybind11::size_t( reach.shape( 0 ) ) };
        res.error = 0;
        for( pybind11::size_t i = 0; i < reach.shape( 0 ); ++i ) {
            auto coeffs = shape_functions( radius_cylinder, radius_sphere, reach.at( i, 0 ) );
            TF smu = 0;
            for( int r = 0; r < 2; ++r )
                smu += coeffs[ r ] * X[ r ];
            TF loc = smu - reach.at( i, 1 );
            res.error_vec.mutable_at( i ) = loc;
            res.error += loc * loc;
        }

        return res;
    }

    ResFederer static py_federer( py::array_t<TF> &reach, TF min_rs, TF max_rs, TF min_rc, TF max_rc, TF inc_rs, TF inc_rc ) {
        ResFederer best;
        best.error = std::numeric_limits<TF>::max();
        for( TF rc = min_rc; rc < max_rc; rc += inc_rc ) {
            for( TF rs = min_rs; rs < max_rs; rs += inc_rs ) {
                ResFederer t = test( reach, rc, rs );
                if ( best.error > t.error )
                    best = t;
            }
        }
        return best;
    }
};



// ----------------------------  -----------------------------
PYBIND11_MODULE( reconstruction_toolbox_eig, m ) {
    m.doc() = "reconstruction toolbox";

    pybind11::class_<ResFederer>( m, "ResFederer" )
        .def_readwrite( "radius_cylinder", &ResFederer::radius_cylinder, "" )
        .def_readwrite( "length_cylinder", &ResFederer::length_cylinder, "" )
        .def_readwrite( "radius_sphere"  , &ResFederer::radius_sphere  , "" )
        .def_readwrite( "nb_spheres"     , &ResFederer::nb_spheres     , "" )
        .def_readwrite( "error_vec"      , &ResFederer::error_vec      , "" )
        .def_readwrite( "error"          , &ResFederer::error          , "" )
    ;


    m.def( "spheriticity", &py_spheriticity );
    m.def( "make_lapl"   , &py_make_lapl    );
    m.def( "federer"     , &Federer::py_federer, py::arg( "input" ), py::arg( "min_rs" ), py::arg( "max_rs" ), py::arg( "min_rc" ), py::arg( "max_rc" ), py::arg( "inc_rs" ), py::arg( "inc_rc" ) );
}

