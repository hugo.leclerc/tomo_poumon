#pragma once

#include <pybind11/numpy.h>
#include <cuda_runtime.h>
#include "cuda_config.h"

/**
 * a 3D array in the GPU */
struct GpuVolRef {
    using TF         = float;
    using TI         = pybind11::ssize_t;
    using PA         = pybind11::array_t<TF>;
    using IA         = pybind11::array_t<TI>;

    TI    size_in_mem() const { return lx * ly * lz * sizeof( TF ); }
    PA    to_cpu     () const { PA res( { lz, ly, lx } ); cudaMemcpy( res.mutable_data(), data, size_in_mem(), cudaMemcpyDeviceToHost ); return res; }

    IA    get_shape  () const { IA res( { 3 } ); res.mutable_at( 0 ) = lz; res.mutable_at( 1 ) = ly; res.mutable_at( 2 ) = lx; return res; }
    void  set_shape  ( IA ns ) const {}

    __device__ __host__
    TF    operator() ( TI z, TI y, TI x ) const { return data[ z * lx * ly + y * lx + x ]; }

    __device__ __host__
    TF   &operator() ( TI z, TI y, TI x ) { return data[ z * lx * ly + y * lx + x ]; }

    TF   *data;
    TI    lz;
    TI    ly;
    TI    lx;
};
