#pragma once

#include <cmath>

namespace Internal {
    template<class T>
    constexpr T next_pow2_impl( T a, T trial ) {
        return a <= trial ? trial : next_pow2_impl( a, 2 * trial );
    }
}

template<class T,class U>
constexpr T ceil( T a, U m ) {
    // return m ? ( a >= 0 ? a + m - 1 : a ) / m * m : a;
    return m ? ( a + m - 1 ) / m * m : a;
}

template<class T,class U>
constexpr T div_up( T a, U m ) {
    return ( a + m - 1 ) / m;
}

template<class T>
constexpr T next_pow2( T a ) {
    return a ? Internal::next_pow2_impl( a, T( 1 ) ) : 0;
}
