#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <cmath>

#include "reco_rings.h"
#include "Stream.h"
//#include "Ad.h"

//// nsmake cpp_flag -march=native
//// nsmake cpp_flag -O3

namespace py = pybind11;
using TI = py::ssize_t;
using TF = float;


GpuVol py_reco_rings( GpuVol &rotations, TI px, TI py, TI pz, TI bx, TI ix ) {
    GpuVol res( px, py, pz );
    reco_rings( res, rotations, bx, ix );
    return res;
}


//py::array_t<TF> py_reconstruct( PyGpuVol &projections, py::array_t<TF> &rotations,
//    TF beg_x, TF inc_x, TI len_x,
//    TF beg_y, TF inc_y, TI len_y,
//    TF beg_z, TF inc_z, TI len_z,
//    int order
//) {

//    py::array_t<TF> res;
//    res.resize( { py::ssize_t( len_z ), py::ssize_t( len_y ), py::ssize_t( len_x ) } );

//    reconstruct(
//        res.mutable_data(), projections.data, rotations.data(),
//        len_x, len_y, len_z, beg_x, beg_y, beg_z, inc_x, inc_y, inc_z,
//        projections.len_x, projections.len_y, projections.len_z,
//        order
//    );

//    return res;
//}

//py::array_t<float> py_make_medi( py::array_t<float> &content, int length, int thickness/*, bool only_inside*/ ) {
//    py::array_t<float> res( {
//        content.shape( 0 ) - 2 * length,
//        content.shape( 1 ) - 2 * length,
//        content.shape( 2 ) - 2 * length
//    } );

//    make_medi( res.mutable_data(), content.data(),
//        content.shape( 2 ), content.shape( 1 ), content.shape( 0 ),
//        length, thickness
//    );

//    return res;
//}

//py::array_t<float> py_remo_ring( py::array_t<float> &reco, py::array_t<float> &rings ) {
//    py::array_t<float> res;
//    // res.resize( { projections.shape( 2 ), py::ssize_t( len_y ), projections.shape( 2 ) } );

//    // reco_ring( res.mutable_data(),
//    //      projections.shape( 2 ), projections.shape( 1 ), projections.shape( 2 ),
//    //      projections.shape( 2 ), projections.shape( 1 ), projections.shape( 0 ),
//    //      rotations.data(), beg_y, len_y, pos_x, pos_y
//    // );

//    return res;
//}

//py::array_t<float> py_optimize_reco( py::array_t<float> &reco, py::array_t<float> &rings, float /*coeff*/ ) {
//    using std::isfinite;
//    using std::isnan;

//    // init Vd img
//    constexpr int n = 65;
//    using VF = Ad<float,n,2>;

//    float mean = 0, nb_points = 0;
//    std::vector<VF> img( reco.shape( 0 ) * reco.shape( 1 ) );
//    for ( pybind11::ssize_t z = 0, c = 0; z < reco.shape( 0 ); ++z ) {
//        for ( pybind11::ssize_t x = 0; x < reco.shape( 1 ); ++x, ++c ) {
//            if ( isfinite( reco.at( z, x ) ) ) {
//                mean += reco.at( z, x );
//                nb_points += 1;
//            }

//            img[ c ] = reco.at( z, x );
//            for ( pybind11::ssize_t y = 0; y < rings.shape( 1 ); ++y )
//                if ( isfinite( rings.at( z, y, x ) ) )
//                    img[ c ].v[ y ] = rings.at( z, y, x );
//        }
//    }
//    mean /= nb_points;

//    // get |grad|^2
//    std::size_t nb_jobs = thread_pool.nb_threads();
//    std::vector<VF> res_v( nb_jobs, VF( 0 ) );
//    thread_pool.execute( nb_jobs, [&]( std::size_t num_job, int ) {
//        size_t beg_z = 1 + ( num_job + 0 ) * ( reco.shape( 0 ) - 1 ) / nb_jobs;
//        size_t end_z = 1 + ( num_job + 1 ) * ( reco.shape( 0 ) - 1 ) / nb_jobs;
//        for( size_t z = beg_z; z < end_z; ++z ) {
//            for( pybind11::ssize_t x = 1; x < reco.shape( 1 ); ++x ) {
//                VF vc = img[ ( z + 0 ) * reco.shape( 1 ) + ( x + 0 ) ];
//                VF gx = vc - img[ ( z + 0 ) * reco.shape( 1 ) + ( x - 1 ) ];
//                VF gz = vc - img[ ( z - 1 ) * reco.shape( 1 ) + ( x + 0 ) ];
//                if ( isfinite( vc.x ) && isfinite( gx.x ) && isfinite( gz.x ) ) {
//                    res_v[ num_job ] += ( vc - mean ) * ( vc - mean );
//                    res_v[ num_job ] += gx * gx + gz * gz;
//                }
//            }
//        }
//    } );
    
//    VF res = 0;
//    for( VF v : res_v )
//        res += v;

//    for( size_t x = 0; x < n; ++x )
//        res.m.coeffRef( x, x ) += 1e7;

//    Eigen::LLT<Eigen::MatrixXf> C;
//    C.compute( res.m );
//    Eigen::VectorXf D = C.solve( res.v );

//    // res *= coeff;
//    for ( pybind11::ssize_t z = 0, c = 0; z < reco.shape( 0 ); ++z )
//        for ( pybind11::ssize_t x = 0; x < reco.shape( 1 ); ++x, ++c )
//            for ( pybind11::ssize_t y = 0; y < rings.shape( 1 ); ++y )
//                img[ c ].x -= D[ y ] * img[ c ].v[ y ];

//    for ( pybind11::ssize_t z = 0, c = 0; z < reco.shape( 0 ); ++z )
//        for ( pybind11::ssize_t x = 0; x < reco.shape( 1 ); ++x, ++c )
//            reco.mutable_at( z, x ) = img[ c ].x;

//    return reco;
//}

GpuVol py_to_gpu( pybind11::array_t<TF> &a ) { return a; }

PYBIND11_MODULE( reconstruction_toolbox_gpu, m ) {
    m.doc() = "reconstruction toolbox";

    py::class_<GpuVol>( m, "GpuVol" )
     .def( py::init<pybind11::array_t<TF> &>() )
     .def( py::init<TI, TI, TI>() )
     .def( "__sizeof__"   , &GpuVol::size_in_mem )
     .def( "to_cpu"       , &GpuVol::to_cpu );

    m.def( "to_gpu"       , &py_to_gpu );

    m.def( "reco_rings"   , &py_reco_rings );
    //    m.def( "optimize_reco", &py_optimize_reco );
    //    m.def( "reconstruct"  , &py_reconstruct   );
    //    m.def( "make_medi"    , &py_make_medi     );
    //    m.def( "remo_ring"    , &py_remo_ring     );
}

