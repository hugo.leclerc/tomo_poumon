#include "Ad.h"

template<class T,int n>
Ad<T,n,2>::Ad( ScalarType x, const VectorType &v, const MatrixType &m ) : x( x ), v( v ), m( m ) {
}

template<class T,int n>
Ad<T,n,2>::Ad( ScalarType x ) : x( x ), m( n, n ) {
    for( size_t i = 0; i < n; ++i )
        v[ i ] = 0;
    for( size_t i = 0; i < n; ++i )
        for( size_t j = 0; j < n; ++j )
            m.coeffRef( i, j ) = 0;
}

template<class T,int n>
Ad<T,n,2>::Ad( const Ad &that ) : x( that.x ), v( that.v ), m( that.m ) {
}

template<class T,int n>
void Ad<T,n,2>::write_to_stream( std::ostream &os ) const {
    os << '(' << x << "+v[";
    for( size_t i = 0; i < n; ++i )
        os << ( i ? "," : "" ) << v[ i ];
    os << "]+m[";
    for( size_t i = 0; i < n; ++i ) {
        os << ( i ? "," : "" ) << "[";
        for( size_t j = 0; j < n; ++j )
            os << ( j ? "," : "" ) << m.coeff( i, j );
        os << "]";
    }
    os << "])";
}

template<class T,int n>
void Ad<T,n,2>::operator+=( const Ad &b ) {
    x += b.x;
    v += b.v;
    m += b.m;
}

template<class T,int n>
void Ad<T,n,2>::operator+=( T b ) {
    x += b;
}

template<class T,int n>
void Ad<T,n,2>::operator-=( const Ad &b ) {
    x -= b.x;
    v -= b.v;
    m -= b.m;
}

template<class T,int n>
void Ad<T,n,2>::operator-=( T b ) {
    x -= b;
}

template<class T,int n>
void Ad<T,n,2>::operator*=( T b ) {
    x *= b;
    v *= b;
    m *= b;
}

template<class T,int n>
void Ad<T,n,2>::operator/=( T b ) {
    x /= b;
    v /= b;
    m /= b;
}

template<class T,int n>
Ad<T,n,2> Ad<T,n,2>::operator-() const {
    return { - x, - v, - m };
}

template<class T,int n>
Ad<T,n,2> operator+( const Ad<T,n,2> &a, const Ad<T,n,2> &b ) {
    return { a.x + b.x, a.v + b.v, a.m + b.m };
}

template<class T,int n>
Ad<T,n,2> operator-( const Ad<T,n,2> &a, const Ad<T,n,2> &b ) {
    return { a.x - b.x, a.v - b.v, a.m - b.m };
}

template<class T,int n>
Ad<T,n,2> operator-( T a, const Ad<T,n,2> &b ) {
    return { a - b.x, - b.v, - b.m };
}

template<class T,int n>
Ad<T,n,2> operator-( const Ad<T,n,2> &a, T b ) {
    return { a.x - b, a.v, a.m };
}

template<class T,int n>
Ad<T,n,2> operator*( T a, const Ad<T,n,2> &b ) {
    return { a * b.x, a * b.v, a * b.m };
}

template<class T,int n>
Ad<T,n,2> operator*( const Ad<T,n,2> &a, T b ) {
    return { a.x * b, a.v * b, a.m * b };
}

template<class T,int n>
Ad<T,n,2> operator*( const Ad<T,n,2> &a, const Ad<T,n,2> &b ) {
    Ad<T,n,2> r;
    r.x = a.x * b.x;
    r.v = a.x * b.v + b.x * a.v;
    r.m = a.x * b.m + b.x * a.m + a.v * b.v.transpose() + b.v * a.v.transpose();
    return r;
}


//template<class T,int n>
//Ad<T,n,2> operator/( const Ad<T,n,2> &a, const Ad<T,n,2> &b ) {
//    Ad<T,n,2> r;
//    r.x = a.x / b.x;
//    for( size_t i = 1; i < n; ++i )
//        r.values[ i ] = a.values[ i ] / b.x - a.x * b.values[ i ] / ( b.x * b.x );
//    return r;
//}

template<class T,int n>
Ad<T,n,2> abs( const Ad<T,n,2> &a ) {
    return a.x >= 0 ? a : - a;
}

template<class T,int n>
Ad<T,n,2> min( const Ad<T,n,2> &a, const Ad<T,n,2> &b ) {
    return a.x <= b.x ? a : b;
}

template<class T,int n>
Ad<T,n,2> max( const Ad<T,n,2> &a, const Ad<T,n,2> &b ) {
    return a.x >= b.x ? a : b;
}

template<class T,int n>
Ad<T,n,2> pow( const Ad<T,n,2> &a, T e ) {
    using std::pow;
    Ad<T,n,2> r;
    r.x = pow( a.x, e );
    for( size_t i = 1; i < n; ++i )
        r.values[ i ] = e * a.values[ i ] * pow( a.x, e - 1 );
    return r;
}
