#pragma once

#include <eigen3/Eigen/Dense>
#include <iostream>
#include <cmath>

template<class T,int nb_variables,int order>
struct Ad;

/**
*/
template<class T,int n>
struct Ad<T,n,2> {
    using      ScalarType       = T;
    using      VectorType       = Eigen::Matrix<T,n,1>;
    using      MatrixType       = Eigen::Matrix<T,n,n>;

    enum {     nb_variables     = n };

    /**/       Ad               ( ScalarType x, const VectorType &v, const MatrixType &m );
    /**/       Ad               ( ScalarType x = 0 );
    /**/       Ad               ( const Ad &x );

    void       write_to_stream  ( std::ostream &os ) const;

    void       operator+=       ( const Ad &b );
    void       operator+=       ( T b );

    void       operator-=       ( const Ad &b );
    void       operator-=       ( T b );

    void       operator*=       ( T b );

    void       operator/=       ( T b );

    Ad         operator-        () const;

    ScalarType x;
    VectorType v;
    MatrixType m;
};

template<class T,int n> Ad<T,n,2> operator+( const Ad<T,n,2> &a, const Ad<T,n,2> &b );

template<class T,int n> Ad<T,n,2> operator-( const Ad<T,n,2> &a, const Ad<T,n,2> &b );
template<class T,int n> Ad<T,n,2> operator-( const Ad<T,n,2> &a, T b );
template<class T,int n> Ad<T,n,2> operator-( T a, const Ad<T,n,2> &b );

template<class T,int n> Ad<T,n,2> operator*( const Ad<T,n,2> &a, const Ad<T,n,2> &b );
template<class T,int n> Ad<T,n,2> operator*( const Ad<T,n,2> &a, T b );
template<class T,int n> Ad<T,n,2> operator*( T a, const Ad<T,n,2> &b );

template<class T,int n> Ad<T,n,2> operator/( const Ad<T,n,2> &a, const Ad<T,n,2> &b );

template<class T,int n> Ad<T,n,2> abs      ( const Ad<T,n,2> &a );
template<class T,int n> Ad<T,n,2> min      ( const Ad<T,n,2> &a, const Ad<T,n,2> &b );
template<class T,int n> Ad<T,n,2> max      ( const Ad<T,n,2> &a, const Ad<T,n,2> &b );
template<class T,int n> Ad<T,n,2> pow      ( const Ad<T,n,2> &a, T e );

#include "Ad.tcc"
