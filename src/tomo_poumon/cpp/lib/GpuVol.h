#pragma once

#include "GpuVolRef.h"

/**
 * a 3D array in the GPU
 */
struct GpuVol : GpuVolRef {
    /**/   GpuVol( pybind11::array_t<TF> &cpu_data ) : GpuVol( cpu_data.shape( 0 ), cpu_data.shape( 1 ), cpu_data.shape( 2 ) ) { cudaMemcpy( data, cpu_data.data(), size_in_mem(), cudaMemcpyHostToDevice ); }
    /**/   GpuVol( TI lz, TI ly, TI lx ) : GpuVolRef{ nullptr, lz, ly, lx } { cudaMalloc( &data, size_in_mem() ); }
    /**/   GpuVol( GpuVol &&that ) : GpuVolRef{ that.data, that.lz, that.ly, that.lx } { that.data = nullptr; }
    /**/  ~GpuVol() { if ( data ) cudaFree( data ); }

    GpuVol copy  () const { GpuVol res( lz, ly, lx ); cudaMemcpy( res.data, data, size_in_mem(), cudaMemcpyDeviceToDevice ); return res; }
};
