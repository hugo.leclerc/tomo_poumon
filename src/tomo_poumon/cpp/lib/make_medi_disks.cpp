#include <algorithm>
#include "Point3.h"
#include "Stream.h"
#include <fstream>
#include <sstream>
#include <set>

//// nsmake cpp_flag -O3

int main( int argc, char **argv ) {
    using Pt = Point3<double>;

    // read points
    std::ifstream f( argv[ 1 ] );
    std::string line;
    while( std::getline( f, line ) )
        if ( line.size() >= 6 && line.substr( 0, 6 ) == "$Nodes" )
            break;
    std::getline( f, line );
    std::vector<Pt> points;
    while( std::getline( f, line ) ) {
        if ( line.size() && line[ 0 ] == '$' )
            break;
        std::istringstream ss( line );
        int n; double x, y, z;
        ss >> n >> x >> y >> z;
        if ( z >= 0 )
            points.push_back( { x, y, z } );
    }

    // make disks
    std::vector<int> lengths{ 0, 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> thicknesses{ 0/*, 1, 2, 3, 4, 5, 6, 7, 8*/ };
    for( int t : thicknesses ) {
        for( int l : lengths ) {
            std::set<std::vector<Pt>> disks;

            for( Pt p : points ) {
                if ( norm_2( p ) < 1 - 1e-3 || norm_2( p ) > 1 + 1e-3 )
                    continue;
                Pt x = ortho_rand( p );
                Pt y = normalized( cross_prod( x, p ) );

                std::set<Pt> ps;
                for( double dz = - t; dz <= t; dz += 0.25 ) {
                    for( double dy = - l; dy <= l; dy += 0.25 ) {
                        for( double dx = - l; dx <= l; dx += 0.25 ) {
                            if ( dx * dx + dy * dy <= l * l + 1e-6 && dx * dx + dy * dy + dz * dz <= l * l * l + 1e-6 ) {
                                Pt np = dx * x + dy * y + dz * p;
                                double nx = int( np.x );
                                double ny = int( np.y );
                                double nz = int( np.z );

                                ps.insert( Pt{ nx, ny, nz } );
                            }
                        }
                    }
                }

                std::vector<Pt> disk;
                for( Pt n : ps )
                    disk.push_back( n );
                std::sort( disk.begin(), disk.end() );

                disks.insert( disk );
            }

            //
            std::cout << "static constexpr int medi_disks_len_" << l << "_" << t << " = " << disks.size() << ";\n";
            std::cout << "static constexpr int medi_disks_off_" << l << "_" << t << "[] = {\n";
            std::cout << "    0,\n";
            int a = 0;
            for( auto disk : disks ) {
                a += 3 * disk.size();
                std::cout << "    " << a << ",\n";
            }
            std::cout << "};\n";
            std::cout << "static constexpr std::int8_t medi_disks_pts_" << l << "_" << t << "[] = {\n";
            for( auto disk : disks ) {
                std::cout << "    // \n";
                for( Pt p : disk )
                    std::cout << "    " << p.x << ", " << p.y << ", " << p.z << ",\n";
            }
            std::cout << "};\n";
        }
    }

    //
    std::cout << "struct MediDisk {\n";
    std::cout << "    int                nb_disks;\n";
    std::cout << "    const int         *offsets;\n";
    std::cout << "    const std::int8_t *points;\n";
    std::cout << "};\n";

    //
    std::cout << "static MediDisk medi_disk( int l, int t ) {\n";
    std::cout << "    switch( 10 * l + t ) {\n";
    for( int t : thicknesses )
        for( int l : lengths )
            std::cout << "    case " << 10 * l + t << ": return { medi_disks_len_" << l << "_" << t << ", medi_disks_off_" << l << "_" << t << ", medi_disks_pts_" << l << "_" << t << " };\n";
    std::cout << "    default: return { -1, nullptr, nullptr };\n";
    std::cout << "    }\n";
    std::cout << "}\n";


    return 0;
}
