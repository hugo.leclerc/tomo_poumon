import numpy as np
import gmsh

gmsh.initialize()
gmsh.option.setNumber( "General.Terminal", 1 )

def make_geom( nb_angles = 16, radii = [ 10, 100, 1000 ] ):
    model = gmsh.model
    factory = model.geo

    for r in radii:
        for t in range( nb_angles ):
            factory.addPoint( r * np.cos( 2 * np.pi * t / nb_angles ), r * np.sin( 2 * np.pi * t / nb_angles ), 0, ( r / radii[ 0 ] ) ** 2 )

    for n in range( len( radii ) ):
        o = 1 + n * nb_angles
        for t in range( nb_angles ):
            factory.addLine( o + t, o + ( t + 1 ) % nb_angles )

    for n in range( len( radii ) ):
        cl = []
        o = 1 + n * nb_angles
        for t in range( nb_angles ):
            cl.append( o + t )
        factory.addCurveLoop( cl )

    factory.addPlaneSurface( [ 1 ] )
    factory.addPlaneSurface( [ 1, -2 ] )
    factory.addPlaneSurface( [ 2, -3 ] )

    factory.synchronize()

    gmsh.model.mesh.generate( 2 )
    gmsh.write( "src/tomo_poumon/cpp/lib/exp_disc.stl" )

    gmsh.fltk.run()
    gmsh.finalize()

make_geom()
