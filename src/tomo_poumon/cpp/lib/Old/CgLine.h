

            // //
            // auto test_z_inter = [&]( P3 A, P3 B, P3 C, TF &min_z, TF &max_z ) {
            //     TF m0x = B.x - A.x, m1x = C.x - A.x;
            //     TF m0y = B.y - A.y, m1y = C.y - A.y;
            //     if ( TF det = m0x * m1y - m1x * m0y ) {
            //         TF al = ( ( x - A.x ) * m1y - m1x * ( y - A.y ) ) / det;
            //         TF be = ( m0x * ( y - A.y ) - ( x - A.x ) * m0y ) / det;
            //         const TF e = - 1e-6;
            //         if ( al > e && be > e && 1 - al - be > e ) {
            //             TF z = A.z + ( B.z - A.z ) * al + ( C.z - A.z ) * be;
            //             min_z = min( min_z, z );
            //             max_z = max( max_z, z );
            //         }
            //     }
            // };

            // TF min_z = + std::numeric_limits<TF>::max();
            // TF max_z = - std::numeric_limits<TF>::max();
            // test_z_inter( p0, p1, p2, min_z, max_z );
            // test_z_inter( p0, p1, p3, min_z, max_z );
            // test_z_inter( p0, p2, p3, min_z, max_z );
            // test_z_inter( p1, p2, p3, min_z, max_z );

            // if ( min_z < max_z ) {
            //     // intersection points
            //     P3 i0( x, y, min_z );
            //     P3 i1( x, y, max_z );

            //     // ref coordinates
            //     auto orig_coord = [&]( TF x, TF y, TF z ) -> P3 {
            //         // metil scripts/hugo/solve3.met
            //         TF R0 = z - p0.z; TF R1 = p2.x - p0.x; TF R2 = p3.y - p0.y; TF R3 = R1*R2; TF R4 = p3.x - p0.x; TF R5 = p2.y - p0.y; 
            //         TF R6 = R4*R5; TF R7 = R3-R6; TF R8 = R0*R7; TF R9 = x - p0.x; TF R10 = p3.z - p0.z; TF R11 = R5*R10; 
            //         TF R12 = p2.z - p0.z; TF R13 = R2*R12; TF R14 = R11-R13; TF R15 = R9*R14; TF R16 = R8+R15; TF R17 = y - p0.y; 
            //         TF R18 = R1*R10; TF R19 = R4*R12; TF R20 = R18-R19; TF R21 = R17*R20; TF R22 = R16-R21; TF R23 = p1.x - p0.x; 
            //         TF R24 = R23*R14; TF R25 = p1.z - p0.z; TF R26 = R25*R7; TF R27 = R24+R26; TF R28 = p1.y - p0.y; TF R29 = R28*R20; 
            //         TF R30 = R27-R29; TF R31 = 1/R30; TF R32 = R22*R31; TF al = R32; TF R33 = R23*R10; TF R34 = R4*R25; 
            //         TF R35 = R33-R34; TF R36 = R17*R35; TF R37 = R28*R10; TF R38 = R2*R25; TF R39 = R37-R38; TF R40 = R9*R39; 
            //         TF R41 = R23*R2; TF R42 = R4*R28; TF R43 = R41-R42; TF R44 = R0*R43; TF R45 = R40+R44; TF R46 = R36-R45; 
            //         TF R47 = R46*R31; TF be = R47; TF R48 = R23*R5; TF R49 = R1*R28; TF R50 = R48-R49; TF R51 = R0*R50; 
            //         TF R52 = R28*R12; TF R53 = R5*R25; TF R54 = R52-R53; TF R55 = R9*R54; TF R56 = R51+R55; TF R57 = R23*R12; 
            //         TF R58 = R1*R25; TF R59 = R57-R58; TF R60 = R17*R59; TF R61 = R56-R60; TF R62 = R61*R31; TF ga = R62;
            //         return { al, be, ga };
            //     };
            //     P3 e0 = orig_coord( x, y, min_z );
            //     P3 e1 = orig_coord( x, y, max_z );

            //     e0.x = min( TF( 1 ), max( TF( 0 ), e0.x ) );
            //     e0.y = min( TF( 1 ), max( TF( 0 ), e0.y ) );
            //     e0.z = min( TF( 1 ), max( TF( 0 ), e0.z ) );

            //     // coordinate in the mesh for angle = 0
            //     P3 r0 = o0 + e0.x * ( o1 - o0 ) + e0.y * ( o2 - o0 ) + e0.z * ( o3 - o0 ) - reco_orig;
            //     P3 r1 = o0 + e1.x * ( o1 - o0 ) + e1.y * ( o2 - o0 ) + e1.z * ( o3 - o0 ) - reco_orig;

            //     TF cs = 0;
            //     P3 cr = r0;
            //     TF le = norm_2( r1 - r0 );
            //     while ( true ) {
            //         // find the next rs (the next intersection ray/grid)
            //         auto test = [&]( TF &best_ns, TF cs, TF cr, TF r0, TF r1 ) {
            //             if ( TF div = r1 - r0 ) {
            //                 TF ns = ( TI( cr ) + ( r1 > r0 ? 1 : -1 ) - r0 ) / div;
            //                 if ( ns > cs )
            //                     best_ns = min( best_ns, ns );
            //             }
            //         };

            //         TF best_ns = std::numeric_limits<TF>::max();
            //         test( best_ns, cs, cr.x, r0.x, r1.x );
            //         test( best_ns, cs, cr.y, r0.y, r1.y );
            //         test( best_ns, cs, cr.z, r0.z, r1.z );
            //         if ( best_ns >= 1 ) {
            //             f( x, y, cr.x, cr.y, cr.z, le * ( 1 - cs ) );
            //             break;
            //         }
                        
            //         f( x, y, cr.x, cr.y, cr.z, le * ( best_ns - cs ) );
            //         cr = r0 + best_ns * ( r1 - r0 );
            //         cs = best_ns;
            //     }
            // }
