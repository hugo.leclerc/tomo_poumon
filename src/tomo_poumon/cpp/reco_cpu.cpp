#include "lib/ThreadPool.h"
#include "lib/Stream.h"
#include <iostream>
#include <fstream>
#include <cmath>

#include <pybind11/numpy.h>

#include "lib/(make_medi_disks.cpp sphere.msh).h"

//// nsmake cpp_flag -march=native
//// nsmake cpp_flag -O3

using TF = float;
using TI = ssize_t;
using AF = pybind11::array_t<TF>;
using AI = pybind11::array_t<TI>;

AF part_rotation( AF &corner_0, AF &corner_7, AF &rot_0, AF &rot_1, int nb_angles, double x_slip, double y_slip, double z_slip ) {
    using std::sin;
    using std::cos;

    AF res( { TI( nb_angles ), TI( 8 ), TI( 3 ) } );
    for( int num_angle = 0; num_angle < nb_angles; ++num_angle ) {
        // rotation axis
        float p1 = rot_1.at( 0 ) - rot_0.at( 0 ), p2 = rot_1.at( 1 ) - rot_0.at( 1 ), p3 = rot_1.at( 2 ) - rot_0.at( 2 );
        float n = sqrt( p1 * p2 + p2 * p2 + p3 * p3 );
        p1 /= n; p2 /= n; p3 /= n;

        // rotation matrix
        float pos_in_cycle = num_angle / ( nb_angles - 1.0 );
        float theta = M_PI * pos_in_cycle;
        float co = cos( theta ), si = sin( theta );
        float rot_m[] = {
            co + ( 1 - co ) * p1 * p1     , ( 1 - co ) * p1 * p2 - si * p3, ( 1 - co ) * p1 * p3 + si * p2,
            ( 1 - co ) * p2 * p1 + si * p3, co + ( 1 - co ) * p2 * p2     , ( 1 - co ) * p2 * p3 - si * p1,
            ( 1 - co ) * p3 * p1 - si * p2, ( 1 - co ) * p3 * p2 + si * p1, co + ( 1 - co ) * p3 * p3     ,
        };

        // values
        for( int i = 0; i < 8; ++i ) {
            // point to be transformed
            float base_point[ 3 ];

            for( int d = 0; d < 3; ++d )
                base_point[ d ] = i & ( 1 << d ) ? corner_7.at( d ) : corner_0.at( d );

            base_point[ 0 ] += pos_in_cycle * z_slip;
            base_point[ 1 ] += pos_in_cycle * y_slip;
            base_point[ 2 ] += pos_in_cycle * x_slip;

            base_point[ 0 ] -= rot_0.at( 0 );
            base_point[ 1 ] -= rot_0.at( 1 );
            base_point[ 2 ] -= rot_0.at( 2 );

            // transform and store
            for( int d = 0; d < 3; ++d )
                res.mutable_at( num_angle, i, d ) = rot_0.at( d ) +
                        rot_m[ 3 * d + 0 ] * base_point[ 0 ] +
                        rot_m[ 3 * d + 1 ] * base_point[ 1 ] +
                        rot_m[ 3 * d + 2 ] * base_point[ 2 ] ;
        }
    }
    return res;
}

template<class Proj,int take_iy>
AF reco_gen( const Proj &projections, AF &rotations, TI nx, TI ny, TI nz, N<take_iy> ) {
    TI na = rotations.shape( 0 );
    AF reco( { nz, ny, nx } );

    std::size_t nb_jobs = thread_pool.nb_threads();
    thread_pool.execute( nb_jobs, [&]( std::size_t num_job, int ) {
        TI bz = ( num_job + 0 ) * nz / nb_jobs;
        TI ez = ( num_job + 1 ) * nz / nb_jobs;

        for( TI iz = bz; iz < ez; iz++ ) {
            for( TI iy = 0; iy < ny; ++iy ) {
                for( TI ix = 0; ix < nx; ++ix ) {
                    TF dx = TF( ix ) / nx;
                    TF dy = TF( iy ) / ny;
                    TF dz = TF( iz ) / nz;

                    TF res = 0.0;
                    for( TF pa = 0; pa < na - 1; pa += 0.5 ) {
                        TF hp[ 3 ] = { 0, 0, 0 };
                        auto add_hp = [&]( TI a, TF c ) {
                            const TF *bhp = rotations.mutable_data() + 3 * 8 * a;

                            for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 1.0f - dx ) * ( 1.0f - dy ) * ( 1.0f - dz ) * *( bhp++ );
                            for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 1.0f - dx ) * ( 1.0f - dy ) * ( 0.0f + dz ) * *( bhp++ );
                            for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 1.0f - dx ) * ( 0.0f + dy ) * ( 1.0f - dz ) * *( bhp++ );
                            for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 1.0f - dx ) * ( 0.0f + dy ) * ( 0.0f + dz ) * *( bhp++ );
                            for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 0.0f + dx ) * ( 1.0f - dy ) * ( 1.0f - dz ) * *( bhp++ );
                            for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 0.0f + dx ) * ( 1.0f - dy ) * ( 0.0f + dz ) * *( bhp++ );
                            for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 0.0f + dx ) * ( 0.0f + dy ) * ( 1.0f - dz ) * *( bhp++ );
                            for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 0.0f + dx ) * ( 0.0f + dy ) * ( 0.0f + dz ) * *( bhp++ );
                        };

                        TI ia = pa; TF fa = pa - ia;
                        add_hp( ia + 0, 1 - fa );
                        add_hp( ia + 1, 0 + fa );

                        if ( take_iy )
                            hp[ 1 ] = iy;

                        TI px = floor( hp[ 2 ] ), py = floor( hp[ 1 ] );
                        TF fx = hp[ 2 ] - TF( px );

                        TF v0 = projections( pa, py, px + 0 );
                        TF v1 = projections( pa, py, px + 1 );
                        res += ( TF( 1 ) - fx ) * v0;
                        res += ( TF( 0 ) + fx ) * v1;
                    }

                    reco.mutable_at( iz, iy, ix ) = res;
                }
            }
        }
    } );

    return reco;
}

AF ring_traces( AF &rotations, TI beg_pix_x, TI end_pix_y, TF step_size ) {
    auto lr = [&]( int n ) {
        TF z = rotations.at( 0, n, 0 ) - rotations.at( 0, 0, 0 );
        TF y = rotations.at( 0, n, 1 ) - rotations.at( 0, 0, 1 );
        TF x = rotations.at( 0, n, 2 ) - rotations.at( 0, 0, 2 );
        return sqrt( x * x + y * y + z * z );
    };

    const TI nz = lr( 1 ) / step_size;
    const TI ny = end_pix_y - beg_pix_x;
    const TI nx = lr( 4 ) / step_size;

    struct Proj {
        TF operator()( TF /*a*/, TF y, TF x ) const { return x == beg_pix_x + y; }
        TI beg_pix_x;
    };

    Proj proj{ beg_pix_x };
    return reco_gen( proj, rotations, nx, ny, nz, N<1>() );
}

AF reconstruction( AF &projections, AF &rotations, TF step_size, int interp_order = 1 ) {
    auto lr = [&]( int n ) {
        TF z = rotations.at( 0, n, 0 ) - rotations.at( 0, 0, 0 );
        TF y = rotations.at( 0, n, 1 ) - rotations.at( 0, 0, 1 );
        TF x = rotations.at( 0, n, 2 ) - rotations.at( 0, 0, 2 );
        return sqrt( x * x + y * y + z * z );
    };

    const TI nz = lr( 1 ) / step_size;
    const TI ny = lr( 2 ) / step_size;
    const TI nx = lr( 4 ) / step_size;

    struct Proj {
        TF operator()( TF a, TF y, TF x ) const {
            TI ia = a, iy = y, ix = x;
            if ( iy < 0 || ix < 0 || iy + 1 >= projections.shape( 1 ) || ix + 1 >= projections.shape( 2 ) )
                return nan( "" );
            TF fa = a - ia, fy = y - iy, fx = x - ix;

            TF v000 = projections.at( ia + 0, iy + 0, ix + 0 );
            TF v100 = projections.at( ia + 1, iy + 0, ix + 0 );
            TF v010 = projections.at( ia + 0, iy + 1, ix + 0 );
            TF v110 = projections.at( ia + 1, iy + 1, ix + 0 );
            TF v001 = projections.at( ia + 0, iy + 0, ix + 1 );
            TF v101 = projections.at( ia + 1, iy + 0, ix + 1 );
            TF v011 = projections.at( ia + 0, iy + 1, ix + 1 );
            TF v111 = projections.at( ia + 1, iy + 1, ix + 1 );

            TF res = 0;
            res += ( 1.0f - fa ) * ( 1.0f - fy ) * ( 1.0f - fx ) * v000;
            res += ( 0.0f + fa ) * ( 1.0f - fy ) * ( 1.0f - fx ) * v100;
            res += ( 1.0f - fa ) * ( 0.0f + fy ) * ( 1.0f - fx ) * v010;
            res += ( 0.0f + fa ) * ( 0.0f + fy ) * ( 1.0f - fx ) * v110;
            res += ( 1.0f - fa ) * ( 1.0f - fy ) * ( 0.0f + fx ) * v001;
            res += ( 0.0f + fa ) * ( 1.0f - fy ) * ( 0.0f + fx ) * v101;
            res += ( 1.0f - fa ) * ( 0.0f + fy ) * ( 0.0f + fx ) * v011;
            res += ( 0.0f + fa ) * ( 0.0f + fy ) * ( 0.0f + fx ) * v111;
            return res;
        }
        AF &projections;

        // #define I0_3( F ) 1.0f / 6 * ( 0.0f - F ) * ( F - 1.0f ) * ( F - 2.0f )
        // #define I1_3( F ) 1.0f / 2 * ( F + 1.0f ) * ( F - 1.0f ) * ( F - 2.0f )
        // #define I2_3( F ) 1.0f / 2 * ( F + 1.0f ) * ( 0.0f - F ) * ( F - 2.0f )
        // #define I3_3( F ) 1.0f / 6 * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )
        // #define I0_5( F ) 1.0f / ( -120 ) *                ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
        // #define I1_5( F ) 1.0f / (   24 ) * ( F + 2.0f ) *                ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
        // #define I2_5( F ) 1.0f / (  -12 ) * ( F + 2.0f ) * ( F + 1.0f ) *                ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
        // #define I3_5( F ) 1.0f / (   12 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) *                ( F - 2.0f ) * ( F - 3.0f )
        // #define I4_5( F ) 1.0f / (  -24 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )                * ( F - 3.0f )
        // #define I5_5( F ) 1.0f / (  120 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f )
        // #define I0_7( F ) 1.0f / ( -5040 ) *                ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
        // #define I1_7( F ) 1.0f / (   720 ) * ( F + 3.0f ) *                ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
        // #define I2_7( F ) 1.0f / (  -240 ) * ( F + 3.0f ) * ( F + 2.0f ) *                ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
        // #define I3_7( F ) 1.0f / (   144 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) *                ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
        // #define I4_7( F ) 1.0f / (  -144 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) *                ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
        // #define I5_7( F ) 1.0f / (   240 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )                * ( F - 3.0f ) * ( F - 4.0f )
        // #define I6_7( F ) 1.0f / (  -720 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) *                ( F - 4.0f )
        // #define I7_7( F ) 1.0f / (  5040 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
    };

    Proj proj{ projections };
    return reco_gen( proj, rotations, nx, ny, nz, N<0>() );
}

AF binarisation_max_disc( AF &reco, TF threshold, int radius ) {
    MediDisk md = medi_disk( std::max( 0, std::min( 8, radius ) ), 0 );
    if ( md.nb_disks < 0 ) {
        std::cerr << "wrong radius (medi_disk has not been compiled for this radius)" << std::endl;
        return reco;
    }

    if ( reco.shape( 0 ) - 2 * radius <= 0 || reco.shape( 1 ) - 2 * radius <= 0 || reco.shape( 2 ) - 2 * radius <= 0 ) {
        std::cerr << "volume for binarization is not large enough";
        return reco;
    }

    AF res( { reco.shape( 0 ) - 2 * radius, reco.shape( 1 ) - 2 * radius, reco.shape( 2 ) - 2 * radius } );
    for( TI iz = 0; iz < res.shape( 0 ); ++iz ) {
        for( TI iy = 0; iy < res.shape( 1 ); ++iy ) {
            for( TI ix = 0; ix < res.shape( 2 ); ++ix ) {
                bool val = false;
                for( int num_disk = 0; num_disk < md.nb_disks; ++num_disk ) {
                    int nb_points = md.offsets[ num_disk + 1 ] - md.offsets[ num_disk + 0 ], nbw = 0;
                    for( int off_point = md.offsets[ num_disk + 0 ]; off_point < md.offsets[ num_disk + 1 ]; ++off_point )
                        nbw += reco.at( iz + radius + md.points[ off_point + 0 ], iy + radius + md.points[ off_point + 1 ], ix + radius + md.points[ off_point + 2 ] ) >= threshold;
                    if ( nbw >= nb_points / 2 ) {
                        val = true;
                        break;
                    }
                }

                res.mutable_at( iz, iy, ix ) = val;
            }
        }
    }

    return res;
}

static float median( std::vector<float> &v ) {
    std::size_t n = v.size() / 2;
    nth_element( v.begin(), v.begin() + n, v.end() );
    return v[ n ];
}

AF max_median_on_discs( AF &content, int length, int thickness ) {
    using std::round;
    using std::max;
    using std::min;
    std::cout << length << std::endl;

    if ( length <= 0 )
        return content;

    if ( content.shape( 0 ) - 2 * length <= 0 || content.shape( 1 ) - 2 * length <= 0 || content.shape( 2 ) - 2 * length <= 0 ) {
        std::cerr << "volume for max_median_on_discs is not large enough";
        return content;
    }


    AF res( {
        content.shape( 0 ) - 2 * length,
        content.shape( 1 ) - 2 * length,
        content.shape( 2 ) - 2 * length
    } );

    //
    MediDisk md = medi_disk( min( length, 8 ), thickness );
    if ( md.nb_disks < 0 ) {
        std::cerr << "wrong length (medi_disk has not been compiled for this length)" << std::endl;
        return res;
    }

    std::size_t nb_jobs = thread_pool.nb_threads();
    thread_pool.execute( nb_jobs, [&]( std::size_t num_job, int ) {
        std::int64_t beg_z = ( num_job + 0 ) * res.shape( 0 ) / nb_jobs;
        std::int64_t end_z = ( num_job + 1 ) * res.shape( 0 ) / nb_jobs;

        std::vector<float> values;
        for( std::int64_t z = beg_z; z < end_z; z++ ) {
            if ( num_job + 1 == nb_jobs )
                std::cout << "  progress: " << z << " / " << end_z << std::endl;
            for( std::int64_t y = 0; y < res.shape( 1 ); y++ ) {
                for( std::int64_t x = 0; x < res.shape( 2 ); x++ ) {
                    float val = - std::numeric_limits<float>::max();

                    for( int num_disk = 0; num_disk < md.nb_disks; ++num_disk ) {
                        values.clear();
                        for( int off = md.offsets[ num_disk + 0 ]; off < md.offsets[ num_disk + 1 ]; off += 3 ) {
                            std::int64_t nx = x + length + md.points[ off + 0 ];
                            std::int64_t ny = y + length + md.points[ off + 1 ];
                            std::int64_t nz = z + length + md.points[ off + 2 ];
                            if ( nx < 0 || nx >= content.shape( 2 ) || ny < 0 || ny >= content.shape( 1 ) || nz < 0 || nz >= content.shape( 0 ) )
                                continue;
                            values.push_back( content.at( nz, ny, nx ) );
                        }
                        val = max( val, median( values ) );
                    }

                    res.mutable_at( z, y, x ) = val;
                }
            }
        }
    } );

    return res;
}

void py_save_xdmf( AF &content, const std::string &base_name ) {
    // bin
    std::string filename_bin = base_name + ".bin";
    std::ofstream bin( filename_bin.c_str() );
    bin.write( (char *)content.request().ptr, sizeof( float ) * content.size() );

    // xdmf
    std::string filename_xdmf = base_name + ".xdmf";
    std::ofstream xdmf( filename_xdmf.c_str() );

    xdmf << "<?xml version='1.0' ?>\n";
    xdmf << "<!DOCTYPE Xdmf SYSTEM 'Xdmf.dtd' []>\n";
    xdmf << "<Xdmf xmlns:xi='http://www.w3.org/2001/XInclude' Version='2.0'>\n";
    xdmf << "<Domain>\n";
    xdmf << "    <Topology name='topo' TopologyType='3DCoRectMesh'\n";
    xdmf << "        Dimensions='" << content.shape( 0 ) << " " << content.shape( 1 ) << " " << content.shape( 2 ) << "'>\n";
    xdmf << "    </Topology>\n";
    xdmf << "    <Geometry name='geo' Type='ORIGIN_DXDYDZ'>\n";
    xdmf << "        <!-- Origin -->\n";
    xdmf << "        <DataItem Format='XML' Dimensions='3'>\n";
    xdmf << "            0.0 0.0 0.0\n";
    xdmf << "        </DataItem>\n";
    xdmf << "        <!-- DxDyDz -->\n";
    xdmf << "        <DataItem Format='XML' Dimensions='3'>\n";
    xdmf << "            1.0 1.0 1.0\n";
    xdmf << "        </DataItem>\n";
    xdmf << "    </Geometry>\n";
    xdmf << "    <Grid Name='T1' GridType='Uniform'>\n";
    xdmf << "        <Topology Reference='/Xdmf/Domain/Topology[1]'/>\n";
    xdmf << "        <Geometry Reference='/Xdmf/Domain/Geometry[1]'/>\n";
    xdmf << "        <Attribute Name='value' Center='Node'>\n";
    xdmf << "            <DataItem Format='Binary'\n";
    xdmf << "             DataType='Float' Precision='4' Endian='Little'\n";
    xdmf << "             Dimensions='" << content.shape( 0 ) << " " << content.shape( 1 ) << " " << content.shape( 2 ) << "'>\n";
    xdmf << "                " << base_name.substr( base_name.rfind( "/" ) + 1 ) << ".bin\n";
    xdmf << "            </DataItem>\n";
    xdmf << "        </Attribute>\n";
    xdmf << "    </Grid>\n";
    xdmf << "</Domain>\n";
    xdmf << "</Xdmf>\n";
}

PYBIND11_MODULE( reco_cpu, m ) {
    m.doc() = "reconstruction tools";

    m.def( "binarisation_max_disc", &binarisation_max_disc );
    m.def( "max_median_on_discs"  , &max_median_on_discs   );
    m.def( "reconstruction"       , &reconstruction        );
    m.def( "part_rotation"        , &part_rotation         );
    m.def( "ring_traces"          , &ring_traces           );
    m.def( "save_xdmf"            , &py_save_xdmf          );
}

