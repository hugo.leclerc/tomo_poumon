#include "lib/ThreadPool.h"
#include <algorithm>
#include <iostream>
#include <cmath>

#include <pybind11/numpy.h>

//// nsmake cpp_flag -march=native
//// nsmake cpp_flag -O3

using TF = float;
using TI = ssize_t;
using AF = pybind11::array_t<TF>;

AF fuzzy_dist( AF &content, TF threshold, int nb_points, int max_dist ) {
    using std::sqrt;

    // sorted list of points
    struct Point {
        bool operator<( const Point &p ) const { return dist() < p.dist(); }
        TI   dist     () const { return x * x + y * y + z * z; }
        TI   x, y, z;
    };

    std::vector<Point> points;
    for( TI z = -100; z <= 100; z++ ) {
        for( TI y = -100; y <= 100; y++ ) {
            for( TI x = -100; x <= 100; x++ ) {
                TI d = x * x + y * y + z * z;
                if ( d > 100 * 100 )
                    continue;
                points.push_back( { x, y, z } );
            }
        }
    }
    std::sort( points.begin(), points.end() );

    std::vector<TI> offsets;
    for( TI o = 0, old_dist = -1; o < TI( points.size() ); ++o ) {
        TI new_dist = points[ o ].dist();
        if ( old_dist != new_dist ) {
            offsets.push_back( o );
            old_dist = new_dist;
        }
    }
    offsets.push_back( points.size() );

    //
    AF res( { content.shape( 0 ), content.shape( 1 ), content.shape( 2 ) } );

    std::size_t nb_jobs = thread_pool.nb_threads();
    thread_pool.execute( nb_jobs, [&]( std::size_t num_job, int ) {
        TI beg_z = ( num_job + 0 ) * content.shape( 0 ) / nb_jobs;
        TI end_z = ( num_job + 1 ) * content.shape( 0 ) / nb_jobs;

        for( TI z = beg_z; z < end_z; z++ ) {
            if ( num_job + 1 == nb_jobs )
                std::cout << "dist: " << z << " / " << end_z << std::endl;

            for( TI y = 0; y < content.shape( 1 ); y++ ) {
                std::cout << "dist: " << y << std::endl;
                for( TI x = 0; x < content.shape( 2 ); x++ ) {
                    float acc = 0, nacc = 0, mean_dist = 0;
                    for( TI no = 1; no < TI( offsets.size() ); ++no ) {
                        for( TI np = offsets[ no - 1 ]; np < offsets[ no ]; ++np ) {
                            Point p = points[ np ];
                            TI nx = x + p.x;
                            TI ny = y + p.y;
                            TI nz = z + p.z;
                            if ( nx < 0 || nx >= content.shape( 2 ) || ny < 0 || ny >= content.shape( 1 ) || nz < 0 || nz >= content.shape( 0 ) )
                                continue;

                            float ins = content.at( nz, ny, nx ) > threshold;
                            mean_dist += ins * sqrt( p.dist() );
                            nacc += ! ins;
                            acc += ins;
                        }
                        if ( acc >= nb_points )
                            break;
                    }

                    res.mutable_at( z, y, x ) = mean_dist / acc;
                }
            }
        }
    } );
    std::cout << "done" << std::endl;

    return res;
}


PYBIND11_MODULE( fuzzy_dist, m ) {
    m.doc() = "computation of distances";
    m.def( "fuzzy_dist", &fuzzy_dist );
}

