#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <fstream>
#include <cmath>

#include "lib/ThreadPool.h"
#include "lib/base_types.h"
// #include "lib/VolCpu.h"
#include "lib/Stream.h"

//// nsmake cpp_flag -march=native
//// nsmake cpp_flag -O3
//// nsmake cpp_flag -g3

namespace py = pybind11;

/*
  Prop: pour décrire les déplacements, on propose axe + angle de rotation + translation.

*/
struct Msh {
    using TI = int;
    using P2 = std::array<TF,2>;
    using Pt = std::array<TF,3>;
    using Tr = std::array<TI,3>;

    struct Movement {
        // global rotation
        Pt normal;
        Pt center;
        TF angle;
        // global translation
        Pt translation;
        // voxel description
    };

    Msh( const std::string &name ) {
        std::ifstream f( name );

        std::map<P2,TI> node_map;
        auto read_vertex = [&]( TI &n ) -> bool {
            std::string line, word;
            while( std::getline( f, line ) ) {
                std::istringstream is( line );
                is >> word;
                if ( is ) {
                    P2 p;
                    is >> p[ 0 ] >> p[ 1 ];
                    if ( is ) {
                        auto iter = node_map.find( p );
                        if ( iter == node_map.end() ) {
                            iter = node_map.insert( iter, { p, nodes.size() } );
                            nodes.push_back( p );
                        }
                        n = iter->second;
                        return true;
                    }
                }
            }
            return false;
        };

        TI a, b, c;
        while ( read_vertex( a ) && read_vertex( b ) && read_vertex( c ) )
            triangles.push_back( { a, b, c } );
    }

    std::vector<Tr> triangles;
    std::vector<P2> nodes;
};

PYBIND11_MODULE( reco_msh, m ) {
    m.doc() = "reco_msh";

    py::class_<Msh>( m, "Msh" )
        .def( py::init<const std::string &>())
        // .def( "setName", &Pet::setName)
        // .def( "getName", &Pet::getName)
        ;
    // m.def( "make_medi"     , &py_make_medi, py::arg( "input" ), py::arg( "length"    ), py::arg( "thickness" ) );
}

