//// nsmake cxx_name /usr/local/cuda/bin/nvcc
//// nsmake inc_path /usr/local/cuda/include
//// nsmake lib_path /usr/local/cuda/lib64
//// nsmake cpp_flag --x
//// nsmake cpp_flag cu
//// nsmake cpp_flag -O3

#include "lib/GpuVol.h"
#include "lib/Stream.h"
#include "lib/N.h"
#include <array>

#include "lib/(make_medi_disks.cpp sphere.msh).h"

using TF = GpuVolRef::TF;
using TI = GpuVolRef::TI;
using AF = pybind11::array_t<TF>;

__global__
void __part_rotation( GpuVolRef res,
                      TF corner_0_x, TF corner_0_y, TF corner_0_z,
                      TF corner_7_x, TF corner_7_y, TF corner_7_z,
                      TF rot_0_x, TF rot_0_y, TF rot_0_z,
                      TF rot_1_x, TF rot_1_y, TF rot_1_z,
                      int nb_angles, TF x_slip, TF y_slip, TF z_slip ) {
    using std::sin;
    using std::cos;

    for( int num_angle = 0; num_angle < nb_angles; ++num_angle ) {
        // rotation axis
        float p1 = rot_1_z - rot_0_z, p2 = rot_1_y - rot_0_y, p3 = rot_1_x - rot_0_x;
        float n = sqrt( p1 * p2 + p2 * p2 + p3 * p3 );
        p1 /= n; p2 /= n; p3 /= n;

        // rotation matrix
        float pos_in_cycle = num_angle / ( nb_angles - 1.0 );
        float theta = M_PI * pos_in_cycle;
        float co = cos( theta ), si = sin( theta );
        float rot_m[] = {
            co + ( 1 - co ) * p1 * p1     , ( 1 - co ) * p1 * p2 - si * p3, ( 1 - co ) * p1 * p3 + si * p2,
            ( 1 - co ) * p2 * p1 + si * p3, co + ( 1 - co ) * p2 * p2     , ( 1 - co ) * p2 * p3 - si * p1,
            ( 1 - co ) * p3 * p1 - si * p2, ( 1 - co ) * p3 * p2 + si * p1, co + ( 1 - co ) * p3 * p3     ,
        };

        // values
        for( int i = 0; i < 8; ++i ) {
            // point to be transformed
            float base_point[ 3 ];

            base_point[ 0 ] = i & ( 1 << 0 ) ? corner_7_z : corner_0_z;
            base_point[ 1 ] = i & ( 1 << 1 ) ? corner_7_y : corner_0_y;
            base_point[ 2 ] = i & ( 1 << 2 ) ? corner_7_x : corner_0_x;

            base_point[ 0 ] += pos_in_cycle * z_slip;
            base_point[ 1 ] += pos_in_cycle * y_slip;
            base_point[ 2 ] += pos_in_cycle * x_slip;

            base_point[ 0 ] -= rot_0_z;
            base_point[ 1 ] -= rot_0_y;
            base_point[ 2 ] -= rot_0_x;

            // transform and store
            res( num_angle, i, 0 ) = rot_0_z +
                    rot_m[ 3 * 0 + 0 ] * base_point[ 0 ] +
                    rot_m[ 3 * 0 + 1 ] * base_point[ 1 ] +
                    rot_m[ 3 * 0 + 2 ] * base_point[ 2 ] ;
            res( num_angle, i, 1 ) = rot_0_y +
                    rot_m[ 3 * 1 + 0 ] * base_point[ 0 ] +
                    rot_m[ 3 * 1 + 1 ] * base_point[ 1 ] +
                    rot_m[ 3 * 1 + 2 ] * base_point[ 2 ] ;
            res( num_angle, i, 2 ) = rot_0_x +
                    rot_m[ 3 * 2 + 0 ] * base_point[ 0 ] +
                    rot_m[ 3 * 2 + 1 ] * base_point[ 1 ] +
                    rot_m[ 3 * 2 + 2 ] * base_point[ 2 ] ;
        }
    }
}

GpuVol part_rotation( AF &corner_0, AF &corner_7, AF &rot_0, AF &rot_1, int nb_angles, TF x_slip, TF y_slip, TF z_slip ) {
    GpuVol res( nb_angles, 8, 3 );

    __part_rotation<<<1,1>>>( res,
        corner_0.at( 2 ), corner_0.at( 1 ), corner_0.at( 0 ),
        corner_7.at( 2 ), corner_7.at( 1 ), corner_7.at( 0 ),
        rot_0.at( 2 ), rot_0.at( 1 ), rot_0.at( 0 ),
        rot_1.at( 2 ), rot_1.at( 1 ), rot_1.at( 0 ),
        nb_angles, x_slip, y_slip, z_slip
    );

    return res;
}

template<class Proj,int take_iy> __global__
void __reco_gen( GpuVolRef reco, Proj projections, GpuVolRef rotations, TI nx, TI ny, TI nz, N<take_iy> ) {
    TI na = rotations.lz;

    const TI nb_points = nx * ny * nz;
    for( TI num_point = TI( blockIdx.x ) * blockDim.x + threadIdx.x; num_point < nb_points; num_point += gridDim.x * blockDim.x ) {
        TI ix = num_point % nx, ty = num_point / nx;
        TI iy = ty % ny;
        TI iz = ty / ny;

        TF dx = TF( ix ) / nx;
        TF dy = TF( iy ) / ny;
        TF dz = TF( iz ) / nz;

        TF res = 0.0;
        for( TF pa = 0; pa < na - 1; pa += 0.125 ) {
            TF hp[ 3 ] = { 0, 0, 0 };
            auto add_hp = [&]( TI a, TF c ) {
                const TF *bhp = rotations.data + 3 * 8 * a;

                for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 1.0f - dx ) * ( 1.0f - dy ) * ( 1.0f - dz ) * *( bhp++ );
                for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 1.0f - dx ) * ( 1.0f - dy ) * ( 0.0f + dz ) * *( bhp++ );
                for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 1.0f - dx ) * ( 0.0f + dy ) * ( 1.0f - dz ) * *( bhp++ );
                for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 1.0f - dx ) * ( 0.0f + dy ) * ( 0.0f + dz ) * *( bhp++ );
                for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 0.0f + dx ) * ( 1.0f - dy ) * ( 1.0f - dz ) * *( bhp++ );
                for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 0.0f + dx ) * ( 1.0f - dy ) * ( 0.0f + dz ) * *( bhp++ );
                for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 0.0f + dx ) * ( 0.0f + dy ) * ( 1.0f - dz ) * *( bhp++ );
                for( TI d = 0; d < 3; ++d ) hp[ d ] += c * ( 0.0f + dx ) * ( 0.0f + dy ) * ( 0.0f + dz ) * *( bhp++ );
            };

            TI ia = pa; TF fa = pa - ia;
            add_hp( ia + 0, 1 - fa );
            add_hp( ia + 1, 0 + fa );

            if ( take_iy )
                hp[ 1 ] = iy;

            TI px = floor( hp[ 2 ] ), py = floor( hp[ 1 ] );
            TF fx = hp[ 2 ] - TF( px );

            TF v0 = projections( pa, py, px + 0 );
            TF v1 = projections( pa, py, px + 1 );
            res += ( TF( 1 ) - fx ) * v0;
            res += ( TF( 0 ) + fx ) * v1;
        }

        reco.data[ num_point ] = res;
    }
}

std::array<TF,3> cube_lengths( GpuVolRef rotations ) {
    std::vector<TF> cpu_data( 5 * 3 );
    cudaMemcpy( cpu_data.data(), rotations.data, cpu_data.size() * sizeof( TF ), cudaMemcpyDeviceToHost );
    rotations.data = cpu_data.data();

    auto lr = [&]( int n ) {
        using std::sqrt;
        TF z = rotations( 0, n, 0 ) - rotations( 0, 0, 0 );
        TF y = rotations( 0, n, 1 ) - rotations( 0, 0, 1 );
        TF x = rotations( 0, n, 2 ) - rotations( 0, 0, 2 );
        return sqrt( x * x + y * y + z * z );
    };

    return { lr( 1 ), lr( 2 ), lr( 4 ) };
}

struct ProjTraces {
    __device__
    TF projections( TI y, TI x ) const {
        return x == beg_pix_x + y;
    }

    __device__
    TF operator()( TF /*a*/, TF y, TF x ) const {
        TI iy = y, ix = x;
        TF fy = y - iy, fx = x - ix;

        TF v00 = projections( iy + 0, ix + 0 );
        TF v10 = projections( iy + 1, ix + 0 );
        TF v01 = projections( iy + 0, ix + 1 );
        TF v11 = projections( iy + 1, ix + 1 );

        TF res = 0;
        res += ( 1.0f - fy ) * ( 1.0f - fx ) * v00;
        res += ( 0.0f + fy ) * ( 1.0f - fx ) * v10;
        res += ( 1.0f - fy ) * ( 0.0f + fx ) * v01;
        res += ( 0.0f + fy ) * ( 0.0f + fx ) * v11;
        return res;
    }

    TI beg_pix_x;
};

GpuVol ring_traces( GpuVol &rotations, TI beg_pix_x, TI end_pix_y, TF step_size ) {
    std::array<TF,3> cl = cube_lengths( rotations );
    const TI nz = cl[ 0 ] / step_size;
    const TI ny = end_pix_y - beg_pix_x;
    const TI nx = cl[ 2 ] / step_size;

    ProjTraces proj{ beg_pix_x };
    GpuVol reco( { nz, ny, nx } );
    __reco_gen<<<32,256>>>( reco, proj, rotations, nx, ny, nz, N<1>() );
    return reco;
}

struct ProjReco {
    __device__
    TF operator()( TF a, TF y, TF x ) const {
        TI ia = a, iy = y, ix = x;
        if ( iy < 0 || ix < 0 || iy + 1 >= projections.ly || ix + 1 >= projections.lx )
            return nan( "" );
        TF fa = a - ia, fy = y - iy, fx = x - ix;

        TF v000 = projections( ia + 0, iy + 0, ix + 0 );
        TF v100 = projections( ia + 1, iy + 0, ix + 0 );
        TF v010 = projections( ia + 0, iy + 1, ix + 0 );
        TF v110 = projections( ia + 1, iy + 1, ix + 0 );
        TF v001 = projections( ia + 0, iy + 0, ix + 1 );
        TF v101 = projections( ia + 1, iy + 0, ix + 1 );
        TF v011 = projections( ia + 0, iy + 1, ix + 1 );
        TF v111 = projections( ia + 1, iy + 1, ix + 1 );

        TF res = 0;
        res += ( 1.0f - fa ) * ( 1.0f - fy ) * ( 1.0f - fx ) * v000;
        res += ( 0.0f + fa ) * ( 1.0f - fy ) * ( 1.0f - fx ) * v100;
        res += ( 1.0f - fa ) * ( 0.0f + fy ) * ( 1.0f - fx ) * v010;
        res += ( 0.0f + fa ) * ( 0.0f + fy ) * ( 1.0f - fx ) * v110;
        res += ( 1.0f - fa ) * ( 1.0f - fy ) * ( 0.0f + fx ) * v001;
        res += ( 0.0f + fa ) * ( 1.0f - fy ) * ( 0.0f + fx ) * v101;
        res += ( 1.0f - fa ) * ( 0.0f + fy ) * ( 0.0f + fx ) * v011;
        res += ( 0.0f + fa ) * ( 0.0f + fy ) * ( 0.0f + fx ) * v111;
        return res;
    }
    GpuVolRef projections;

    // #define I0_3( F ) 1.0f / 6 * ( 0.0f - F ) * ( F - 1.0f ) * ( F - 2.0f )
    // #define I1_3( F ) 1.0f / 2 * ( F + 1.0f ) * ( F - 1.0f ) * ( F - 2.0f )
    // #define I2_3( F ) 1.0f / 2 * ( F + 1.0f ) * ( 0.0f - F ) * ( F - 2.0f )
    // #define I3_3( F ) 1.0f / 6 * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )
    // #define I0_5( F ) 1.0f / ( -120 ) *                ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
    // #define I1_5( F ) 1.0f / (   24 ) * ( F + 2.0f ) *                ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
    // #define I2_5( F ) 1.0f / (  -12 ) * ( F + 2.0f ) * ( F + 1.0f ) *                ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
    // #define I3_5( F ) 1.0f / (   12 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) *                ( F - 2.0f ) * ( F - 3.0f )
    // #define I4_5( F ) 1.0f / (  -24 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )                * ( F - 3.0f )
    // #define I5_5( F ) 1.0f / (  120 ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f )
    // #define I0_7( F ) 1.0f / ( -5040 ) *                ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
    // #define I1_7( F ) 1.0f / (   720 ) * ( F + 3.0f ) *                ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
    // #define I2_7( F ) 1.0f / (  -240 ) * ( F + 3.0f ) * ( F + 2.0f ) *                ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
    // #define I3_7( F ) 1.0f / (   144 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) *                ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
    // #define I4_7( F ) 1.0f / (  -144 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) *                ( F - 2.0f ) * ( F - 3.0f ) * ( F - 4.0f )
    // #define I5_7( F ) 1.0f / (   240 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f )                * ( F - 3.0f ) * ( F - 4.0f )
    // #define I6_7( F ) 1.0f / (  -720 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) *                ( F - 4.0f )
    // #define I7_7( F ) 1.0f / (  5040 ) * ( F + 3.0f ) * ( F + 2.0f ) * ( F + 1.0f ) * ( F - 0.0f ) * ( F - 1.0f ) * ( F - 2.0f ) * ( F - 3.0f )
};

GpuVol reconstruction( GpuVol &projections, GpuVol &rotations, TF step_size, int interp_order = 1 ) {
    std::array<TF,3> cl = cube_lengths( rotations );
    const TI nz = cl[ 0 ] / step_size;
    const TI ny = cl[ 1 ] / step_size;
    const TI nx = cl[ 2 ] / step_size;

    ProjReco proj{ projections };
    GpuVol reco( { nz, ny, nx } );
    __reco_gen<<<32,256>>>( reco, proj, rotations, nx, ny, nz, N<0>() );
    return reco;
}

__global__
void __ring_corr_matrix( GpuVolRef res, GpuVolRef ring_traces ) {
    const TI nb_points = res.ly * res.ly;
    for( TI num_point = blockIdx.x * blockDim.x + threadIdx.x; num_point < nb_points; num_point += gridDim.x * blockDim.x ) {
        TI i = num_point % res.ly;
        TI j = num_point / res.ly;

        TF acc = 0;
        for( TI z = 0; z < ring_traces.lz - 1; ++z ) {
            for( TI x = 0; x < ring_traces.lx - 1; ++x ) {
                TF grx_i = ring_traces( z, i, x + 1 ) - ring_traces( z, i, x + 0 );
                TF grx_j = ring_traces( z, j, x + 1 ) - ring_traces( z, j, x + 0 );
                TF grz_i = ring_traces( z + 1, i, x ) - ring_traces( z + 0, i, x );
                TF grz_j = ring_traces( z + 1, j, x ) - ring_traces( z + 0, j, x );

                acc += grx_i * grx_j + grz_i * grz_j;
            }
        }

        res( 0, i, j ) = acc;
    }
}

AF ring_corr_matrix( GpuVol &ring_traces, TF regul ) {
    GpuVol gpu_res( { 1, ring_traces.ly, ring_traces.ly } );
    __ring_corr_matrix<<<32,256>>>( gpu_res, ring_traces );

    AF res( { ring_traces.ly, ring_traces.ly } );
    cudaMemcpy( res.mutable_data(), gpu_res.data, gpu_res.size_in_mem(), cudaMemcpyDeviceToHost );

    TF m = 0;
    for( TI r = 0; r < ring_traces.ly; ++r )
        m = std::max( m, std::abs( res.at( r, r ) ) );
    m *= regul;

    for( TI r = 0; r < ring_traces.ly; ++r )
        res.mutable_at( r, r ) += m;

    return res;
}

__global__
void __ring_corr_vector( GpuVolRef res, GpuVolRef ring_traces, GpuVolRef reco ) {
    for( TI i = blockIdx.x * blockDim.x + threadIdx.x; i < ring_traces.ly; i += gridDim.x * blockDim.x ) {
        for( TI y = 0; y < res.lx; ++y ) {
            TF acc = 0;
            for( TI z = 0; z < ring_traces.lz - 1; ++z ) {
                for( TI x = 0; x < ring_traces.lx - 1; ++x ) {
                    TF grx_i = ring_traces( z, i, x + 1 ) - ring_traces( z, i, x + 0 );
                    TF grz_i = ring_traces( z + 1, i, x ) - ring_traces( z + 0, i, x );

                    TF grx_r = reco( z, y, x + 1 ) - reco( z, y, x + 0 );
                    TF grz_r = reco( z + 1, y, x ) - reco( z + 0, y, x );

                    acc += grx_i * grx_r + grz_i * grz_r;
                }
            }
            res( 0, i, y ) = acc;
        }
    }
}

AF ring_corr_vector( GpuVol &ring_traces, GpuVol &reco ) {
    GpuVol gpu_res( { 1, ring_traces.ly, reco.ly } );
    __ring_corr_vector<<<32,256>>>( gpu_res, ring_traces, reco );

    AF res( { pybind11::size_t( ring_traces.ly ), pybind11::size_t( reco.ly ) } );
    cudaMemcpy( res.mutable_data(), gpu_res.data, gpu_res.size_in_mem(), cudaMemcpyDeviceToHost );
    return res;
}

__global__
void __ring_corr_apply( GpuVolRef ring_traces, GpuVolRef reco, GpuVolRef vec ) {
    const TI nb_points = reco.lz * reco.lx;
    for( TI num_point = blockIdx.x * blockDim.x + threadIdx.x; num_point < nb_points; num_point += gridDim.x * blockDim.x ) {
        TI x = num_point % reco.lx;
        TI z = num_point / reco.lx;

        for( TI y = 0; y < reco.ly; ++y ) {
            TF acc = 0;
            for( TI i = 0; i < ring_traces.ly; ++i )
                acc -= ring_traces( z, i, x ) * vec( 0, i, y );
            reco( z, y, x ) += acc;
        }
    }
}

void ring_corr_apply( GpuVol &ring_traces, GpuVol &reco, AF &vec ) {
    GpuVol gpu_vec( { 1, vec.shape( 0 ), vec.shape( 1 ) } );
    cudaMemcpy( gpu_vec.data, vec.data(), gpu_vec.size_in_mem(), cudaMemcpyHostToDevice );
    __ring_corr_apply<<<32,256>>>( ring_traces, reco, gpu_vec );
}

__global__
void __binarisation_max_disc( GpuVolRef res, GpuVolRef reco, TF threshold, const int *offsets, const std::int8_t *points, TI nb_disks, int radius ) {
    const TI nb_points = res.lx * res.ly * res.lz;
    for( TI num_point = TI( blockIdx.x ) * blockDim.x + threadIdx.x; num_point < nb_points; num_point += gridDim.x * blockDim.x ) {
        TI ix = num_point % res.lx, ty = num_point / res.lx;
        TI iy = ty % res.ly;
        TI iz = ty / res.ly;

        bool val = false;
        for( int num_disk = 0; num_disk < nb_disks; ++num_disk ) {
            int nb_points = offsets[ num_disk + 1 ] - offsets[ num_disk + 0 ], nbw = 0;
            for( int off_point = offsets[ num_disk + 0 ]; off_point < offsets[ num_disk + 1 ]; ++off_point )
                nbw += reco( iz + radius + points[ off_point + 0 ], iy + radius + points[ off_point + 1 ], ix + radius + points[ off_point + 2 ] ) >= threshold;
            if ( nbw >= nb_points / 2 ) {
                val = true;
                break;
            }
        }

        res( iz, iy, ix ) = val;
    }
}

GpuVol binarisation_max_disc( GpuVol &reco, TF threshold, int radius ) {
    MediDisk md = medi_disk( std::max( 0, std::min( 8, radius ) ), 0 );
    if ( md.nb_disks < 0 ) {
        std::cerr << "wrong radius (medi_disk has not been compiled for this radius)" << std::endl;
        return reco.copy();
    }

    if ( reco.lz - 2 * radius <= 0 || reco.ly - 2 * radius <= 0 || reco.lx - 2 * radius <=0 ) {
        std::cerr << "volume for binarization is not large enough";
        return reco.copy();
    }

    int *offsets;
    cudaMalloc( &offsets, sizeof( int ) * ( md.nb_disks + 1 ) );
    cudaMemcpy( offsets, md.offsets, sizeof( int ) * ( md.nb_disks + 1 ), cudaMemcpyHostToDevice );

    std::int8_t *points;
    cudaMalloc( &points, sizeof( std::int8_t ) * ( md.offsets[ md.nb_disks ] - md.offsets[ 0 ] ) );
    cudaMemcpy( points, md.points, sizeof( std::int8_t ) * ( md.offsets[ md.nb_disks ] - md.offsets[ 0 ] ), cudaMemcpyHostToDevice );
    
    GpuVol res( reco.lz - 2 * radius, reco.ly - 2 * radius, reco.lx - 2 * radius );
    __binarisation_max_disc<<<32,256>>>( res, reco, threshold, offsets, points, md.nb_disks, radius );

    cudaFree( offsets );
    cudaFree( points );

    return res;
}

PYBIND11_MODULE( reco_gpu, m ) {
    m.doc() = "reco_gpu";

    m.def( "binarisation_max_disc", &binarisation_max_disc );
    m.def( "ring_corr_matrix"     , &ring_corr_matrix      );
    m.def( "ring_corr_vector"     , &ring_corr_vector      );
    m.def( "ring_corr_apply"      , &ring_corr_apply       );
    m.def( "reconstruction"       , &reconstruction        );
    m.def( "part_rotation"        , &part_rotation         );
    m.def( "ring_traces"          , &ring_traces           );
}


