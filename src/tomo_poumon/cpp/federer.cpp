#include <eigen3/Eigen/Dense>
#include "lib/ThreadPool.h"
#include "lib/Ad.h"
#include <iostream>
#include <cmath>

#include <pybind11/numpy.h>

//// nsmake cpp_flag -march=native
//// nsmake cpp_flag -O3
//// nsmake cpp_flag -g3

using TF = double;
using TI = ssize_t;
using AF = pybind11::array_t<TF>;

struct ResFederer {
    TF radius_cylinder;
    TF length_cylinder;
    TF radius_sphere;
    TF nb_spheres;
    AF error_grid;
    AF error_vec;
    TF error;
};

template<class T> static T p2( const T &a ) { return a * a; }
template<class T> static T p3( const T &a ) { return a * a * a; }
template<class T> static T pp( const T &a ) { using std::max; return max( a, T( 0 ) ); }

template<class T>
std::array<T,2> shape_functions( T radius_cylinder, T radius_sphere, T r ) {
    return {
        4 * M_PI / 3 * ( p3( radius_sphere ) - p3( pp( radius_sphere - r ) ) ),
        M_PI * ( p2( radius_cylinder ) - p2( pp( radius_cylinder - r ) ) )
    };
}

template<class T>
T error( T radius_cylinder, T length_cylinder, T radius_sphere, T nb_spheres, AF &reach ) {
    T res = 0;
    for( std::size_t i = 0; i < reach.shape( 0 ); ++i ) {
        std::array<T,2> coeffs = shape_functions( radius_cylinder, radius_sphere, T( reach.at( i, 0 ) ) );
        T v = nb_spheres * coeffs[ 0 ] + length_cylinder * coeffs[ 1 ];
        res += p2( v - reach.at( i, 1 ) );
    }
    return res;
}

ResFederer test( AF &reach, TF radius_cylinder, TF radius_sphere ) {
    // std::cout << radius_cylinder << " " << radius_sphere << std::endl;
    ResFederer res;
    res.radius_cylinder = radius_cylinder;
    res.radius_sphere = radius_sphere;

    Eigen::Matrix<TF,2,2> M;
    Eigen::Matrix<TF,2,1> V;
    M.setZero();
    V.setZero();
    for( TI i = 0; i < reach.shape( 0 ); ++i ) {
        auto coeffs = shape_functions( radius_cylinder, radius_sphere, reach.at( i, 0 ) );
        for( int r = 0; r < 2; ++r ) {
            for( int c = 0; c < 2; ++c )
                M.coeffRef( r, c ) += coeffs[ r ] * coeffs[ c ];
            V.coeffRef( r ) += coeffs[ r ] * reach.at( i, 1 );
        }
    }

    Eigen::Matrix<TF,2,1> X = M.colPivHouseholderQr().solve( V );
    res.length_cylinder = X[ 1 ];
    res.nb_spheres = X[ 0 ];

    res.error_vec = AF{ reach.shape( 0 ) };
    res.error = 0;
    for( TI i = 0; i < reach.shape( 0 ); ++i ) {
        auto coeffs = shape_functions( radius_cylinder, radius_sphere, reach.at( i, 0 ) );
        TF smu = 0;
        for( int r = 0; r < 2; ++r )
            smu += coeffs[ r ] * X[ r ];
        TF loc = smu - reach.at( i, 1 );
        res.error_vec.mutable_at( i ) = loc;
        res.error += loc * loc;
    }

    return res;
}

TI nb_val_step( TF min, TF max, TF inc ) {
    TI res = 0;
    for( TF rc = min; rc < max; rc += inc )
        ++res;
    return res;
}

void newton_step( ResFederer &res, AF &reach ) {
    using T = Ad<TF,4,2>;
    std::array<T,4> W{
        res.radius_cylinder,
        res.length_cylinder,
        res.radius_sphere  ,
        res.nb_spheres
    };
    for( std::size_t i = 0; i < W.size(); ++i )
        W[ i ].v[ i ] = 1;

    T d = error( W[ 0 ], W[ 1 ], W[ 2 ], W[ 3 ], reach );

    auto x = d.m.colPivHouseholderQr().solve( d.v );
    std::cout << "e=" << d.x << " v=" << x.transpose() << std::endl;

    TF relax = 0.25;
    res.radius_cylinder -= relax * x[ 0 ];
    res.length_cylinder -= relax * x[ 1 ];
    res.radius_sphere   -= relax * x[ 2 ];
    res.nb_spheres      -= relax * x[ 3 ];
}

ResFederer federer( AF &reach, TF min_rs, TF max_rs, TF inc_rs, TF min_rc, TF max_rc, TF inc_rc ) {
    AF error_grid( { nb_val_step( min_rs, max_rs, inc_rs ), nb_val_step( min_rc, max_rc, inc_rc ) } );

    ResFederer best;
    best.error = std::numeric_limits<TF>::max();
    for( TF rc = min_rc, nc = 0; rc < max_rc; rc += inc_rc, ++nc ) {
        for( TF rs = min_rs, ns = 0; rs < max_rs; rs += inc_rs, ++ns ) {
            ResFederer t = test( reach, rc, rs );
            if ( best.error > t.error )
                best = t;

            error_grid.mutable_at( ns, nc ) = t.error;
        }
    }

    ResFederer newt = best;
    for( std::size_t cpt = 0; cpt < 15; ++cpt ) {
        newton_step( newt, reach );

        ResFederer t = test( reach, newt.radius_cylinder, newt.radius_sphere );
        if ( best.error > t.error )
            best = t;
    }

    best.error_grid = error_grid;
    return best;
}


PYBIND11_MODULE( federer, m ) {
    m.doc() = "federer";

    pybind11::class_<ResFederer>( m, "ResFederer" )
        .def_readwrite( "radius_cylinder", &ResFederer::radius_cylinder, "" )
        .def_readwrite( "length_cylinder", &ResFederer::length_cylinder, "" )
        .def_readwrite( "radius_sphere"  , &ResFederer::radius_sphere  , "" )
        .def_readwrite( "nb_spheres"     , &ResFederer::nb_spheres     , "" )
        .def_readwrite( "error_grid"     , &ResFederer::error_grid     , "" )
        .def_readwrite( "error_vec"      , &ResFederer::error_vec      , "" )
        .def_readwrite( "error"          , &ResFederer::error          , "" )
    ;

    m.def( "federer", &federer );
}

