#include <boost/multiprecision/mpfr.hpp>
#include <eigen3/Eigen/Dense>
#include <pybind11/numpy.h>
#include <iostream>
#include <cmath>
#include <set>

//// nsmake lib_name gmp
//// nsmake lib_name mpfr

//// nsmake cpp_flag -march=native
//// nsmake cpp_flag -O3
//// nsmake cpp_flag -g3

using TF = double;
using TI = ssize_t;
using AF = pybind11::array_t<TF>;
using HP = boost::multiprecision::mpfr_float_100;

using TMat = Eigen::Matrix<HP,Eigen::Dynamic,Eigen::Dynamic>;
using TVec = Eigen::Matrix<HP,Eigen::Dynamic,1>;
#include "eiquadprog.h"

AF federer( AF &reach, TF min_rs, TF max_rs, TF inc_rs, TF min_rc, TF max_rc, TF inc_rc ) {
    // Eigen::Matrix<TF,2,2> M;
    // auto x = ;
    // AF error_grid( { nb_val_step( min_rs, max_rs, inc_rs ), nb_val_step( min_rc, max_rc, inc_rc ) } );

    std::vector<HP> rs_vec, rc_vec;
    for( TF rs = min_rs; rs <= max_rs; rs += inc_rs )
        rs_vec.push_back( rs );
    for( TF rc = min_rc; rc <= max_rc; rc += inc_rc )
        rc_vec.push_back( rc );

    TVec reach_eigpp( reach.shape( 0 ) );
    for( TI r = 0; r < reach.shape( 0 ); ++r )
        reach_eigpp[ r ] = reach.at( r, 1 );
    
    TMat shape_functions( rs_vec.size(), reach.shape( 0 ) );
    for( TI r = 0; r < rs_vec.size(); ++r )
        for( TI c = 0; c < reach.shape( 0 ); ++c )
            shape_functions.coeffRef( r, c ) = 4 * M_PI / 3 * ( pow( rs_vec[ r ], 3 ) - pow( rs_vec[ r ] > reach.at( c, 0 ) ? rs_vec[ r ] - reach.at( c, 0 ) : HP( 0 ), 3 ) );

    TMat M = shape_functions * shape_functions.transpose(), Ai( rs_vec.size(), rs_vec.size() ), Ae;
    TVec V = - shape_functions * reach_eigpp, X( rs_vec.size() ), Bi( rs_vec.size() ), Be;
    for( TI r = 0; r < rs_vec.size(); ++r )
        M.coeffRef( r, r ) += HP( 1e-3 );

    for( TI r = 0; r < rs_vec.size(); ++r )
        for( TI c = 0; c < rs_vec.size(); ++c )
            Ai.coeffRef( r, c ) = ( r == c );
    Bi.setZero();
    X.setZero();

    // EigenQP::quadprog( M, V, A, B, X );
    Eigen::solve_quadprog( M, V, Ae, Be, Ai, Bi, X );

    AF res( { pybind11::size_t( X.size() ), pybind11::size_t( 2 ) } );
    for( TI i = 0; i < X.size(); ++i ) {
        res.mutable_at( i, 0 ) = TF( rs_vec[ i ] );
        res.mutable_at( i, 1 ) = TF( X[ i ] );
    }
    return res;
}


PYBIND11_MODULE( federer_linalg, m ) {
    m.doc() = "federer";

    // pybind11::class_<ResFederer>( m, "ResFederer" )
    //     .def_readwrite( "radius_cylinder", &ResFederer::radius_cylinder, "" )
    //     .def_readwrite( "length_cylinder", &ResFederer::length_cylinder, "" )
    //     .def_readwrite( "radius_sphere"  , &ResFederer::radius_sphere  , "" )
    //     .def_readwrite( "nb_spheres"     , &ResFederer::nb_spheres     , "" )
    //     .def_readwrite( "error_grid"     , &ResFederer::error_grid     , "" )
    //     .def_readwrite( "error_vec"      , &ResFederer::error_vec      , "" )
    //     .def_readwrite( "error"          , &ResFederer::error          , "" )
    // ;

    m.def( "federer", &federer );
}

