//// nsmake inc_path /usr/local/cuda/include
//// nsmake lib_path /usr/local/cuda/lib64
//// nsmake cpp_flag -march=native
//// nsmake lib_name cudart
//// nsmake cpp_flag -O3

#include "lib/GpuVol.h"

using TF = GpuVolRef::TF;
using TI = GpuVolRef::TI;
using AF = pybind11::array_t<TF>;

GpuVol to_gpu( AF &a ) { return a; }

PYBIND11_MODULE( GpuVol, m ) {
    m.doc() = "gpu vol";

    pybind11::class_<GpuVol>( m, "GpuVol" )
     .def( pybind11::init<TI,TI,TI>()                               )
     .def( pybind11::init<AF&>()                                    )
     .def( "__sizeof__"    , &GpuVol::size_in_mem                   )
     .def( "to_cpu"        , &GpuVol::to_cpu                        )
     .def( "copy"          , &GpuVol::copy                          )
     .def_property( "shape", &GpuVol::get_shape, &GpuVol::set_shape );

    m.def( "to_gpu"       , &to_gpu );
}

