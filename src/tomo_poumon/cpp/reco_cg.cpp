#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <fstream>
#include <cmath>

#include "lib/TetraAssembly.h"
#include "lib/ThreadPool.h"
#include "lib/Point2.h"
#include "lib/Stream.h"

//// nsmake cpp_flag -march=native
//// nsmake cpp_flag -O3
//// nsmake cpp_flag -g3

// // nsmake cpp_flag -Wall

namespace py = pybind11;
using TF = float;
using TI = ssize_t;
using AF = pybind11::array_t<TF>;
using AI = pybind11::array_t<TI>;
using P2 = Point2<TF>;
using P3 = Point3<TF>;
using std::min;
using std::max;

/// for elem ... and angle ..., get traversed voxels (reco_...) and proj points
void for_each_traversed_voxels(
        const AF &reco, const AF &proj, const AI &elements, const AF &nodes,
        const std::function<void( TI num_angle, TI proj_x, TI proj_y, TI reco_x, TI reco_y, TI reco_z, TF reco_dist )> &f ) {

    struct Pc { using TF = double; using TI = std::size_t; using CI = int; enum { allow_ball_cut = false, dim = 3 }; };

    // dimension of mesh at angle 0
    P3 reco_orig( + std::numeric_limits<TF>::max() );
    P3 reco_pmax( - std::numeric_limits<TF>::max() );
    for( TI num_node = 0; num_node < nodes.shape( 1 ); ++num_node ) {
        P3 n( nodes.at( 0, num_node, 2 ), nodes.at( 0, num_node, 1 ), nodes.at( 0, num_node, 0 ) );
        reco_orig = min( reco_orig, n );
        reco_pmax = max( reco_pmax, n );
    }
    // P3 reco_scale = ( reco_pmax - reco_orig ) / P3( reco.shape( 2 ), reco.shape( 1 ), reco.shape( 0 ) );

    // loop for each angle
    TI nb_jobs = 4 * thread_pool.nb_threads();
    TI len_angles = proj.shape( 0 );
    //    std::mutex mutex;
    //    TI seen_angles = 0;
    thread_pool.execute( nb_jobs, [&]( TI num_job, int num_thread ) {
        TI beg_angle = ( num_job + 0 ) * len_angles / nb_jobs;
        TI end_angle = ( num_job + 1 ) * len_angles / nb_jobs;
        for( TI ind_angle = beg_angle; ind_angle < end_angle; ++ind_angle ) {
            TI num_angle = ind_angle;

            TF vol = 0;

            //            mutex.lock();
            //            ++seen_angles;
            //            PR( seen_angles, len_angles );
            //            mutex.unlock();

            for( TI num_element = 0; num_element < elements.shape( 0 ); ++num_element ) {
                TI n0 = elements.at( num_element, 0 );
                TI n1 = elements.at( num_element, 1 );
                TI n2 = elements.at( num_element, 2 );
                TI n3 = elements.at( num_element, 3 );

                P3 o0( nodes.at( 0, n0, 2 ), nodes.at( 0, n0, 1 ), nodes.at( 0, n0, 0 ) );
                P3 o1( nodes.at( 0, n1, 2 ), nodes.at( 0, n1, 1 ), nodes.at( 0, n1, 0 ) );
                P3 o2( nodes.at( 0, n2, 2 ), nodes.at( 0, n2, 1 ), nodes.at( 0, n2, 0 ) );
                P3 o3( nodes.at( 0, n3, 2 ), nodes.at( 0, n3, 1 ), nodes.at( 0, n3, 0 ) );

                P3 p0( nodes.at( num_angle, n0, 2 ), nodes.at( num_angle, n0, 1 ), nodes.at( num_angle, n0, 0 ) );
                P3 p1( nodes.at( num_angle, n1, 2 ), nodes.at( num_angle, n1, 1 ), nodes.at( num_angle, n1, 0 ) );
                P3 p2( nodes.at( num_angle, n2, 2 ), nodes.at( num_angle, n2, 1 ), nodes.at( num_angle, n2, 0 ) );
                P3 p3( nodes.at( num_angle, n3, 2 ), nodes.at( num_angle, n3, 1 ), nodes.at( num_angle, n3, 0 ) );

                P3 pc = TF( 1 ) / 4 * ( p0 + p1 + p2 + p3 );

                TF min_x = min( { p0.x, p1.x, p2.x, p3.x } ), max_x = max( { p0.x, p1.x, p2.x, p3.x } );
                TF min_y = min( { p0.y, p1.y, p2.y, p3.y } ), max_y = max( { p0.y, p1.y, p2.y, p3.y } );

                TetraAssembly ray, voxel;
                for( TI proj_y = max( TF( 0 ), min_y ); proj_y < min( TF( proj.shape( 1 ) ), max_y ); ++proj_y ) {
                    for( TI proj_x = max( TF( 0 ), min_x ); proj_x < min( TF( proj.shape( 2 ) ), max_x ); ++proj_x ) {
                        // start with the tetra
                        ray.init_with_tetra( p0, p1, p2, p3 );

                        // cut it with the faces of the ray
                        ray.plane_cut( { TF( proj_x + 0 ), TF( proj_y + 0 ), 0 }, { -1, 0, 0 } );
                        ray.plane_cut( { TF( proj_x + 0 ), TF( proj_y + 0 ), 0 }, { 0, -1, 0 } );
                        ray.plane_cut( { TF( proj_x + 1 ), TF( proj_y + 1 ), 0 }, { +1, 0, 0 } );
                        ray.plane_cut( { TF( proj_x + 1 ), TF( proj_y + 1 ), 0 }, { 0, +1, 0 } );

                        // if non void
                        if ( ! ray.empty() ) {
                            // get potentially touched voxels, move ray to ref reco coordinates
                            P3 min_pos_reco = + std::numeric_limits<TF>::max();
                            P3 max_pos_reco = - std::numeric_limits<TF>::max();
                            ray.for_each_node( [&]( TetraAssembly::TF &mod_x, TetraAssembly::TF &mod_y, TetraAssembly::TF &mod_z ) {
                                // get reference coordinates (-> al, be, ge). Generated using metil scripts/hugo/solve3.met
                                TF R0 = mod_z - p0.z; TF R1 = p2.x - p0.x; TF R2 = p3.y - p0.y; TF R3 = R1*R2; TF R4 = p3.x - p0.x; TF R5 = p2.y - p0.y;
                                TF R6 = R4*R5; TF R7 = R3-R6; TF R8 = R0*R7; TF R9 = mod_x - p0.x; TF R10 = p3.z - p0.z; TF R11 = R5*R10;
                                TF R12 = p2.z - p0.z; TF R13 = R2*R12; TF R14 = R11-R13; TF R15 = R9*R14; TF R16 = R8+R15; TF R17 = mod_y - p0.y;
                                TF R18 = R1*R10; TF R19 = R4*R12; TF R20 = R18-R19; TF R21 = R17*R20; TF R22 = R16-R21; TF R23 = p1.x - p0.x;
                                TF R24 = R23*R14; TF R25 = p1.z - p0.z; TF R26 = R25*R7; TF R27 = R24+R26; TF R28 = p1.y - p0.y; TF R29 = R28*R20;
                                TF R30 = R27-R29; TF R31 = 1/R30; TF R32 = R22*R31; TF al = R32; TF R33 = R23*R10; TF R34 = R4*R25;
                                TF R35 = R33-R34; TF R36 = R17*R35; TF R37 = R28*R10; TF R38 = R2*R25; TF R39 = R37-R38; TF R40 = R9*R39;
                                TF R41 = R23*R2; TF R42 = R4*R28; TF R43 = R41-R42; TF R44 = R0*R43; TF R45 = R40+R44; TF R46 = R36-R45;
                                TF R47 = R46*R31; TF be = R47; TF R48 = R23*R5; TF R49 = R1*R28; TF R50 = R48-R49; TF R51 = R0*R50;
                                TF R52 = R28*R12; TF R53 = R5*R25; TF R54 = R52-R53; TF R55 = R9*R54; TF R56 = R51+R55; TF R57 = R23*R12;
                                TF R58 = R1*R25; TF R59 = R57-R58; TF R60 = R17*R59; TF R61 = R56-R60; TF R62 = R61*R31; TF ga = R62;

                                // pos in reco
                                mod_x = o0.x + al * ( o1.x - o0.x ) + be * ( o2.x - o0.x ) + ga * ( o3.x - o0.x ) - reco_orig.x;
                                mod_y = o0.y + al * ( o1.y - o0.y ) + be * ( o2.y - o0.y ) + ga * ( o3.y - o0.y ) - reco_orig.y;
                                mod_z = o0.z + al * ( o1.z - o0.z ) + be * ( o2.z - o0.z ) + ga * ( o3.z - o0.z ) - reco_orig.z;

                                min_pos_reco.x = min( min_pos_reco.x, TF( mod_x ) );
                                min_pos_reco.y = min( min_pos_reco.y, TF( mod_y ) );
                                min_pos_reco.z = min( min_pos_reco.z, TF( mod_z ) );

                                max_pos_reco.x = max( max_pos_reco.x, TF( mod_x ) );
                                max_pos_reco.y = max( max_pos_reco.y, TF( mod_y ) );
                                max_pos_reco.z = max( max_pos_reco.z, TF( mod_z ) );
                            } );

                            // for each potentially touched voxel
                            for( TI reco_z = max( TF( 0 ), min_pos_reco.z ); reco_z < min( TF( reco.shape( 0 ) ), max_pos_reco.z ); ++reco_z ) {
                                for( TI reco_y = max( TF( 0 ), min_pos_reco.y ); reco_y < min( TF( reco.shape( 1 ) ), max_pos_reco.y ); ++reco_y ) {
                                    for( TI reco_x = max( TF( 0 ), min_pos_reco.x ); reco_x < min( TF( reco.shape( 2 ) ), max_pos_reco.x ); ++reco_x ) {
                                        TetraAssembly::Pt A( TF( reco_x + 0 ), TF( reco_y + 0 ), TF( reco_z + 0 ) );
                                        TetraAssembly::Pt B( TF( reco_x + 1 ), TF( reco_y + 1 ), TF( reco_z + 1 ) );

                                        // voxel cut
                                        voxel = ray;

                                        voxel.plane_cut( A, { -1, 0, 0 } );
                                        voxel.plane_cut( A, { 0, -1, 0 } );
                                        voxel.plane_cut( A, { 0, 0, -1 } );

                                        voxel.plane_cut( B, { +1, 0, 0 } );
                                        voxel.plane_cut( B, { 0, +1, 0 } );
                                        voxel.plane_cut( B, { 0, 0, +1 } );

                                        if ( TF vm = voxel.measure() ) {
                                            f( num_angle, proj_x, proj_y, reco_x, reco_y, reco_z, vm );
                                            vol += vm;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //PR( num_angle, vol );

        }
    } );
}

/*
*/
AF rdir( AF &reco, AF &proj, AI &elements, AF &nodes ) {
    AF res( { reco.shape( 0 ), reco.shape( 1 ), reco.shape( 2 ) } );
    for( TI z = 0; z < res.shape( 0 ); ++z )
        for( TI y = 0; y < res.shape( 1 ); ++y )
            for( TI x = 0; x < res.shape( 2 ); ++x )
                res.mutable_at( z, y, x ) = 0.0;

    AF dia( { reco.shape( 0 ), reco.shape( 1 ), reco.shape( 2 ) } );
    for( TI z = 0; z < res.shape( 0 ); ++z )
        for( TI y = 0; y < res.shape( 1 ); ++y )
            for( TI x = 0; x < res.shape( 2 ); ++x )
                dia.mutable_at( z, y, x ) = 0.0;

    std::mutex mutex;
    for_each_traversed_voxels( reco, proj, elements,nodes, [&]( TI num_angle, TI proj_x, TI proj_y, TI reco_x, TI reco_y, TI reco_z, TF reco_dist ) {
        mutex.lock();
        res.mutable_at( reco_z, reco_y, reco_x ) += proj.at( num_angle, proj_y, proj_x ) * reco_dist;
        dia.mutable_at( reco_z, reco_y, reco_x ) += reco_dist;
        mutex.unlock();
    } );

    for( TI z = 0; z < res.shape( 0 ); ++z )
        for( TI y = 0; y < res.shape( 1 ); ++y )
            for( TI x = 0; x < res.shape( 2 ); ++x )
                if ( dia.at( z, y, x ) > 1e-6 )
                    res.mutable_at( z, y, x ) /= dia.at( z, y, x );

    return res;
}

/*
*/
AF proj( AF &reco, AF &orig_proj, AI &elements, AF &nodes ) {
    AF res( { orig_proj.shape( 0 ), orig_proj.shape( 1 ), orig_proj.shape( 2 ) } );
    for( TI z = 0; z < res.shape( 0 ); ++z )
        for( TI y = 0; y < res.shape( 1 ); ++y )
            for( TI x = 0; x < res.shape( 2 ); ++x )
                res.mutable_at( z, y, x ) = 0.0;

    std::mutex mutex;
    for_each_traversed_voxels( reco, orig_proj, elements,nodes, [&]( TI num_angle, TI proj_x, TI proj_y, TI reco_x, TI reco_y, TI reco_z, TF reco_dist ) {
        mutex.lock();
        res.mutable_at( num_angle, proj_y, proj_x ) += reco.at( reco_z, reco_y, reco_x ) * reco_dist;
        mutex.unlock();
    } );

    return res;
}

PYBIND11_MODULE( reco_cg, m ) {
    m.doc() = "reco_cg";

    m.def( "rdir", &rdir );
    m.def( "proj", &proj );
}

