import numpy as np

def back_filter( input ):
    neve = int( np.round( 2 ** np.ceil( np.log2( input.shape[ 2 ] ) + 1 ) ) )
    cmul = np.indices( ( input.shape[ 1 ], int( neve / 2 ) + 1 ) )[ 1 ]
    for i in range( input.shape[ 0 ] ):
        tmp = np.ones( ( input.shape[ 1 ], neve ) ) * np.mean( input[ i, :, : ] )
        tmp[ :, : input.shape[ 2 ] ] = input[ i, :, : ]

        input[ i, :, : ] = np.fft.irfft( cmul * np.fft.rfft( tmp ) ).real[ :, 0 : input.shape[ 2 ] ]
    return input
