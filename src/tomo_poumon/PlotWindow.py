import pyqtgraph, numpy as np
from PyQt5.Qt import Qt

# pyqtgraph.setConfigOption( "useWeave", True )

class PlotWindow( pyqtgraph.GraphicsWindow ):
    def __init__( self, connection, name ):
        super().__init__( title = name )

        self.setAttribute( Qt.WA_ShowWithoutActivating )
        self.setWindowFlags( Qt.WindowStaysOnTopHint )

        self.connection = connection
        self.plot_items = {} # "plot": ..., "curves": {...}

    def keyPressEvent( self, event ):
        if event.key() == Qt.Key_Q:
            self.connection.quit()
            return

        if event.key() == Qt.Key_R:
            self.connection.send( "reset_plots" )
            return

        super().keyPressEvent( event )

    def set_content( self, nplt, ncrv, *args, **kargs ):
        """  """
        json_labels = kargs.pop( "json_labels", False )

        if nplt not in self.plot_items:
            p = self.addPlot()
            if ncrv:
                p.addLegend()
            xax = p.getAxis('bottom')
            xax.setLabel( nplt )
            self.plot_items[ nplt ] = { "plot": p, "curves": {} }
        if ncrv not in self.plot_items[ nplt ][ "curves" ]:
            self.plot_items[ nplt ][ "curves" ][ ncrv ] = self.plot_items[ nplt ][ "plot" ].plot( [], name = ncrv )
        self.plot_items[ nplt ][ "curves" ][ ncrv ].setData( *args, **kargs )

        # if json_labels, update the plot names
        if json_labels:
            for pi in self.plot_items.values():
                for na, cr in pi[ "curves" ].items():
                    cr.setData( name = "proute" )
