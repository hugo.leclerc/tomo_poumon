
class OptResult:
    def __init__( self, params, value ):
        self.params = {}
        for k, v in params.items():
            self.params[ k ] = v.value()
        self.value = value
