# -*- coding: utf8 -*-
import os

# répertoires où trouver $date/id17/Rat$x_sc$y. On peut ajouter autant qu'on veut, les scripts prendrons le premier répertoire qui donnera l'acquisition voulue.
acquisition_directories = [
    "/projet/RAT/",
    "/data/Rat/acquisitions/",
    "/home/leclerc/Data/Rat/acquisitions/",
    "/media/leclerc/CIBOX 1TO/Rat/",
    "/home/leclerc/Montages/projet_rat_cinaps/", # sshfs cinaps:/projet/RAT /home/leclerc/Montages/projet_rat_cinaps/
    "/homelocal/leclerc/Montages/projet_rat_cinaps/", # sshfs cinaps:/projet/RAT /homelocal/leclerc/Montages/projet_rat_cinaps/
]

# répertoire où stocker les résultats. On peut ajouter autant qu'on veut dans la liste, ça prendra le premier répertoire existant.
output_directory = next( filter( os.path.exists, [
    "/homelocal/leclerc/resultats",
    "/data/Rat/resultats",
    "/home/leclerc/resultats",
] ) ) 

# function used to display (verbose) information. You can comment the line to remove this information on the screen.
def info( msg ):
    print( "  \x1b[90m" + msg + "\x1b[0m" )
