
class OptParameter:
    def __init__( self, center, width, sampling, index = 0 ):
        self.sampling = sampling
        self.center = center
        self.width = width
        self.index = index

    @property
    def value( self ):
        off = ( self.index + 1 ) // 2 * self.width / self.sampling
        if self.index % 2:
            return self.center - off
        return self.center + off

    def next_index( self ):
        self.index += 1
        if self.index != self.sampling:
            return True
        self.index = 0
        return False

    def set_value( self, v ):
        if v < self.center:
            self.index = 2 * round( ( self.center - v ) * self.sampling / self.width ) + 1
        else:
            self.index = 2 * round( ( v - self.center ) * self.sampling / self.width )

    def repr_for_json( self ):
        return {
            "sampling" : self.sampling,
            "center" : self.center,
            "width" : self.width,
            "index" : self.index,
        }
