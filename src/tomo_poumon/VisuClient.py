import subprocess, socket, sys, os, time, json, pickle
import io, numpy as np
import urllib.parse

class VisuClient:
    """
        Handle a connection to a VisuServer. Launch it if necessary
    """
    def __init__( self, port = 15555 ):
        # make a subprocess
        self.proc = subprocess.Popen( [ sys.executable, "-m", "tomo_poumon.VisuServer", str( port ) ] )

        # socket
        for _ in range( 100 ):
            try:
                self.socket = socket.create_connection( ( "localhost", port ), timeout = 10000 )
                self.swfile = self.socket.makefile( "rwb" )
                break
            except ConnectionRefusedError:
                time.sleep( 0.1 )
        else:
            print( "connection refused" )
            sys.exit( 1 )
        
    def __del__( self ):
        if self.proc:
            self.proc.kill()

    def wait( self ):
        while True:
            i = self.get_instruction()
            if i == "quit":
                break

    def send_reset_plots( self ):
        self.swfile.write( bytes( "reset_plots\n", "latin1" ) )
        self.swfile.flush()

    def send_params( self, params ):
        d = dict( ( k, { "sampling": p.sampling, "center": p.center, "width": p.width, "index": p.index } ) for k, p in params.items() )
        self.swfile.write( bytes( "params " + json.dumps( d ) + "\n", "latin1" ) )
        self.swfile.flush()

    def send_image( self, name, image, nan_value = None ):
        if not ( nan_value is None ):
            image = image.copy()
            image[ np.isnan( image ) ] = nan_value
        self.send_with_np_save( "image " + urllib.parse.quote_plus( name ), image )

    def send_splot( self, name, image ):
        self.send_with_np_save( "splot " + urllib.parse.quote_plus( name ), image )

    def send_plot( self, name, *args, **kargs ):
        """ 
            see http://www.pyqtgraph.org/documentation/graphicsItems/plotdataitem.html#pyqtgraph.PlotDataItem.__init__ for args and kargs

            name can be a simple string (=> name of the window) or a list:
            * ( name of the window )    
            * ( name of the window, name of the plot )    
            * ( name of the window, name of the plot, name of the curve )    
        """
        if isinstance( name, tuple ) or isinstance( name, list ):
            s = ""
            for v in name:
                if s:
                    s += "&"
                s += urllib.parse.quote_plus( v )
            name = s
        else:
            name = urllib.parse.quote_plus( name )

        self.send_with_pickle( "plot " + name, ( args, kargs ) )

    def send_with_np_save( self, line, data ):
        f = io.BytesIO()
        np.save( f, data )
        buff = f.getbuffer()

        self.swfile.write( bytes( line + " " + str( len( buff ) ) + "\n", "latin1" ) )
        self.swfile.write( buff )
        self.swfile.flush()

    def send_with_pickle( self, line, data ):
        buff = pickle.dumps( data )

        self.swfile.write( bytes( line + " " + str( len( buff ) ) + "\n", "latin1" ) )
        self.swfile.write( buff )
        self.swfile.flush()

    def get_instruction( self ):
        """ wait for the next instruction """
        return str( self.swfile.readline().strip(), "latin1" )
