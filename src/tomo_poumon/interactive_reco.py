import multiprocessing
import itertools
import curses
import yaml
import sys
import os

from .load_or_execute import output_filename
from . import config

#
class Interface:
    def __init__( self, stdscr, experiment ):
        self.params_filename = os.path.join( config.output_directory, experiment, "reco_parameters.yml" )
        self.numeric_mode    = ""
        self.modified_var    = ""
        self.experiment      = experiment
        self.stdscr          = stdscr
        self.params          = {}
        self.cmds            = ["optimize", "reconstruct"]
        self.msg             = ""

    def loop( self ):
        # make an external process
        from . import reco_process
        queue_i2c = multiprocessing.Queue()
        queue_c2i = multiprocessing.Queue()
        process   = multiprocessing.Process( target=reco_process.process_func, args=( queue_i2c, queue_c2i, self.experiment ) )

        # try to load params and send them
        try:
            with open( self.params_filename, 'r' ) as f:
                self.params = yaml.load( f )
        except:
            pass
        process.daemon = True
        queue_i2c.put( self.params )

        # start the ext process
        process.start()

        # get params updated (once) by the external process
        self.params = queue_c2i.get()
        self.draw()

        # loop until quit key
        while self.key( self.stdscr.getkey() ):
            if self.numeric_mode == "" or any( [ cmd in self.params for cmd in self.cmds ] ):
                queue_i2c.put( self.params )
            self.draw()

            for cmd in self.cmds:
                self.params.pop( cmd, None )

    def draw( self ):
        if len( self.params ) and not ( self.modified_var in self.params ):
            self.modified_var = next( iter( self.params ) )

        # column lengths
        cn = [ "variable", "value", "amplitude" ]
        cs = [ len( c ) for c in cn ]
        for key in self.params:
            val = str( self.params[ key ][ "val" ] )
            amp = str( self.params[ key ][ "amp" ] )
            cs[ 0 ] = max( cs[ 0 ], len( key ) )
            cs[ 1 ] = max( cs[ 1 ], len( val ) )
            cs[ 2 ] = max( cs[ 2 ], len( amp ) )
        cs = [ 0 ] + list( itertools.accumulate( [ c + 1 for c in cs ] ) )

        # headers
        self.stdscr.clear()
        for i in range( len( cn ) ):
            self.stdscr.addstr( 0, cs[ i ], cn[ i ] )

        # values
        cpt = [ 1 ]
        cur_pos = 0
        for key in self.params:
            val = str( self.params[ key ][ "val" ] )
            amp = str( self.params[ key ][ "amp" ] )
            self.stdscr.addstr( cpt[ 0 ], cs[ 0 ], key )
            self.stdscr.addstr( cpt[ 0 ], cs[ 1 ], val )
            self.stdscr.addstr( cpt[ 0 ], cs[ 2 ], amp )
            if self.modified_var == key:
                cur_pos = cpt[ 0 ]
            cpt[ 0 ] += 1

        # values
        if self.msg:
            cpt[ 0 ] += 1
            self.stdscr.addstr( cpt[ 0 ], 0, self.msg )
            cpt[ 0 ] += 1

        # info
        def pmsg( cpt, msg ):
            cpt[ 0 ] += 1
            self.stdscr.addstr( cpt[ 0 ], 0, msg )
        pmsg( cpt, "key 's' to save the current parameters (for the next interactive session)" )
        pmsg( cpt, "key 'o' to optimize a parameter" )
        pmsg( cpt, "key 'q' to quit" )

        #
        try:
            fn = output_filename( self.experiment, [ 
                "s", [ int( self.params[ "scale xy" ][ "val" ] ), int( self.params[ "scale a" ][ "val" ] ) ], 
                "p", int( self.params[ "paga_coeff" ][ "val" ] ), 
                "b", [ int( self.params[ v ][ "val" ] ) for v in [ "center x", "center y", "center z", "len x", "len y", "len z" ] ],
                "x", self.params[ "x_slip" ][ "val" ],
                "y", self.params[ "y_slip" ][ "val" ],
                "z", self.params[ "z_slip" ][ "val" ],
                "r", self.params[ "x_rot" ][ "val" ],
            ], "", absolute = False )
            pmsg( cpt, "" )
            pmsg( cpt, "To make the reconstruction:" )
            pmsg( cpt, "  python -m tomo_poumon.reco {}".format( fn ) )
        except:
            pass

        # cursor
        x = ( self.numeric_mode != "" ) * cs[ 1 ]
        if self.numeric_mode == self.modified_var:
            x += len( str( self.params[ self.numeric_mode ][ "val" ] ) )
        self.stdscr.move( cur_pos, x )

        # refresh
        self.stdscr.refresh()

    def key( self, key ):
        if key in "qQ":
            return False

        if key == "+":
            self.params[ self.modified_var ][ "amp" ] *= 2
            return True
        if key == "-":
            self.params[ self.modified_var ][ "amp" ] /= 2
            return True

        if key == "KEY_DOWN":
            v = [ k for k in self.params ]
            i = v.index( self.modified_var )
            n = min( i + 1, len( v ) - 1 )
            self.modified_var = v[ n ]
            if self.numeric_mode != "":
                self.numeric_mode = " "
            return True
        if key == "KEY_UP":
            v = [ k for k in self.params ]
            i = v.index( self.modified_var )
            n = max( 0, i - 1 )
            self.modified_var = v[ n ]
            if self.numeric_mode != "":
                self.numeric_mode = " "
            return True

        if key == "KEY_LEFT":
            self.params[ self.modified_var ][ "val" ] -= self.params[ self.modified_var ][ "amp" ]
            return True
        if key == "KEY_RIGHT":
            self.params[ self.modified_var ][ "val" ] += self.params[ self.modified_var ][ "amp" ]
            return True

        if key in "0123456789":
            if self.numeric_mode != self.modified_var:
                self.numeric_mode = self.modified_var
                self.params[ self.modified_var ][ "val" ] = 0
            self.params[ self.modified_var ][ "val" ] = 10 * self.params[ self.modified_var ][ "val" ] + "0123456789".index( key )
            return True

        # KEY_DC => suppr
        # KEY_HOME KEY_END KEY_PPAGE KEY_NPAGE
        if key == "KEY_BACKSPACE":
            if self.numeric_mode != self.modified_var:
                self.numeric_mode = self.modified_var
            self.params[ self.modified_var ][ "val" ] = int( self.params[ self.modified_var ][ "val" ] / 10 )
            return True

        if key in "oO":
            v = [ k for k in self.params ]
            i = v.index( self.modified_var )
            self.params[ "optimize" ] = { "val": i, "amp": 0 }
            return True

        if key in "sS":
            os.makedirs( os.path.dirname( self.params_filename ), exist_ok = True )
            with open( self.params_filename, 'w' ) as f:
                yaml.dump( self.params, f )
            return True

        # if key == "r":
        #     self.params[ "reconstruct" ] = { "val": 1, "amp": 0 }
        #     return True

        # if key == "R":
        #     self.params[ "reconstruct" ] = { "val": 2, "amp": 0 }
        #     return True

        if key == "\n":
            self.numeric_mode = ""
            return True

        self.msg = key

        return True


# 
def run( experiment ):
    def main_curses( stdscr ):
        interface = Interface( stdscr, experiment )
        interface.loop()
    curses.wrapper( main_curses )

run( sys.argv[ 1 ] )
