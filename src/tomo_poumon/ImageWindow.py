from PyQt5.Qt import Qt
import pyqtgraph


class ImageWindow( pyqtgraph.ImageView ):
    def __init__( self, connection, name ):
        super().__init__()

        self.setAttribute( Qt.WA_ShowWithoutActivating )
        self.setWindowFlags( Qt.WindowStaysOnTopHint )

        self.connection = connection
        self.name = name

    def keyPressEvent( self, event ):
        if event.key() == Qt.Key_Q:
            self.connection.quit()
            return

        super().keyPressEvent( event )

        
