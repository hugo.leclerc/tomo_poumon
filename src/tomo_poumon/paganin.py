import numpy as np

def paganin( proj, coeff ):
    if coeff == 0:
        return proj

    nevx = int( np.round( 2 ** np.ceil( np.log2( proj.shape[ 2 ] ) + 1 ) ) )
    nevy = int( np.round( 2 ** np.ceil( np.log2( proj.shape[ 1 ] ) + 1 ) ) )

    grid = np.indices( ( nevy, nevx ) )
    kx   = 2 * np.pi * np.minimum( grid[ 1 ], nevx - grid[ 1 ] ) / nevx
    ky   = 2 * np.pi * np.minimum( grid[ 0 ], nevy - grid[ 0 ] ) / nevy
    cdiv = 1 + coeff * ( kx * kx + ky * ky )

    for i in range( proj.shape[ 0 ] ):
        tmp = np.ones( ( nevy, nevx ) ) * np.mean( proj[ i, :, : ] )
        tmp[ :proj.shape[ 1 ], :proj.shape[ 2 ] ] = proj[ i, :, : ]

        fwdt = np.fft.fft2( tmp[ :, : ] )
        invt = np.fft.ifft2( fwdt / cdiv )
        proj[ i, :, : ] = invt.real[ :proj.shape[ 1 ], :proj.shape[ 2 ] ]

    return proj

