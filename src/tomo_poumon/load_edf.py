from skimage.transform import downscale_local_mean
import numpy as np
import re

def load_edf( filename, beg_y = 0, len_y = -1, x_scale = 1 ):
    # text data
    f = open( filename, "r", encoding = "latin-1" )
    s = 4
    c = 3
    x = 0
    y = 0
    t = ""
    for l in f.readlines( 1024 ):
        d = re.search( 'Dim_1 = ([0-9]+)', l )
        if d:
            x = int( d.group( 1 ) )
            c -= 1 
            
        d = re.search( 'Dim_2 = ([0-9]+)', l )
        if d:
            y = int( d.group( 1 ) )
            c -= 1 
            
        d = re.search( 'DataType = ([a-zA-Z]+)', l )
        if d:
            m = {
              "SignedInteger": ( "int32"  , 4 ),
              "UnsignedShort": ( "uint16" , 2 ),
              "Float"        : ( "float32", 4 ),
              "FloatValue"   : ( "float32", 4 ),
            }
            t = m[ d.group( 1 ) ][ 0 ]
            s = m[ d.group( 1 ) ][ 1 ]
            c -= 1 
            
        if c == 0:
            break

    #
    if len_y < 0:
        len_y = y

    # binary data
    f.seek( 1024 + beg_y * x * s )
    R = np.fromfile( f, count = len_y * x, dtype = t, sep = "" )
    R = R.reshape( [ len_y, x ] ).astype( np.float32 )
    if x_scale != 1:
        R = downscale_local_mean( R, ( x_scale, x_scale ) )
    return R
