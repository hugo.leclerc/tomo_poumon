import json, io, pickle, urllib.parse, numpy as np
from .ParamWindow import ParamWindow
from .ImageWindow import ImageWindow
from .SplotWindow import SplotWindow
from .PlotWindow import PlotWindow
from PyQt5.Qt import Qt, QByteArray

class VisuServerConnection:
    def __init__( self, app, socket ):
        # socket
        self.socket = socket
        self.socket.readyRead.connect( self.readyRead )
        self.socket.disconnected.connect( self.disconnected )

        self.params_window = None
        self.image_windows = {}
        self.splot_windows = {}
        self.plot_windows = {}
        self.pending = None
        self.cmd = QByteArray()
        self.app = app

    def readyRead( self ):
        self.cmd += self.socket.readAll()

        while True:
            # in the middle of a command
            if self.pending:
                size = self.pending[ "size" ]
                if len( self.cmd ) < size:
                    break

                data = self.cmd.left( size )
                cmd = self.pending[ "cmd" ]
                self.cmd.remove( 0, size )
                self.pending = None

                self.on_cmd_with_data( cmd, data )

            # there's a new command
            p = self.cmd.indexOf( b'\n' )
            if p < 0:
                break

            cmd = str( self.cmd.left( p ), "latin1" )
            self.cmd.remove( 0, p+1 )

            #
            if cmd == "reset_plots":
                self.on_reset_plots()
                continue
            #
            if cmd.startswith( "params " ):
                self.on_params( cmd )
                continue

            if cmd.split()[ 0 ] in [ "image", "splot", "plot" ]:
                self.pending = { 'cmd': cmd, 'size': int( cmd.split()[ -1 ] ) }
                continue

            print( "unhandled command {}".format( cmd ) )

    def on_cmd_with_data( self, cmd, data ):
        if cmd.startswith( "image " ):
            self.on_image( cmd, data )
            return

        if cmd.startswith( "splot " ):
            self.on_splot( cmd, data )
            return

        if cmd.startswith( "plot " ):
            self.on_plot( cmd, data )
            return

        print( "unhandled command {}".format( cmd ) )

    def on_reset_plots( self ):
        self.plot_windows.clear()

    def on_image( self, cmd, data ):
        img = np.load( io.BytesIO( data ) )
        name = urllib.parse.unquote_plus( cmd.split()[ 1 ] )

        if name not in self.image_windows:
            self.image_windows[ name ] = ImageWindow( self, name )
            self.image_windows[ name ].show()
        self.image_windows[ name ].setImage( img )

    def on_splot( self, cmd, data ):
        img = np.load( io.BytesIO( data ) )
        name = urllib.parse.unquote_plus( cmd.split()[ 1 ] )

        if name not in self.splot_windows:
            self.splot_windows[ name ] = SplotWindow( self, name )
            self.splot_windows[ name ].show()
        self.splot_windows[ name ].setData( img )

    def on_plot( self, cmd, data ):
        lstn = cmd.split()[ 1 ].split( "&" )
        name = urllib.parse.unquote_plus( lstn[ 0 ] )
        nplt = urllib.parse.unquote_plus( lstn[ 1 ] ) if len( lstn ) >= 2 else ""
        ncrv = urllib.parse.unquote_plus( lstn[ 2 ] ) if len( lstn ) >= 3 else ""

        args, kargs = pickle.loads( data )

        if name not in self.plot_windows:
            self.plot_windows[ name ] = PlotWindow( self, name )
            self.plot_windows[ name ].show()
            if not self.params_window:
                self.plot_windows[ name ].activateWindow()
        self.plot_windows[ name ].set_content( nplt, ncrv, *args, **kargs )

    def on_params( self, cmd ):
        if not self.params_window:
            self.params_window = ParamWindow( self, json.loads( cmd[ 7: ] ) )
            self.params_window.show()
            self.params_window.activateWindow()

    def disconnected( self ):
        self.app.quit()

    def send( self, msg ):
        self.socket.write( bytes( msg + "\n", "utf8" ) )
        self.socket.flush()

    def quit( self ):
        self.send( "quit" )
        self.app.quit()
