from tomo_poumon import Opt, cst_val, load_edf_projections, back_filter, paganin, regular_rotation, reconstruction, ring_traces # max_median_on_discs
import tomo_poumon.cuda as cu
import numpy as np

# To force the CPU
# cu._checked_cuda = True

# 
for opt in Opt( interactive = True ):
    begx = opt.value( "begx", 200 )
    prot = regular_rotation( [ 400, 1, 400 ], corner_0 = ( 0, 0, 0 ), corner_1 = ( 400, 1, 400 ), off_rx = opt.value( "offx", 0 ) )
    rgtr = ring_traces( prot, begx, begx + 1 ).value()

    opt.visu_client.send_image( "rgtr", rgtr[ :, 0, : ], nan_value = 0 )

