from tomo_poumon.Cache import Cache
import numpy as np

c = Cache( max_mem = 800, verbose = True )

for i in range( 10 ):
    c.set( str( i ), np.ones( 10 ) )

print( c.get( "9" ).value )
print( c.size, c.compute_size() )
