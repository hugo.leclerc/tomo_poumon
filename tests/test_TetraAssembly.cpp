#include "../src/tomo_poumon/cpp/lib/TetraAssembly.h"
#include "../src/tomo_poumon/cpp/lib/Stream.h"
using Pt = TetraAssembly::Pt;

void test_tetra() {
    VtkOutput vo;
    TetraAssembly ta( { 0, 0, 0 }, { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } );
    PR( ta.barycenter() );
    PR( ta.measure() );

    ta.plane_cut( ta.barycenter(), { -1, 0, 0 } );

    ta.display_vtk( vo );
    vo.save( "out.vtk" );
}

void test_cube() {
    VtkOutput vo;
    for( int i = 0, p = 0; i < 27 * 2; ++i ) {
        Pt N( i % 3 - 1, ( i / 3 ) % 3 - 1, ( i / 9 ) % 3 - 1 );
        if ( N.x || N.y || N.z ) {
            TetraAssembly ta( { 0 + 1.5 * ( p % 6 ), 1.5 * ( p / 6 ), 0 }, { 1 + 1.5 * ( p % 6 ), 1 + 1.5 * ( p / 6 ), 1 } );
            if ( p == 0 ) {
                PR( ta.measure() );
                PR( ta.barycenter() );
            }
            ++p;

            PR( i );
            N /= norm_2( N );
            ta.plane_cut( ta.barycenter() + ( i / 27 ? - 0.4 : 0.4 ) * N, N );
            PR( ta.measure() );

            ta.display_vtk( vo );
        }
    }
    vo.save( "out.vtk" );
}

int main() {
    //    test_tetra();
    test_cube();
}
