from tomo_poumon import Opt, cst_val, add_ball, add_cylinder, dist, reach, federer, VisuClient, filter
import numpy as np

@filter( save = False )
def sum_filter( a, b ):
    r = a.copy()
    r[ :, 1 ] = a[ :, 1 ] + b[ :, 1 ]
    return r

def test_boundary_sphere( radius = 24, step = 0.5, only_sphere = False ):
    v = VisuClient()
    for bd in [ True, False ]:
        res_r = []
        res_n = []
        for pos in range( int( 1.5 * radius ) ):
            s = 2 * radius + 2
            d = cst_val ( size = [ s, s, s - pos ], value = -1e6 )
            d = add_ball( d, [ radius + 1, radius + 1, radius + 1 ], radius )
            d = dist( d, rem_dist_from_boundaries = bd )

            # opt.visu_client.send_image( "dist", d.value() * ( d.value() >= 0 ) )
            r = reach( d, step = step )
            f = federer( r, min_rs = 5, max_rs = 30, inc_rs = 0.025, min_rc = 2, max_rc = 30, inc_rc = 0.1, newton = False, only_spheres = only_sphere ).value()
            res_r.append( f.radius_sphere )
            res_n.append( f.nb_spheres )

        v.send_plot( ( "radius", "", [ "no corr", "bd corr" ][ bd ] ), res_r, pen = [ "r", "w" ][ bd ] )
        v.send_plot( ( "#spheres", "", [ "no corr", "bd corr" ][ bd ] ), res_n, pen = [ "r", "w" ][ bd ] )
    v.wait()

def test_linalg_two_spheres( radius = 24, delta_radius = 6, step = 1 ):
    s = 2 * ( radius + delta_radius ) + 2

    # ref
    d = cst_val ( size = [ s, s, s ], value = -1e6 )
    d = add_ball( d, [ s / 2, s / 2, s / 2 ], radius )
    d = dist( d )
    r_ref = reach( d, step = step )

    # visu = VisuClient()
    cpt = 0
    pre = np.linspace( radius - delta_radius, radius, 7 )
    val = []
    for other_radius in pre:
        e = cst_val ( size = [ s, s, s ], value = -1e6 )
        e = add_ball( e, [ s / 2, s / 2, s / 2 ], other_radius )
        e = dist( e )

        r_new = reach( e, step = step )
        r_tot = sum_filter( r_ref, r_new )
        f = federer( r_tot, min_rs = 18, max_rs = 30, inc_rs = 0.1, linalg = True ).value()

        val.append( np.sum( f[ :, 1 ] * f[ :, 0 ] ) / np.sum( f[ :, 1 ] ) )

        # visu.send_plot( ( "reach", "", "ref" ), r_ref.value()[ :, 0 ], r_ref.value()[ :, 1 ] )
        # visu.send_plot( ( "reach", "", "tot" ), r_tot.value()[ :, 0 ], r_tot.value()[ :, 1 ] )
        # visu.send_plot( ( "distrib", "", "R=" + str( other_radius ) ), f, pen = {'color': ["F00","0F0","00F","FF0","F0F","0FF","FFF"][ cpt % 7 ]} )
        cpt += 1

    # visu.send_plot( ( "R", "Radius 2nd sphere (Radius 1st = 24)" , "R (weighted sum)" ), pre, val )
    # visu.wait()

    return pre, val

def test_newton_two_spheres( radius = 24, delta_radius = 6, step = 1 ):
    s = 2 * ( radius + delta_radius ) + 2

    # ref
    d = cst_val ( size = [ s, s, s ], value = -1e6 )
    d = add_ball( d, [ s / 2, s / 2, s / 2 ], radius )
    d = dist( d )
    r_ref = reach( d, step = step )

    # visu = VisuClient()
    pre = np.linspace( radius - delta_radius, radius, 15 )
    val = []
    for other_radius in pre:
        e = cst_val ( size = [ s, s, s ], value = -1e6 )
        e = add_ball( e, [ s / 2, s / 2, s / 2 ], other_radius )
        e = dist( e )

        r_new = reach( e, step = step )
        r_tot = sum_filter( r_ref, r_new )
        f = federer( r_tot, min_rs = 18, max_rs = 30, inc_rs = 0.0125, min_rc = 18, max_rc = 18.1, inc_rc = 1, newton = False ).value()
        val.append( f.radius_sphere )

    # visu.send_plot( ( "R", "Radius 2nd sphere (Radius 1st = 24)" , "identified R" ), pre, val )
    # visu.wait()

    return pre, val

def test_inter_sphere_cylinder( radius_sphere = 30, radius_cylinder = 40, step = 1, linalg = False ):
    s = 2 * radius_sphere + 2 * radius_cylinder + 2

    # ref
    pre = np.linspace( 0, 2 * radius_sphere, 31 )
    vac = []
    vas = []
    for off in pre:
        d = cst_val( size = [ s, s, s ], value = -1e6 )
        c = radius_cylinder + 1
        d = add_ball( d, [ radius_sphere, c, c ], radius_sphere )
        d = add_cylinder( d, [ 2 * radius_sphere + 1 + radius_cylinder - off, c, c ], radius_cylinder )
        d = dist( d )

        r = reach( d, step = step )

        f = federer( r, min_rs = 20, max_rs = 85, inc_rs = 0.125, min_rc = 30, max_rc = 45, inc_rc = 0.5, newton = False, linalg = linalg ).value()
        if linalg:
            vas.append( np.sum( f[ :, 1 ] * f[ :, 0 ] ) / np.sum( f[ :, 1 ] ) )
        else:
            vac.append( f.radius_cylinder )
            vas.append( f.radius_sphere )

    visu = VisuClient()
    if not linalg:
       visu.send_plot( ( "R", "penetration" , "radius cylinder" ), pre, vac, pen = "r" )
    visu.send_plot( ( "R", "penetration" , "radius sphere" ), pre, vas )
    visu.wait()


def test_inter_sphere_sphere( radius_sphere = 40, step = 0.25, linalg = False ):
    pre = np.linspace( 0, 2 * radius_sphere, 41 )
    vas = []
    for off in pre:
        s = 4 * radius_sphere + 4
        c = radius_sphere + 2
    
        d = cst_val( size = [ s, s, s ], value = -1e6 )
        d = add_ball( d, [ c, c, c ], radius_sphere )
        d = add_ball( d, [ c + 2 * radius_sphere + 1 - off, c, c ], radius_sphere )
        d = dist( d )

        # visu = VisuClient()
        # visu.send_image( "d", d.value() )
        # visu.wait()

        r = reach( d, step = step )

        f = federer( r, min_rs = 20, max_rs = 50, inc_rs = 0.125, min_rc = 30, max_rc = 32, inc_rc = 1.5, newton = False, linalg = linalg ).value()
        if linalg:
            vas.append( np.sum( f[ :, 1 ] * f[ :, 0 ] ) / np.sum( f[ :, 1 ] ) )
        else:
            vas.append( f.radius_sphere )
        print( vas )

    visu = VisuClient()
    visu.send_plot( ( "R", "penetration" , "radius sphere" ), pre, vas )
    visu.wait()


# test_boundary_sphere( step = 0.1, only_sphere = False )
# pl, vl = test_linalg_two_spheres()
# pn, vn = test_newton_two_spheres()

# visu = VisuClient()
# visu.send_plot( ( "R", "Radius 2nd sphere (Radius 1st = 24)" , "linalg" ), pl, vl, pen = "g" )
# visu.send_plot( ( "R", "Radius 2nd sphere (Radius 1st = 24)" , "newton" ), pn, vn, pen = "b" )
# visu.send_plot( ( "R", "Radius 2nd sphere (Radius 1st = 24)" , "mean" )  , pl, 0.5 * ( pl + 24 ), pen = "w" )
# visu.wait()
# test_inter_sphere_cylinder( linalg = True )
test_inter_sphere_sphere()
