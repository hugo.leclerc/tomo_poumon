from tomo_poumon import Opt, regular_rotation, reco_cg, back_filter, reconstruction
from tomo_poumon import array as ar
from tomo_poumon import reco as rc
from tomo_poumon import mesh as ms
import numpy as np

for opt in Opt( interactive = True ):
    L = 60
    r = 0.3
    l = int( L * r )
    h = int( 0.75 * l )

    print( l )

    # make a large reconstruction
    breco = rc.make_sphere_reco( shape = [ L, 1, L ], pos = [ L / 2 - 0*l / 2, 0.5, L / 2 - 0*l / 2 ], radius = l / 2 )

    # projection
    bmesh = ms.make_box( [ L / 2 - l / 2, 0, L / 2 - l / 2 ], [ L / 2 + l / 2, 1, L / 2 + l / 2 ], l )
    nodes = ms.make_rotations( bmesh, ar.cst_val( [ h, 1, l ], 0 ), nb_scales = 1 )
    bproj = ar.exp_neg( rc.proj_cg( [ h, 1, l ], breco, bmesh, nodes ) )

    # opt.visu_client.send_image( "breco", breco.value().transpose( 1, 0, 2 ), nan_value = 0 )
    # opt.visu_client.send_image( "bproj", bproj.value().transpose( 1, 0, 2 ), nan_value = 0 )

    # reconstruction
    # reco = reco_cg( bproj, bmesh, nodes, nb_level_multiscale = 1 ).value()
    # opt.visu_client.send_image( "reco", reco.transpose( 1, 0, 2 ), nan_value = 0 )

    # proj = back_filter( proj )

    # prot = regular_rotation( proj.shape, corner_0 = [ 0.25 * l, 0, 0.25 * l ], corner_1 = [ 0.75 * l, 1, 0.75 * l ], off_rx = 0 )

    # reco = reconstruction( proj, prot )
    # print( reco.shape )
    # print( reco.value()[ 25, 0, 25 ] )
    # opt.visu_client.send_image( "reco", reco.value().transpose( 1, 2, 0 ), nan_value = 0 )

