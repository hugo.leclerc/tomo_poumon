from tomo_poumon import load_edf_projections, back_filter, paganin, regular_rotation, reconstruction
from sklearn.mixture import GaussianMixture
import matplotlib.pyplot as plt
import numpy as np

def disp( name, vol ):
    rd = vol.value()
    y_mid = int( rd.shape[ 1 ] / 2 )

    from matplotlib import pyplot
    pyplot.imsave( name + '.png', rd[ :, y_mid, : ], cmap = "gray" )

# 
best_tv = 1e300
best_parms = {}
for x_slip in np.arange( -5, 5 ):
    for y_slip in np.arange( -5, 5 ):
        for z_slip in np.arange( -5, 5 ):
            # projections
            proj = load_edf_projections( "2018_07_01/id17/Rat6_sc8_", beg_y = 0, len_y = 100 )
            proj = paganin( proj, coeff = 350 )
            proj = back_filter( proj )

            # reconstruction
            b_extr = np.array( [ 1002, 51, 1326 ] )
            l_extr = np.array( [  400,  2,  400 ] )
            prot = regular_rotation( proj.shape, corner_0 = b_extr, corner_1 = b_extr + l_extr, off_rx = 8.5, x_slip = x_slip, y_slip = y_slip, z_slip = z_slip )
            reco = reconstruction( proj, prot )

            # GaussianMixture
            X = reco.value().ravel().reshape( [ -1, 1 ] )

            gm = GaussianMixture( n_components=2, random_state=0 ).fit( X )
            print( np.sum( gm.covariances_.ravel() ) )
            print( gm.covariances_ )
            print( gm.converged_ )
            print( gm.means_ )

            # plt.hist( X, 50 ) # density=True, facecolor='g', alpha=0.75
            # plt.show()

            tv = np.sum( gm.covariances_.ravel() )
            if best_tv > tv:
                print( x_slip, y_slip, z_slip, tv )
                best_parms[ "x_slip" ] = x_slip
                best_parms[ "y_slip" ] = y_slip
                best_parms[ "z_slip" ] = z_slip
                disp( "base", reco )
                best_tv = tv

print( best_parms )
