from tomo_poumon import Opt, cst_val, load_edf_projections, back_filter, paganin, regular_rotation, reconstruction, ring_corr, binarisation_max_disc
from tomo_poumon import max_median_on_discs
from tomo_poumon import total_variation
from tomo_poumon import reco
#from matplotlib import pyplot
import numpy as np

#[   46   223   390   573   751   916  1101  1278  1445  1629  1805  1973
  #2156  2327  2502  2682  2858  3028  3207  3385  3551  3735  3912  4081
  #4263  4440  4610  4789  4969  5137  5317  5494  5665  5844  6023  6188
  #6373  6551  6717  6904  7081  7248  7432  7609  7777  7961  8138  8305
  #8487  8666  8834  9014  9193  9361  9540  9719  9883 10069 10245 10412
 #10596 10774 10940 11125 11303 11468 11653 11830 11998 12181 12359 12524
 #12709 12886 13055 13238 13416 13584 13769 13947 14113 14300 14478 14646
 #14830 15006 15175 15359 15533 15703 15889 16065 16235 16417 16596 16763
 #16947 17122 17293 17477 17654 17824 18008 18186 18356 18538 18716 18882
 #19067 19242 19413 19596 19772 19940 20124 20302 20468 20652 20827 20997
 #21178 21354 21522 21704 21883 22049 22232 22411 22580 22761 22940 23108
 #23291 23466 23638 23820 23993 24162 24347 24523 24691 24874 25051 25218
 #25402 25577 25746 25928 26105 26273 26456 26631 26799 26981 27155 27324
 #27506 27682 27849 28031 28210 28380 28557 28736 28902 29086 29263 29432
 #29613 29789 29956 30140 30318 30488 30667 30846 31019 31195 31373 31538
 #31720 31896 32065 32248 32425 32596 32774 32950 33118 33300 33478 33645
 #33828 34007 34191 34357 34534 34701 34882 35057 35227 35409 35587 35762
 #35933 36111 36288 36459 36636 36813 36984 37162 37330 37511 37689 37867
 #38040 38218 38385 38568 38744 38913 39093 39249 39403 39576 39751 39906
 #40059 40236 40409 40585 40759 40934 41090 41242 41422 41592 41768 41946
 #42117 42294 42473 42644 42820 42998 43168 43344 43528 43691 43867 44051
 #44214 44390 44575 44737 44913 45098 45260 45437 45605 45784 45960 46130
 #46310 46486 46651 46833 47011 47177 47360 47538 47720 47887 48063 48246
 #48413 48590 48771 48939 49115 49301 49464 49642 49826 49988 50166 50342
 #50511 50689 50866 51035 51212 51389 51561 51737 51916 52086 52265 52440
 #52614 52792 52977 53142 53319 53494 53669 53847 54026 54196 54374 54558
 #54724 54898 55085 55249 55423 55610 55771 55947 56132 56295 56472 56656
 #56819 56995 57174 57342 57517 57696 57865 58041 58224 58386 58565 58746
 #58911 59086 59262 59433 59609 59792 59955 60132 60304 60478 60654 60829
 #61000 61174 61355 61522 61697 61877 62042 62218 62400 62563 62738 62921
 #63084 63260 63430 63606 63783 63966 64130 64310 64493 64658 64836 65016
 #65186 65366 65534 65716 65893 66060 66245 66422 66606 66774 66948 67129
 #67295 67471 67654 67816 67992 68166 68336 68511 68688 68856 69030 69210
 #69376 69551 69724 69896 70073 70247 70417 70593 70759 70938 71116 71287
 #71461 71638 71817 71983 72158 72335 72504 72678 72860 73024 73196 73374
 #73542 73715 73884 74062 74237 74411 74583 74758 74934 75104 75280 75457
 #75627 75802 75978 76151 76327 76511 76675 76851 77029 77199 77378 77550
 #77723 77900 78069 78250 78428 78596 78777 78956 79125 79306 79483 79663
 #79834 80012 80188 80361 80538 80711 80888 81066 81236 81415 81593 81764
 #81944 82123 82301 82474 82651 82836 83000 83176 83360 83523 83700 83864
 #84047 84225 84393 84574 84753 84920 85102 85278 85447 85628 85803 85970
 #86151 86324 86491 86672 86845 87013 87193 87367 87536 87715 87888 88056
 #88235 88407 88586 88754 88927 89095 89271 89443 89610 89786 89962]

def run_exp( base ):
    grey_levels = reco.load_edf_projections_gray_levels( base ) # , flat = "ref0000_"

    #s = selection.value()
    #print( s[1:] - s[:-1] ) 

    # 
    for opt in Opt( interactive = True ):
        # parameters
        #b_extr = opt.value_array( "[reco] beg_extraction_", [ 1145, 51, 1326 ] )
        #l_extr = opt.value_array( "[reco] len_extraction_", [  400, 17,  400 ] )
        #b_extr = opt.value_array( "[reco] beg_extraction_", [    0,  0,    0 ] )
        #l_extr = opt.value_array( "[reco] len_extraction_", [ 2560, 17, 2560 ] )
        b_extr = opt.value_array( "[reco] beg_extraction_", [    0, 10,    0 ] )
        l_extr = opt.value_array( "[reco] len_extraction_", [ 2560, 80, 2560 ] )
        off_rx = opt.value( "[reco] off_rx", 11 )
        r_ring = opt.value( "[reco] regul ring", 1e-4 )
        paga_c = opt.value( "[reco] paga coeff", 350 )

        med_rd = int( opt.value( "[mdft] median disc radius", 5 ) )
        med_th = int( opt.value( "[mdft] median disc thickness", 0 ) )

        len_of_cycle = 30
        for num_in_cycle in range( len_of_cycle ):
            print( num_in_cycle, len_of_cycle )
            
            # selection
            selection = reco.selection_from_gray_levels( grey_levels, pos_in_cycle = num_in_cycle / len_of_cycle )

            # projections
            proj = load_edf_projections( base, beg_y = 100, len_y = 100, selection = selection )
            proj = paganin( proj, coeff = paga_c )
            proj = back_filter( proj )

            # reconstruction
            prot = regular_rotation( proj.shape, corner_0 = b_extr, corner_1 = b_extr + l_extr, off_rx = off_rx )
            peco = reconstruction( proj, prot )
            # if opt.value( "[filter] do ring corr", 0 ):
            #     peco = ring_corr( peco, prot, r_ring )

            # display
            if False:
                rd = peco.value()
                y_mid = int( rd.shape[ 1 ] / 2 )
                rd = rd[ :, y_mid : y_mid+1, : ]
                #opt.visu_client.send_image( "reco", rd.transpose( 1, 2, 0 ), nan_value = 0 )
                #opt.set_result( "TV", total_variation( rd ) )
                
                #import matplotlib
                #matplotlib.image.imsave( 'reco_{}.png'.format( num_in_cycle ), np.nan_to_num( rd[ :, 0, : ] ), cmap = "gray" )

            # median filtering
            peco = max_median_on_discs( peco, radius = med_rd, thickness = med_th )
            
            # display
            if False:
                rd = peco.value()
                y_mid = int( rd.shape[ 1 ] / 2 )
                rd = rd[ :, y_mid : y_mid+1, : ]
                
                # opt.visu_client.send_image( "mdft", np.maximum( -4000, rd.transpose( 1, 2, 0 ) ), nan_value = 0 )
                
                #import matplotlib
                #matplotlib.image.imsave( 'medi_{}.png'.format( num_in_cycle ), np.minimum( 4000, np.maximum( -4000, np.nan_to_num( rd[ :, 0, : ] ) ) ), cmap = "gray" )

            np.save( "reco_{}".format( num_in_cycle ), peco.value() )

            # # binarization
            # if rstage >= 2:
            #     rbin = binarisation_max_disc( reco, threshold = bin_th, radius = dsc_rd )

            #     rd = rbin.value()
            #     if dsp_md:
            #         y_mid = int( rd.shape[ 1 ] / 2 )
            #         rd = rd[ :, y_mid : y_mid+1, : ]
            #     opt.visu_client.send_image( "rbin", rd.transpose( 1, 0, 2 ), nan_value = 0 )

            # # dist
            # if rstage >= 3:
            #     dstv = dist( rbin, threshold = 0.5 )
            #     opt.visu_client.send_image( "dist", dstv.value().transpose( 1, 0, 2 ) )

            # # federer
            # if rstage >= 4:
            #     reav = reach( dstv )
            #     fede = federer( reav, min_rs = min_rs, max_rs = max_rs, inc_rs = ( max_rs - min_rs ) / 50, min_rc = min_rc, max_rc = max_rc, inc_rc = ( max_rc - min_rc ) / 50 ).value()

            #     opt.set_result( "radius cylinder", fede.radius_cylinder )
            #     opt.set_result( "radius sphere", fede.radius_sphere )

            #     opt.visu_client.send_plot( ( "reach", "", "exper" ), reav.value()[ :, 0 ], reav.value()[ :, 1 ] )
            #     opt.visu_client.send_plot( ( "reach", "", "ident" ), reav.value()[ :, 0 ], reav.value()[ :, 1 ] + fede.error_vec, pen = 'r' )

            #     opt.visu_client.send_image( "error grid", fede.error_grid )

run_exp( "2018_11_30/id17/Rat7_sc5_" )
