from tomo_poumon import Opt, cst_val, add_ball, dist, reach, federer
import numpy as np

# x = np.linspace( 0, 30, 31 )
# R = 14.5
# r = np.array( [ x, 4 * np.pi / 3 * ( R**3 - np.clip( R - x, 0, None )**3 ) ] ).transpose()
# f = federer( r, min_rs = 10, max_rs = 20, inc_rs = 1, min_rc = 10, max_rc = 20, inc_rc = 1 ).value()

# 
for opt in Opt( interactive = True ):
    d = cst_val ( size = [ 50, 50, 50 ], value = -1e6 )
    d = add_ball( d, opt.value_array( "c0", [ 34, 34, 34 ] ), opt.value( "r0", 15 ) )
    d = add_ball( d, opt.value_array( "c1", [ 16, 16, 16 ] ), opt.value( "r1", 15 ) )
    d = dist( d )

    r = reach( d, step = 1 / 100 )
    i = max( 1e-4, opt.value( "step", 0.5 ) )
    f = federer( r, min_rs = 10, max_rs = 20, inc_rs = i, min_rc = 10, max_rc = 20, inc_rc = i ).value()
    print( f )
    
    # opt.visu_client.send_plot( "f", f[ :, 0 ], f[ :, 1 ] )

    # opt.set_result( "radius sphere", f.radius_sphere )
    # opt.set_result( "radius cylinder", f.radius_cylinder )

    # opt.visu_client.send_image( "dist", np.maximum( d.value(), 0 ) )
    # opt.visu_client.send_plot( ( "reach", "", "exper" ), r.value()[ :, 0 ], r.value()[ :, 1 ] )
    # opt.visu_client.send_plot( ( "reach", "", "ident" ), r.value()[ :, 0 ], r.value()[ :, 1 ] + f.error_vec, pen = 'r' )

    # opt.visu_client.send_splot( "error grid", f.error_grid )
