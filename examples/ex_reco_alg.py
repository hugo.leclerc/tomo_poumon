from tomo_poumon import Opt, load_edf_projections, regular_rotation
from tomo_poumon import total_variation
import numpy as np

# 
for opt in Opt( interactive = True ):
    # parameters
    b_extr = opt.value_array( "[reco] beg_extraction_", [ 1002, 51, 1326 ] ) # [ 1197, 18, 1214 ]
    l_extr = opt.value_array( "[reco] len_extraction_", [  400, 17,  400 ] )
    off_rx = opt.value      ( "[reco] off_rx", -73.5 )
    r_ring = opt.value      ( "[reco] regul ring", 1e-4 )
    dsp_md = opt.value      ( "[disp] display at the middle y", 1 )

    # projections
    proj = load_edf_projections( "2018_07_01/id17/Rat4_sc3_", beg_y = 100, len_y = 1 )

    opt.visu_client.send_image( "proj", proj.value().transpose( 1, 0, 2 ), nan_value = 0 )

    # reconstruction
    # prot = regular_rotation( proj.shape, corner_0 = b_extr, corner_1 = b_extr + l_extr, off_rx = off_rx )
    # if rstage == 0:
    #     opt.set_result( "TV", total_variation( reco ) )

