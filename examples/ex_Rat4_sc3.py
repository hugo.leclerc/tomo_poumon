from tomo_poumon import Opt, load_edf_projections, back_filter, paganin, regular_rotation, reconstruction, ring_corr, binarisation_max_disc, dist, reach, federer
from tomo_poumon import max_median_on_discs
from tomo_poumon import total_variation
import numpy as np

def disp( opt, name, vol ):
    if opt.value( "[disp] display at the middle y", 1 ):
        rd = vol.value()
        y_mid = int( rd.shape[ 1 ] / 2 )
        rd = rd[ :, y_mid : y_mid+1, : ]
        opt.visu_client.send_image( name, rd.transpose( 1, 0, 2 ), nan_value = 0 )

    if opt.value( "[disp] display volume", 0 ):
        rd = vol.value()
        opt.visu_client.send_image( name, rd.transpose( 1, 0, 2 ), nan_value = 0 )

    if opt.value( "[disp] save img at the middle y", 0 ):
        rd = vol.value()
        y_mid = int( rd.shape[ 1 ] / 2 )
    
        from matplotlib import pyplot
        pyplot.imsave( name + '.png', rd[ :, y_mid, : ], cmap = "gray" )

# 
for opt in Opt( interactive = True ):
    # parameters
    rstage = opt.value      ( "stage (RMBDF)", 0 ) # 0: only reconstruction. 1: median filtering, 2: binarization, 3: dist, 4: federer
    paga_c = opt.value      ( "[reco] paganin coeff", 350 ) #
    b_extr = opt.value_array( "[reco] beg_extraction_", [ 1002, 51, 1326 ] ) # [ 1197, 18, 1214 ]
    l_extr = opt.value_array( "[reco] len_extraction_", [  400, 17,  400 ] ) # 17
    off_rx = opt.value      ( "[reco] off_rx", 8.5 )
    r_ring = opt.value      ( "[reco] regul ring", 1e-4 )

    x_slip = opt.value      ( "[reco] x_slip", 0.0 )
    y_slip = opt.value      ( "[reco] y_slip", 0.0 )
    z_slip = opt.value      ( "[reco] z_slip", 0.0 )

    med_rd = int( opt.value ( "[mdft] median disc radius", 6 ) )
    med_th = int( opt.value ( "[mdft] median disc thickness", 0 ) )

    dsc_rd = int( opt.value ( "[bina] binarization disc radius", 0 ) )
    bin_th = opt.value      ( "[bina] threshold", -660 )

    min_rs = opt.value      ( "[fede] min rs", 20 )
    max_rs = opt.value      ( "[fede] max_rs", 30 )
    min_rc = opt.value      ( "[fede] min_rc", 20 )
    max_rc = opt.value      ( "[fede] max_rc", 30 )

    # projections
    proj = load_edf_projections( "2018_07_01/id17/Rat6_sc8_", beg_y = 0, len_y = 100 )
    proj = paganin( proj, coeff = paga_c )
    proj = back_filter( proj )

    # reconstruction
    prot = regular_rotation( proj.shape, corner_0 = b_extr, corner_1 = b_extr + l_extr, off_rx = off_rx, x_slip = x_slip, y_slip = y_slip, z_slip = z_slip )
    reco = reconstruction( proj, prot )
    disp( opt, "base", reco )
    if opt.value( "[filter] do ring corr", 0 ):
        reco = ring_corr( reco, prot, r_ring )
        disp( opt, "ring", reco )

    # opt.visu_client.send_image( "reco", rd.transpose( 1, 0, 2 ), nan_value = 0 )
    if rstage == 0:
        opt.set_result( "TV", total_variation( reco ) )

    # median filtering
    if rstage >= 1:
        reco = max_median_on_discs( reco, radius = med_rd, thickness = med_th )
        disp( opt, "mdft", reco )

    # binarization
    if rstage >= 2:
        rbin = binarisation_max_disc( reco, threshold = bin_th, radius = dsc_rd )
        disp( opt, "rbin", rbin )

    # dist
    if rstage >= 3:
        dstv = dist( rbin, threshold = 0.5 )
        disp( opt, "dist", dstv )

    # federer
    if rstage >= 4:
        reav = reach( dstv )
        fede = federer( reav, min_rs = min_rs, max_rs = max_rs, inc_rs = ( max_rs - min_rs ) / 50, min_rc = min_rc, max_rc = max_rc, inc_rc = ( max_rc - min_rc ) / 50 ).value()

        opt.set_result( "radius cylinder", fede.radius_cylinder )
        opt.set_result( "radius sphere", fede.radius_sphere )

        opt.visu_client.send_plot( ( "reach", "", "exper" ), reav.value()[ :, 0 ], reav.value()[ :, 1 ] )
        opt.visu_client.send_plot( ( "reach", "", "ident" ), reav.value()[ :, 0 ], reav.value()[ :, 1 ] + fede.error_vec, pen = 'r' )

        opt.visu_client.send_image( "error grid", fede.error_grid )

