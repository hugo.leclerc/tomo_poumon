TomoPoumon Howto
================

Installation
------------

If you're not a sudoer, you can install everything using conda. Everything about conda is explained in https://docs.conda.io/en/latest/miniconda.html)

After installation (*and activation*) of conda:

* `conda install nodejs scikit-image pyopengl pybind11` (needed for nsmake)
* `conda install -c conda-forge scikit-fmm` (needed to compute distances)
* `conda install -c anaconda pyqtgraph` (needed for display)
* go to https://github.com/hleclerc/nsmake#installation to install nsmake (build system for C++, Cuda, ...)
* if you have a nvidia GPU, make sure the CUDA toolkit is installed (`conda install cudatoolkit`)

Don't forget add your directories in src/tomo_poumon/config.py (to find the acquisitions and to define where to store the intermediate results).