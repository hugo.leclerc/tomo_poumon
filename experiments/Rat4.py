from tomo_poumon import Opt, load_edf_projections, back_filter, paganin, regular_rotation, reconstruction, ring_corr, binarisation_max_disc, dist, reach, federer
from tomo_poumon import max_median_on_discs
from tomo_poumon import total_variation
from tomo_poumon import save_xdmf
import numpy as np

def disp( name, vol, save_volume = False, save_mid_img = True ):
    rd = vol.value()

    if save_volume:
        save_xdmf( rd, name )

    if save_mid_img:
        y_mid = int( rd.shape[ 1 ] / 2 )

        from matplotlib import pyplot
        pyplot.imsave( name + '.png', rd[ :, y_mid, : ], cmap = "gray" )

def disp_opt( opt, name, vol ):
    rd = vol.value()
    y_mid = int( rd.shape[ 1 ] / 2 )
    rd = rd[ :, y_mid : y_mid+1, : ]
    opt.visu_client.send_image( name, rd.transpose( 1, 0, 2 ), nan_value = 0 )

# 
exprs = [
    { "name": "Rat4_sc2_", "off_rx": -73.5, "threshold": -750 },
    { "name": "Rat4_sc3_", "off_rx": -73.5, "threshold": -200 },
    { "name": "Rat4_sc4_", "off_rx": -73.5, "threshold": -750 },
]

for expr in exprs:
    print( "expr_name", expr[ "name" ] )
    for opt in Opt( interactive = True ):
        # 0: only reconstruction. 1: median filtering, 2: binarization, 3: dist, 4: federer, 5: not interactive
        rstage = opt.value( "stage (RMBDF)", 0 )
        if rstage >= 5:
            opt.interactive = False

        # parameters
        paga_c = 350
        b_extr = opt.value_array( "[reco] beg_extraction_", [ 1002,  51, 1326 ] )
        l_extr = opt.value_array( "[reco] len_extraction_", [  400, 400,  400 ] )
        off_rx = opt.value      ( "[reco] off_rx", expr[ "off_rx" ] )
        r_ring = 1e-4

        # projections
        proj = load_edf_projections( "2018_07_01/id17/" + expr[ "name" ], beg_y = 0, len_y = 600 )
        proj = paganin( proj, coeff = paga_c )
        proj = back_filter( proj )

        # reconstruction
        prot = regular_rotation( proj.shape, corner_0 = b_extr, corner_1 = b_extr + l_extr, off_rx = off_rx )
        reco = reconstruction( proj, prot )
        if rstage == 0:
            disp_opt( opt, "reco", reco )
        # reco = ring_corr( reco, prot, r_ring )
        
        # # median filter
        # mdft = max_median_on_discs( reco, radius = 6, thickness = 0 )
        # disp( "mdft_" + expr_name, mdft, save_volume = True )

        # # binarization
        # # for opt in Opt( interactive = True ):
        # #     rbin = binarisation_max_disc( mdft, threshold = opt.value( "threshold", -200 ), radius = opt.value( "radius", 6 ) )
        # #     disp_opt( opt, "rbin", rbin )

        # rbin = binarisation_max_disc( mdft, threshold = -200, radius = 6 )
        # disp( "rbin_" + expr_name, rbin, save_volume = True )

        # dstv = dist( rbin, threshold = 0.5 )

        # # federer
        # min_rs = 10
        # max_rs = 100
        # min_rc = 50
        # max_rc = 200
        # reav = reach( dstv )
        # fede = federer( reav, min_rs = min_rs, max_rs = max_rs, inc_rs = ( max_rs - min_rs ) / 250, min_rc = min_rc, max_rc = max_rc, inc_rc = ( max_rc - min_rc ) / 50 ).value()
        # print( "radius cylinder", fede.radius_cylinder )
        # print( "radius sphere", fede.radius_sphere )
        # print( "fede", fede )
