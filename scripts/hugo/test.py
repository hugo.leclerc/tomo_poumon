# from tomo_poumon import VolumeFilter, CstVal, Opt
# import numpy as np

# # ================
# class SinusMaker( VolumeFilter ):
#     def __init__( self, period, lx, ly, lz = 0 ):
#         def make_sin( period, lx, ly, lz ):
#             if lz:
#                 x, y, z = np.meshgrid( 1 + np.arange( lx ), 1 + np.arange( ly ), 1 + np.arange( lz ) )
#                 return np.sin( x * np.pi / period ) * np.sin( y * np.pi / period ) * np.sin( z * np.pi / period )
#             x, y = np.meshgrid( np.arange( lx ), np.arange( ly ) )
#             return np.sin( x * np.pi / period ) * np.sin( y * np.pi / period )
#         super().__init__( make_sin, period = period, lx = lx, ly = ly, lz = lz, save = False )

# # ================
# for opt in Opt( interactive = True ):
#     # l = opt.value( "mrad", 1 )
#     d = CstVal( lx = 400, ly = 400, lz = 1, value = -10 ) \
#         .add_ball( opt.value( "cx", 100 ), opt.value( "cy", 100 ), 0, opt.value( "ra", 50 ) ) \
#         .add_ball( opt.value( "dx", 300 ), opt.value( "dy", 100 ), 0, opt.value( "da", 50 ) ) \
#         .dist()
#     r = d.reach()
#     # f = r.federer()

#     opt.visu_client.send_image( "dist", d.value() )
#     opt.set_error( "dist", np.max( d.value() ) )
#     # print( r.value() )
#     # opt.visu_client.send_plot( ( "reach", "", "a" ), r.value(), pen = 'r' )
# for opt in Opt( interactive = True ):
#     # l = opt.value( "mrad", 1 )
#     d = CstVal( lx = 400, ly = 400, lz = 1, value = -10 ) \
#         .add_ball( opt.value( "cx", 100 ), opt.value( "cy", 100 ), 0, opt.value( "ra", 50 ) ) \
#         .add_ball( opt.value( "dx", 300 ), opt.value( "dy", 100 ), 0, opt.value( "da", 50 ) ) \
#         .dist()
#     r = d.reach()
#     f = r.federer( min_rs = 10, max_rs = 150, inc_rs = 0.1, min_rc = 10, max_rc = 150, inc_rc = 0.1 ).value()

#     opt.visu_client.send_image( "dist", d.value() )
#     opt.visu_client.send_plot( ( "reach", "", "exper" ), r.value()[ :, 0 ], r.value()[ :, 1 ] )
#     opt.visu_client.send_plot( ( "reach", "", "ident" ), r.value()[ :, 0 ], r.value()[ :, 1 ] + f.error_vec, pen = 'r' )

#     opt.set_error( "error", f.error )
#     print( f.radius_cylinder )
#     print( f.radius_sphere )
from tomo_poumon.check_cuda import check_cuda
print( check_cuda() )
