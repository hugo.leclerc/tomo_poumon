from tomo_poumon import load_edf_projections, back_filter, paganin, regular_rotation, reconstruction, ring_corr, VisuClient
from torch.autograd import Variable
from torch import nn
import numpy as np
import imageio
import torch

# extraction des reconstructions
b_extr = np.array([ 1002, 51, 1326 ])
l_extr = np.array([  400, 17,  400 ])
off_rx = -73.5
r_ring = 1e-4

proj = load_edf_projections( "2018_07_01/id17/Rat4_sc3_", beg_y = 100, len_y = 100 )
proj = paganin( proj, coeff = 350 )
proj = back_filter( proj )

prot = regular_rotation( proj.shape, corner_0 = b_extr, corner_1 = b_extr + l_extr, off_rx = off_rx )
reco = reconstruction( proj, prot )
# reco = ring_corr( reco, prot, r_ring )

# visu
v = np.empty( [ 3, 400, 400 ] )
v[ 0, :, : ] = reco.value()[ :, 8, : ]
v[ 1, :, : ] = imageio.imread( "data/sm_r4s3_1002_59_1326_400_1_400.png" )[ :, :, 2 ] > 100
v[ 2, :, : ] = imageio.imread( "data/sm_r4s3_1002_59_1326_400_1_400.png" )[ :, :, 2 ] > 100

vc = VisuClient()
vc.send_image( "reco", v, nan_value = 0 )
vc.wait()

class BasicBlock( nn.Module ):
    def __init__( self, in_channels, out_channels, **kwargs ):
        super( BasicBlock, self ).__init__()

        self.conv = nn.Conv3d( in_channels, out_channels, bias=False, **kwargs )
        self.btcn = nn.BatchNorm2d( out_channels, eps=0.001 )
        self.maxp = nn.MaxPool3d( kernel_size=3, stride=2, padding=1, ceil_mode=True )

    def forward(self, x):
        x = self.conv( x )
        x = self.btcn( x )
        return nn.functional.relu( x, inplace=True )


class Segmenter( nn.Module ):
    def __init__( self ):
        super( Segmenter, self ).__init__()

        self.conv1 = ConvBlock( 1, 64, kernel_size=5 )
        self.maxb1 = nn.MaxPool3d( kernel_size=3, stride=2, padding=1, ceil_mode=True )
        self.conv1 = ConvBlock( 64, 128, kernel_size=5 )

    def forward( self, x ):
        x = self.conv1( x )
        x = self.maxb1( x )
        return x

# def run( random_sound_subset, nb_samples, nb_sounds_for_training ):
#     # train
#     model = Autoencoder().cuda()
#     optimizer = torch.optim.Adam( model.parameters(), lr = 1e-3, weight_decay = 1e-5 )

#     num_epochs = 100
#     criterion = nn.MSELoss()
#     for epoch in range( num_epochs ):
#         inp_sounds = random_sound_subset.data( nb_samples, nb_sounds_for_training )
#         inp_sounds = Variable( inp_sounds ).cuda()

#         # ===================forward=====================
#         out_sounds = model( inp_sounds )

#         n = np.linspace( 0, 1, nb_samples )
#         h = torch.tensor( n * ( 1 - n ) ).cuda()
#         loss = criterion(
#             torch.abs( torch.rfft( inp_sounds * h, 1 ) ),
#             torch.abs( torch.rfft( out_sounds * h, 1 ) )
#             # inp_sounds,
#             # out_sounds
#         )

#         # ===================backward====================
#         optimizer.zero_grad()
#         loss.backward()
#         optimizer.step()

#         # ===================log========================
#         print( 'epoch [{}/{}], loss:{}' .format( epoch+1, num_epochs, loss.data ) )

#         if epoch + 1 == num_epochs:
#             for i in range( 20 ):
#                 play( inp_sounds[ i, 0, : ].cpu().detach().numpy() )
#                 play( out_sounds[ i, 0, : ].cpu().detach().numpy() )


# run( RandomSoundSubset( data_filenames.names, 50000 ), 10000, 1000 )
